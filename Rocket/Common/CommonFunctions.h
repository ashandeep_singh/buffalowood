//
//  CommonFunctions.h
//  
//
//  Created by GDS on 23/01/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 *  System Versioning Preprocessor Macros
 */
 
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


@interface CommonFunctions : NSObject {


}

+ (UIColor*)getThemeColor:(NSString*)color;

// user check session
+ (BOOL)isUserLogin;
+ (BOOL)userLoginDetail:(NSDictionary*)userDict;
+ (NSString*)getUserID;
+ (NSString*)getUsername;
+ (NSString*)getUseremail;

+ (double)distanceFromMyLocationWithLat:(float)lat andLong:(float)lng;

+ (BOOL)userLoginWithUserName:(NSString*)userName andUserID:(NSString*)userID;
+ (NSDictionary*)userDetailFromUserDefaults;
+ (void)userLogout;
+ (void)removeAllUserDefault;

+ (NSString*)getDateFormated:(NSString*)date;

+(UIBarButtonItem*) getBackBtn;
+(void)BackBtnPressed;




+ (NSString *)documentsDirectory;
+ (void)openEmail:(NSString *)address;
+ (void)openPhone:(NSString *)number;
+ (void)openSms:(NSString *)number;
+ (void)openBrowser:(NSString *)url;
+ (void)openMap:(NSString *)address;

+ (void) hideTabBar:(UITabBarController *) tabbarcontroller;
+ (void) showTabBar:(UITabBarController *) tabbarcontroller;
+ (void) checkAndCreateDatabase;
+(void) setNavigationTitle:(NSString *) title ForNavigationItem:(UINavigationItem *) navigationItem;

+ (void)scrollViewToCenterOfScreen:(UIView *)theView onScrollView:(UIScrollView *) scrollview;
+(void) becomeNextFirstResponder:(UIView*)vc :(UITextField*)textField;


// get Image with cache store
+(UIImage*)getCachedImage:(NSString*)imageUrl;
// get Image without store cache image
+ (UIImage*)getImageFromURLWithoutCache:(NSString*)URL;

+(void)showServerNotFoundError;



// set passcode
+ (void)setPassCode:(NSString*)value;

// Alert View 
+(void) AlertTitle:(NSString*)title withMsg:(NSString*)msg;
+(void) AlertTitle:(NSString*)title withMsg:(NSString*)msg andDelegate:(id)delegate;

// get retine display or not 
+ (BOOL)isRetineDisplay;

// check that network available or not 
+ (BOOL)isNetworkConnect;

//check that server is resonse is null or not 
+ (BOOL)isValueNotEmpty:(NSString*)aString;


// badge number set and increase decrease
+ (void)setBadgeNumber:(NSUInteger)totalBedgeNumber;
+ (void)increaseBadgeNumber;
+ (void)decreaseBadgeNumber;

// for base 64 converter
+ (NSString *)decodeBase64WithStringToString:(NSString *)strBase64;
+ (NSData *)decodeBase64WithString:(NSString *)strBase64;
+ (NSString *)encodeBase64WithString:(NSString *)strData;
+ (NSData*)encodeBase64WithData:(NSString *)strBase64 ;

// view shaker 
+ (void)shakeView:(UIView *)viewToShake;
+ (void)vibrate;

// show network activity indicator
+ (void) setNetworkActivityIndicatorVisible:(BOOL)setVisible;
+ (UIColor*)colorWithHexString:(NSString*)hex;

@end
