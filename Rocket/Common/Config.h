//
//  Config.h
//  
//
//  Created by GDS on 23/01/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define IS_iPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define IS_iPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define DELEGATE ((AppDelegate*)[[UIApplication sharedApplication]delegate])

#define pr(str,args...) NSLog(str,args);
#define pro(str)        NSLog(@"%@",str);

#define viewBgColor self.view.backgroundColor=[UIColor colorWithRed:((float)((246 & 0xFF0000) >> 16))/255.0 green:((float)((246 & 0xFF00) >> 8))/255.0 blue:((float)(246 & 0xFF))/255.0 alpha:1.0]

#define kMapboxMapID @"ashusingh.ja6db8mk"

//#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface Config : NSObject {

    
}



//extern AppDelegate *appDelegate;



extern NSMutableDictionary* theme;

extern BOOL isUserLogin;

//configuration section...
extern  NSString            *SiteURL;
extern  NSString            *SiteAPIURL;
extern  NSString            *SiteSecureKey;
extern  NSString *isRepeat;

extern UIWindow *mainWindow;

extern UITabBarController *tabController;

extern NSString *appDeviceToken;

// for g+
extern NSString *const kGPlusClientID;





extern  NSString            *DatabaseName;
extern  NSString            *DatabasePath;

extern NSMutableDictionary  *dicGlobal;
extern NSMutableArray *arrayEventsAllCats;
//extern NSString             *userId;

extern BOOL isBackGroundMode;

extern BOOL  isBackGroundMode;

// for facebook id
extern NSString             *kFBAppId ;
extern BOOL                 isFacebookLogin;
extern BOOL                 userLoginWithFB;

// for twitter 
extern NSString		*kOAuthConsumerKey;
extern NSString		*kOAuthConsumerSecret;
extern BOOL         isTwitterLogin;


// for passcode
extern NSString             *passCode;

//for GPS current location
extern float core_latitude, core_longitude;

extern CLLocationManager    *locationManager;         //
extern CLLocation           *currentLocation;        // For Latitude & Longitude
extern CLLocationCoordinate2D coordinates;          //
 
@end
