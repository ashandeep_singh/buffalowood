//
//  CommonFunctions.m
//
//
//  Created by GDS on 23/01/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "CommonFunctions.h"
#import "ImgCache.h"
#import "AppDelegate.h"
#import "GPImage.h"
#import <AudioToolbox/AudioToolbox.h>
#import "UIImage+RoundedCorner.h"

@implementation CommonFunctions


+ (UIColor*)getThemeColor:(NSString*)color
{
    
    unsigned int outVal;
    [[NSScanner scannerWithString:theme[color]] scanHexInt:&outVal];
    //    NSLog(@"%x",outVal);
    
    return UIColorFromRGB(outVal);
}

#pragma mark - user session
+(BOOL)isUserLogin{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //    NSLog(@"userdeflt===  %@",[userDefaults objectForKey:@"userDict"]);
    NSString *userID = [[userDefaults objectForKey:@"userDict"] objectForKey:@"id"];
    if ( userID  && ![Validate isNull:userID] )
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL)userLoginDetail:(NSDictionary*)userDict{
    if (userDict == nil) {
        return NO;
    }
    
    // set SESSION for user
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:userDict forKey:@"userDict"];
    [userDefaults synchronize];
    isUserLogin = YES;
    //    NSLog(@"userdefault=== %@",[userDefaults objectForKey:@"userDict"]);
    return YES;
}
+ (NSString*)getUserID{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //    NSLog(@"userdeflt===  %@",[userDefaults objectForKey:@"userDict"]);
    NSString *userID = [[userDefaults objectForKey:@"userDict"] objectForKey:@"id"];
    if ( userID  && ![Validate isNull:userID] )
    {
        return userID;
    }
    
    return nil;
}
+ (NSString*)getUsername{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //    NSLog(@"userdeflt===  %@",[userDefaults objectForKey:@"userDict"]);
    NSString *username = [[userDefaults objectForKey:@"userDict"] objectForKey:@"name"];
    if ( username  && ![Validate isNull:username] )
    {
        return username;
    }
    
    return nil;
}
+ (NSString*)getUseremail{
   
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    return [[defaults objectForKey:@"useremail"] lowercaseString];
    
}


+ (double)distanceFromMyLocationWithLat:(float)lat andLong:(float)lng {
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:core_latitude longitude:core_longitude];
    
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
    
    CLLocationDistance distance = [locA distanceFromLocation:locB];

    // in meters to km
    distance /= 1000;
    
    return (double)distance;    
}





+ (BOOL)userLoginWithUserName:(NSString*)userName andUserID:(NSString*)userID{
    if ([Validate isNull:userName] || [Validate isNull:userID]) {
        return NO;
    }
    
    // set SESSION for user
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:userName forKey:@"username"];
    [userDefaults setObject:userID forKey:@"userID"];
    [userDefaults synchronize];
    isUserLogin = YES;
    return YES;
}
+ (NSDictionary*)userDetailFromUserDefaults{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"USER DEFAULT user  detail userDict ===  %@",[userDefaults objectForKey:@"userDict"]);
    return [userDefaults objectForKey:@"userDict"];
}

+ (void)userLogout{
    //RESET THE DEFAULT USER THUS SESSION LOGOUT
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    //    [defs removeObjectForKey:@"username"];
    [defs removeObjectForKey:@"userDict"];
    //    [defs removeObjectForKey:@"userLoginWithFacebook"];
    [defs synchronize];
    isUserLogin = NO;
}

+ (void)removeAllUserDefault{
    // remove all user default
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
    isUserLogin = NO;
}

+ (NSString*)getDateFormated:(NSString*)date{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:date];
    dateFormatter.dateFormat = @"MMM dd, yyyy";
    return [dateFormatter stringFromDate:yourDate];
}



#pragma mark- navigation bar back btn
// Make Custom back button for Navigation Bar
+(UIBarButtonItem*) getBackBtn{
	UIButton * backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_iPad) {
        [backBtn setFrame:CGRectMake(0.0f,0.0f,20.0f,84.0f)];
    } else {
        [backBtn setFrame:CGRectMake(0.0f,0.0f,23.0f,23.0f)];
    }
	
    
    
	[backBtn setImage:[UIImage imageNamed:@"navbar-back-icon"] forState:UIControlStateNormal];
	[backBtn addTarget:self action:@selector(BackBtnPressed) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
	return backBarButton;
}
+(void)BackBtnPressed{
    UITabBarController *tabBarController = (UITabBarController*)[[DELEGATE window] rootViewController];
    UINavigationController *navigationController = (UINavigationController*)[tabBarController selectedViewController];
    [navigationController  popViewControllerAnimated:YES];
    //    UINavigationController *navController = (UINavigationController*) [[DELEGATE window] rootViewController];
    //    [navController popViewControllerAnimated:YES];
}





//Default document paths
+ (NSString *)documentsDirectory {
    NSArray *paths =
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
										NSUserDomainMask,
										YES);
    return [paths objectAtIndex:0];
}

// Open application methods
+ (void)openEmail:(NSString *)address {
    NSString *url = [NSString stringWithFormat:@"mailto://%@", address];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    /*
    // if opening in web view use this code
    // web view delegates
    - (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
        if ([[[request URL] scheme] isEqual:@"mailto"]) {
            [[UIApplication sharedApplication] openURL:[request URL]];
            return NO;
        }
        return YES;
    }
    */
}

+ (void)openPhone:(NSString *)number {
    NSString *url = [NSString stringWithFormat:@"tel://%@", number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openSms:(NSString *)number {
    NSString *url = [NSString stringWithFormat:@"sms://%@", number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openBrowser:(NSString *)url {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openMap:(NSString *)address {
	NSString *addressText = [address stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSString *url = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@", addressText];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}



// Custom tab bar
+ (void) hideTabBar:(UITabBarController *) tabbarcontroller {
   
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in tabbarcontroller.view.subviews)
    {
		if([view isKindOfClass:[UITabBar class]])
        {
			[view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
            
        }
		else
		{
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
        }
		
    }
	
    [UIView commitAnimations];
	
}
// Tab bar custmize
+ (void) showTabBar:(UITabBarController *) tabbarcontroller {
	
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            if(result.height > 480)
            {
                
                NSLog(@"%@", view);
                
                if([view isKindOfClass:[UITabBar class]])
                {
                    [view setFrame:CGRectMake(view.frame.origin.x, 519, view.frame.size.width, view.frame.size.height)];
                    
                }
                else
                {
                    [view setFrame:CGRectMake(view.frame.origin.x, 519, view.frame.size.width, 431)];
                }
            }
            else
            {
                if([view isKindOfClass:[UITabBar class]])
                {
                    [view setFrame:CGRectMake(view.frame.origin.x, 430, view.frame.size.width, view.frame.size.height)];
                    
                }
                else
                {
                    [view setFrame:CGRectMake(view.frame.origin.x, 430, view.frame.size.width, 431)];
                }

            }
            
         }
    }
	
    [UIView commitAnimations];
	
}

// Database connect
+(void) checkAndCreateDatabase{
	// Check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success;
	
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:DatabasePath];
	
	// If the database already exists then return without doing anything
	if(success) return;
	
	// If not then proceed to copy the database from the application to the users filesystem
	
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DatabaseName];
	
	// Copy the database from the package to the users filesystem
	[fileManager copyItemAtPath:databasePathFromApp toPath:DatabaseName error:nil];
	
    
}

//Navigation custom title and font color need to write this method replacment of (self.title = @"";)
+(void) setNavigationTitle:(NSString *) title ForNavigationItem:(UINavigationItem *) navigationItem
{
    
    UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 44.0f)];
    
    [titleLbl setFont:[UIFont fontWithName:@"Helvetica" size:24.0f]];
    [titleLbl setBackgroundColor:[UIColor clearColor]];
    //    [titleLbl setTextAlignment:UITextAlignmentCenter];
    
    [titleLbl setTextColor:[UIColor blackColor]];
    //[titleLbl setShadowColor:UIColorFromRedGreenBlue(186,186,186)];
    [titleLbl setShadowOffset:CGSizeMake(1.0f, 1.0f)];
    
    [titleLbl setText:title];
    
    
    [navigationItem setTitleView:titleLbl];
    
    /** May also use this code for Whole App.
     * Inharit NavigationBar (CustomNavBar.h, .m)
     
     [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
     [UIColor blackColor], UITextAttributeTextColor,
     [UIColor whiteColor], UITextAttributeTextShadowColor,
     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
     [UIFont boldSystemFontOfSize:20.0f], UITextAttributeFont,
     nil]];
     */
    
    
}

// set UITextField to be center of UISrollView pass the textFiled and scrollView obj as arguments
+ (void)scrollViewToCenterOfScreen:(UIView *)theView onScrollView:(UIScrollView *) scrollview {
    
	
	CGFloat viewCenterY = theView.center.y;
	CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
	CGFloat availableHeight = applicationFrame.size.height - 230;            // Remove area covered by keyboard
    
	CGFloat y = viewCenterY - availableHeight / 2.0;
	if (y < 0) {
		y = 0;
	}
	[scrollview setContentOffset:CGPointMake(0, y) animated:YES];
    
}


+(void) becomeNextFirstResponder:(UIView*)vc :(UITextField*)textField{
	
	NSMutableArray *arr1=[[NSMutableArray alloc] init];
	BOOL isFound=NO;
    
	for(UIView *tf in [vc subviews])
	{
		if([tf isKindOfClass:[UITextField class]] && ((UITextField*)tf).enabled  && ((UITextField*)tf).hidden==NO)
		{
			[arr1 addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:((UITextField*)tf).frame.origin.y	], @"y", ((UITextField*)tf), @"obj", nil]];
		}
	}
	NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"y"
																 ascending:YES];
    
    
	NSArray	*sortDescriptors = [NSArray arrayWithObject:sortByName];
	arr1 = [[arr1 sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
	for(NSMutableDictionary *dic in arr1){
		UITextField *tf=[dic valueForKey:@"obj"];
		if (isFound) {
			[tf becomeFirstResponder];
			break;
		}
		if (textField == tf) {
			isFound=YES;
			[textField resignFirstResponder];
		}
	}
}




// Image cache concept
+(UIImage*)getCachedImage:(NSString*)imageUrl
{
	if ([Validate isNull:imageUrl]) {
        return [UIImage imageNamed:@"no_image.png"];
    }
    ImgCache *ic = [[ImgCache alloc] init];
	UIImage *img = [ic getCachedImage:[imageUrl stringByURLDecode]];
	return img;//[img roundedCornerImage:7 borderSize:0.7];
}

+(void)showServerNotFoundError{
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Cannot communicate with server"
                                                          message:@"We are currently having trouble connecting to our server. Please make sure you are able to connect to the internet. Thank you!"
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
            [alert show];
        });
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Cannot communicate with server"
                                                      message:@"We are currently having trouble connecting to our server. Please make sure you are able to connect to the internet. Thank you!"
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        [alert show];
        
    }
}

// Check for UIUserDefualt set





+ (void)setPassCode:(NSString*)value{
    // (value == default) means set by default value else set value to default user
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([value isEqualToString:@"default"]) {
        
        if ([userDefaults objectForKey:@"passCodeRewardApp"] &&
            ![Validate isNull:[userDefaults objectForKey:@"passCodeRewardApp"]]) {
            
            passCode = [userDefaults objectForKey:@"passCodeRewardApp"];
        }else{
            passCode = @"";
        }
        
    } else if([value isEqualToString:@"remove"]){
        passCode = @"";
        [userDefaults removeObjectForKey:@"passCodeRewardApp"];
    }else{
        [userDefaults setObject:value forKey:@"passCodeRewardApp"];
        passCode = value;
    }
    [userDefaults synchronize];
    
}

+(void) AlertTitle:(NSString*)title withMsg:(NSString*)msg{
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ([title isEqualToString:@"Error!"]) {
                [[[UIAlertView alloc] initWithTitle:@"Buffalo Wood!"
                                            message:msg
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles: nil] show];
            } else {
                [[[UIAlertView alloc] initWithTitle:title
                                            message:msg
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles: nil] show];
            }
        });
    } else {
        if ([title isEqualToString:@"Error!"]) {
            [[[UIAlertView alloc] initWithTitle:@"Buffalo Wood!"
                                        message:msg
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles: nil] show];
        } else {
            [[[UIAlertView alloc] initWithTitle:title
                                        message:msg
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles: nil] show];
        }
        
    }
}

+(void) AlertTitle:(NSString*)title withMsg:(NSString*)msg andDelegate:(id)delegate{
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:title
                                        message:msg
                                       delegate:delegate
                              cancelButtonTitle:@"OK"
                              otherButtonTitles: nil] show];
        });
    } else {
        [[[UIAlertView alloc] initWithTitle:title
                                    message:msg
                                   delegate:delegate
                          cancelButtonTitle:@"OK"
                          otherButtonTitles: nil] show];
        
    }
}


+ (UIImage*)getImageFromURLWithoutCache:(NSString*)URL{
    NSURL *url = [NSURL URLWithString:[URL stringByURLEncode]];
    NSData *dataImage = [[NSData alloc] initWithContentsOfURL:url];
    UIImage *image = [[UIImage alloc] initWithData:dataImage];
    return image;
}


+ (BOOL)isRetineDisplay{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        return YES;
    } else {
        // not Retine display
        return NO;
    }
}

+ (BOOL)isNetworkConnect{
    if ([UIDevice networkAvailable]){
		return YES;
	}
    
    // if there is no network
    [[[UIAlertView alloc] initWithTitle:@"Network Problem"
                                message:@"You are not connected to the network!"
                               delegate:nil
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:nil] show];
    return NO;
}

+ (BOOL)isValueNotEmpty:(NSString*)aString{
    if (aString == nil || [aString length] == 0){
        [CommonFunctions AlertTitle:@"Server Response Error"
                            withMsg:@"Please try again, server is not responding."];
        return NO;
    }
    return YES;
}


+ (void)setBadgeNumber:(NSUInteger)totalBedgeNumber{
    
    UIApplication *application = [UIApplication sharedApplication];
    
    application.applicationIconBadgeNumber = totalBedgeNumber;
    
}

+ (void)increaseBadgeNumber{
    UIApplication *application = [UIApplication sharedApplication];
    
    application.applicationIconBadgeNumber += 1;
}

+ (void)decreaseBadgeNumber{
    UIApplication *application = [UIApplication sharedApplication];
    
    application.applicationIconBadgeNumber -= 1;
}


// for base 64 encode decode
static const char _base64EncodingTable[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const short _base64DecodingTable[256] = {
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -1, -1, -2, -1, -1, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -1, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, 62, -2, -2, -2, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -2, -2, -2, -2, -2, -2,
    -2,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -2, -2, -2, -2, -2,
    -2, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2
};

+ (NSString *)decodeBase64WithStringToString:(NSString *)strBase64{
    if (![CommonFunctions decodeBase64WithString:strBase64]) {
        return @"";
    }
    return [NSString stringWithUTF8String:[[CommonFunctions decodeBase64WithString:strBase64] bytes]];
}

+ (NSData *)decodeBase64WithString:(NSString *)strBase64 {
    const char * objPointer = [strBase64 cStringUsingEncoding:NSASCIIStringEncoding];
    int intLength = strlen(objPointer);
    int intCurrent;
    int i = 0, j = 0, k;
    
    unsigned char * objResult;
    objResult = calloc(intLength, sizeof(unsigned char));
    
    // Run through the whole string, converting as we go
    while ( ((intCurrent = *objPointer++) != '\0') && (intLength-- > 0) ) {
        if (intCurrent == '=') {
            if (*objPointer != '=' && ((i % 4) == 1)) {// || (intLength > 0)) {
                // the padding character is invalid at this point -- so this entire string is invalid
                free(objResult);
                return nil;
            }
            continue;
        }
        
        intCurrent = _base64DecodingTable[intCurrent];
        if (intCurrent == -1) {
            // we're at a whitespace -- simply skip over
            continue;
        } else if (intCurrent == -2) {
            // we're at an invalid character
            NSLog(@"exit string %s",objResult);
            free(objResult);
            return nil;
        }
        
        switch (i % 4) {
            case 0:
                objResult[j] = intCurrent << 2;
                break;
                
            case 1:
                objResult[j++] |= intCurrent >> 4;
                objResult[j] = (intCurrent & 0x0f) << 4;
                break;
                
            case 2:
                objResult[j++] |= intCurrent >>2;
                objResult[j] = (intCurrent & 0x03) << 6;
                break;
                
            case 3:
                objResult[j++] |= intCurrent;
                break;
        }
        i++;
    }
    
    // mop things up if we ended on a boundary
    k = j;
    if (intCurrent == '=') {
        switch (i % 4) {
            case 1:
                // Invalid state
                free(objResult);
                return nil;
                
            case 2:
                k++;
                // flow through
            case 3:
                objResult[k] = 0;
        }
    }
    
    // Cleanup and setup the return NSData
    NSData * objData = [[NSData alloc] initWithBytes:objResult length:j];
    free(objResult);
    return objData;
}
+ (NSString *)encodeBase64WithString:(NSString *)strData {
    if (![CommonFunctions encodeBase64WithData:strData]) {
        return @"";
    }
    return [NSString stringWithUTF8String:[[CommonFunctions encodeBase64WithData:strData] bytes]];

}

+ (NSData*)encodeBase64WithData:(NSString *)strBase64 {
    const char * objRawData = [strBase64 cStringUsingEncoding:NSASCIIStringEncoding];;
    
    // Get the Raw Data length and ensure we actually have data
    NSUInteger intLength = strlen(objRawData);
    
    if (intLength == 0) return nil;
    
    unsigned char * objPointer;
    unsigned char * strResult;
    
    // Setup the String-based Result placeholder and pointer within that placeholder
    strResult = (unsigned char *)calloc((((intLength + 2) / 3) * 4) + 1, sizeof(unsigned char));
    objPointer = strResult;
    
    // Iterate through everything
    while (intLength > 2) { // keep going until we have less than 24 bits
        *objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
        *objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
        *objPointer++ = _base64EncodingTable[((objRawData[1] & 0x0f) << 2) + (objRawData[2] >> 6)];
        *objPointer++ = _base64EncodingTable[objRawData[2] & 0x3f];
        
        // we just handled 3 octets (24 bits) of data
        objRawData += 3;
        intLength -= 3;
    }
    
    // now deal with the tail end of things
    if (intLength != 0) {
        *objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
        if (intLength > 1) {
            *objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
            *objPointer++ = _base64EncodingTable[(objRawData[1] & 0x0f) << 2];
            *objPointer++ = '=';
        } else {
            *objPointer++ = _base64EncodingTable[(objRawData[0] & 0x03) << 4];
            *objPointer++ = '=';
            *objPointer++ = '=';
        }
    }
    
    // Terminate the string-based result
    *objPointer = '\0';
    
    // Return the results as an NSString object
    NSData * objData = [[NSData alloc] initWithBytes:strResult length:strlen(objRawData)];
    free(strResult);
    return objData;
//    return [NSString stringWithCString:strResult encoding:NSASCIIStringEncoding];
}

// view shaker
+ (void)shakeView:(UIView *)viewToShake{
    CGFloat t = 5.0;
    CGAffineTransform translateRight  = CGAffineTransformTranslate(CGAffineTransformIdentity, t, 0.0);
    CGAffineTransform translateLeft   = CGAffineTransformTranslate(CGAffineTransformIdentity, -t, 0.0);
    
    viewToShake.transform = translateLeft;
    
    [UIView animateWithDuration:0.07
                          delay:0.0
                        options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat
                     animations:^{[UIView setAnimationRepeatCount:2.0];
                         viewToShake.transform = translateRight;}
                     completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.05
                                  delay:0.0
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{viewToShake.transform = CGAffineTransformIdentity;}
                             completion:NULL];
        }
    }];
}

+ (void)vibrate{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

+ (void) setNetworkActivityIndicatorVisible:(BOOL)setVisible {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:setVisible];
}
+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
@end
