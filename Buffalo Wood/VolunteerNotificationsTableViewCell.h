//
//  VolunteerNotificationsTableViewCell.h
//  Buffalo Wood
//
//  Created by Gaurav on 03/09/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VolunteerNotificationsTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *lblHeading, *lblTime;
@property (nonatomic, strong) IBOutlet UIWebView *lblDetail;
@end
