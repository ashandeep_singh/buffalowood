//
//  EventDetailViewController.h
//  Buffalo Wood
//
//  Created by Gaurav on 04/09/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPImageView.h"

@interface EventDetailViewController : UIViewController


@property (nonatomic, strong) NSDictionary *dictForPage;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet GPImageView *imgViewBg;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UIButton *btnBuyTicket;
@property (strong, nonatomic) IBOutlet UITextView *lblDiscription;

- (IBAction)btnBuyTicketPressed:(id)sender;

@end
