//
//  EventDetailViewController.m
//  Buffalo Wood
//
//  Created by Gaurav on 04/09/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "EventDetailViewController.h"


@interface EventDetailViewController ()

@end

@implementation EventDetailViewController

@synthesize dictForPage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = dictForPage[@"name"];
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    

    self.navigationItem.leftBarButtonItem = [CommonFunctions getBackBtn];
    self.navigationController.navigationBarHidden = YES;
    _lblTitle.text = dictForPage[@"name"];
    _lblDiscription.text = [NSString stringWithFormat:@"%@\n\n%@",dictForPage[@"detail"],dictForPage[@"province"]];
    if(![dictForPage[@"image"] isEqualToString:@""])
    {
        [_imgViewBg setImageFromURL:dictForPage[@"image"]
          showActivityIndicator:YES
                  setCacheImage:YES];
    }    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm";
    NSDate *yourDate = [dateFormatter dateFromString:dictForPage[@"startdate"]];
    dateFormatter.dateFormat = @"MMMM dd";
    NSMutableString *startDate = [[dateFormatter stringFromDate:yourDate] mutableCopy];
    
    [startDate appendString:@" -"];
    dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm";
    yourDate = [dateFormatter dateFromString:dictForPage[@"enddate"]];
    dateFormatter.dateFormat = @"MMMM dd";
    [startDate appendString:[dateFormatter stringFromDate:yourDate]];
    
    _lblDate.text = [NSString stringWithFormat:@"%@",startDate];
    
}
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
    
	UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc]
                                         initWithTarget:self action:@selector(handleSwipeFromRight:)];
    
    gesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:gesture];
    
}
- (void)handleSwipeFromRight:(UISwipeGestureRecognizer *)recognizer
{
    [self BackBtnClicked:recognizer];
}
- (IBAction)btnBuyTicketPressed:(id)sender {
    NSLog(@"buy ticket pressed");
}
#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}
-(IBAction)BackBtnClicked:(id)sender{
//    UITabBarController *tabBarController = (UITabBarController*)[DELEGATE.window rootViewController];
//    UINavigationController *navigationController = (UINavigationController*)[tabBarController selectedViewController];
//    [navigationController  popToRootViewControllerAnimated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
