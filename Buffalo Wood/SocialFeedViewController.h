//
//  SocialFeedViewController.h
//  Buffalo Wood
//
//  Created by Gaurav on 24/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

#import "TWTAPIManager.h"
#import "TWTSignedRequest.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <MessageUI/MFMailComposeViewController.h>
//@import Social;


@interface SocialFeedViewController : UIViewController<MBProgressHUDDelegate, UIAlertViewDelegate, GPPSignInDelegate, UIActionSheetDelegate,MFMailComposeViewControllerDelegate>
{
    MBProgressHUD *HUD;
    NSMutableArray *arrayTableData;
    
    IBOutlet UITableView *table;
    IBOutlet UIButton *btnAll, *btnFb, *btnTw;
    
    NSInteger selectedCellIndex;
    ////---- g+
    GPPSignIn *signIn;
}

- (IBAction)btnEmailPressed:(id)sender;
- (IBAction)btnFBPressed:(id)sender;
- (IBAction)btnTwitterPressed:(id)sender;
- (IBAction)btnGPlusPressed:(id)sender;

@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) TWTAPIManager *apiManager;
@property (nonatomic, strong) NSArray *accounts;
@property (nonatomic, retain) NSMutableArray *arrayOfCharacters;
@property (nonatomic, retain) NSMutableDictionary *objectsForCharacters;

@end
