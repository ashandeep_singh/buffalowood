//
//  WebAboutViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 30/11/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebAboutViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *webViewUrl;
@end
