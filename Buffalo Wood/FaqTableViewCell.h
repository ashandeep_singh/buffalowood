//
//  FaqTableViewCell.h
//  SkyNanny
//
//  Created by Madhvi on 30/01/15.
//  Copyright (c) 2015 Baltech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaqTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblContent;
@property (nonatomic, strong) IBOutlet UIImageView  *imgViewArrow;
@property (nonatomic, strong) IBOutlet UIImageView  *imgViewSeparator;
@end
