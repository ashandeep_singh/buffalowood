//
//  VolunteerTableViewCell.m
//  Buffalo Wood
//
//  Created by Gaurav on 29/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "VolunteerTableViewCell.h"

@implementation VolunteerTableViewCell

@synthesize lblTitle, btnSwitchInsideTable;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
