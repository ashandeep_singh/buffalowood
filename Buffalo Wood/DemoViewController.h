//
//  DemoViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 30/10/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h> 

@interface DemoViewController : UIViewController
@property (nonatomic, retain) IBOutlet UIView* innerView;
@property (nonatomic, retain) IBOutlet UIView* MinnerView;
@property (nonatomic, retain) IBOutlet UIButton* btnView;
@property (nonatomic, retain) IBOutlet UIButton* btnNext;
@end
