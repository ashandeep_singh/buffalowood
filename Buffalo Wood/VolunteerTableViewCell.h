//
//  VolunteerTableViewCell.h
//  Buffalo Wood
//
//  Created by Gaurav on 29/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VolunteerTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UISwitch *btnSwitchInsideTable;

@end
