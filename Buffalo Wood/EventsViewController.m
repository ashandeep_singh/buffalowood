//
//  EventsViewController.m
//  Buffalo Wood
//
//  Created by Gaurav on 03/09/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "EventsViewController.h"
#import "EventsTableViewCell.h"
#import "EventDetailViewController.h"
#import "EventCategoryViewController.h"

@interface EventsViewController ()
{
    NSMutableArray *arrayEventsByCat;
  
}
@end
 
@implementation EventsViewController

NSString *catid;

@synthesize arrayEventsTableData,arrayOfCharacters,objectsForCharacters;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.self.title = @"Volunteer";
    //self.navigationItem.leftBarButtonItem = [self getBackBtn];
    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
       self.navigationController.navigationBar.translucent = YES;
    
    catid=@"";
    
    self.title = @"Schedule";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    

    
    UIBarButtonItem *leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"Categories" style:UIBarButtonItemStyleDone target:self action:@selector(getCategories)];
    
    [leftBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:@"Avenir Next" size:18], NSFontAttributeName,
                                        [CommonFunctions colorWithHexString:@"174195"], NSForegroundColorAttributeName,
                                        nil] 
                              forState:UIControlStateNormal];
  
    
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    self.navigationItem.leftItemsSupplementBackButton = NO;
    
   UIRefreshControl *refContr = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [refContr setBackgroundColor: [CommonFunctions colorWithHexString:@"174195"]];
    refContr.tintColor = [UIColor whiteColor];
        [refContr addTarget:self
                     action:@selector(getEventCatFromServer:)
                 forControlEvents:UIControlEventValueChanged];
    [table addSubview:refContr];
    [refContr setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
    
    [[refContr.subviews objectAtIndex:0] setFrame:CGRectMake(30, 0, 20, 30)];


    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *resultArray = [defaults objectForKey:@"EventsArray"];
    
    if(resultArray.count==0)
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.delegate = self;
        HUD.graceTime = 0.05f;
        
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(getEventCatFromServer:)
                       onTarget:self
                     withObject:nil
                       animated:YES];

    }
    else
    {
        if ([resultArray[@"Events"] isKindOfClass:[NSArray class]])
        {
            arrayEventsByCat = resultArray[@"Events"];
            
            [table setHidden:NO];
            
            [self setupIndexData];
        }
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(ReBindData:) name:@"dealNotification" object: nil];
 
    [table setSeparatorInset:UIEdgeInsetsZero];
    
  

}
-(void)ReBindData:(NSNotification *)notification
{
    catid = [notification object];
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    HUD.graceTime = 0.05f;
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(getEventCatFromServer:)
                   onTarget:self
                 withObject:nil
                   animated:YES];

}
-(void)getCategories
{
   EventCategoryViewController *eventCatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventCategoryVC"];
   
   [self presentModalViewController:eventCatVC animated:YES];
    
}

- (void)getEventCatFromServer:(UIRefreshControl *)refCntrl {
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"getCatEvents.php?secureKey=%@&catId=%@",SiteSecureKey,catid];
 
    NSError *error2;
    NSString *jsonStr2=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error2];
    //    NSLog(@"%@",jsonStr);
            if (!error2 && jsonStr2) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr2 error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    
                    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                    [defaults setObject:dic forKey:@"EventsArray"];
                    [defaults synchronize];

                    // play with dic : server response
                    if ([dic[@"Events"] isKindOfClass:[NSArray class]]) {
                        arrayEventsByCat = dic[@"Events"];
                        if (refCntrl) {
                            
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:@"MMM d, h:mm a"];
                            NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                            NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                                        forKey:NSForegroundColorAttributeName];
                            NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                            refCntrl.attributedTitle = attributedTitle;
                            
                            [refCntrl endRefreshing];
                            
                        }
                        
                    } else  {
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Events not found!"];
                    }
                    
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
    
    
    // call if all work is done
   
    
            if ( (arrayEventsByCat != nil && arrayEventsByCat.count > 0) || (arrayEventsByCat != nil && arrayEventsByCat.count > 0) )
            {
                [table setHidden:NO];
                [self setupIndexData];
            }
            else
            {
                [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Events not found!"];
                [table setHidden:YES];
            }
    
}

-(UIBarButtonItem*) getBackBtn{
	UIButton * backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_iPad) {
        [backBtn setFrame:CGRectMake(0.0f,0.0f,20.0f,84.0f)];
    } else {
        [backBtn setFrame:CGRectMake(0.0f,0.0f,23.0f,23.0f)];
    }
    
	[backBtn setImage:[UIImage imageNamed:@"navbar-back-icon"] forState:UIControlStateNormal];
	[backBtn addTarget:self action:@selector(BackBtnPressed) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
	return backBarButton;
}
-(void)BackBtnPressed{
    UITabBarController *tabBarController = (UITabBarController*)[DELEGATE.window rootViewController];
    UINavigationController *navigationController = (UINavigationController*)[tabBarController selectedViewController];
    [navigationController  popToRootViewControllerAnimated:YES];
}


#pragma mark - UITableView delegates
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [arrayOfCharacters count];
    
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"yyyy-MM-dd";
//    NSDate *yourDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",arrayOfCharacters[section]]];
//    dateFormatter.dateFormat = @"EEEE, dd MMMM";
//    return [NSString stringWithFormat:@"    %@",[dateFormatter stringFromDate:yourDate]];
//    
//}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSDate *yourDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",arrayOfCharacters[section]]];
        dateFormatter.dateFormat = @"EEEE, dd MMMM";

    header.textLabel.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:yourDate]];
    header.textLabel.font = [UIFont fontWithName: @"Avenir Next" size: 15.0 ];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
    
    UIImageView *imgVew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"divider-top"]];
    imgVew.frame = CGRectMake(0,0,320,1);
    [header addSubview:imgVew];
    
    UIImageView *imgVew1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"divider-top"]];
    imgVew1.frame = CGRectMake(0,33,320,1);
    [header addSubview:imgVew1];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:section]] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EventsTableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"EventsTableViewCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
//    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    //NSDictionary *dictForCell = arrayEventsTableData[indexPath.row];
    
      NSDictionary *dictForCell =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    cell.lblTitle.text = dictForCell[@"name"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm";
    NSDate *yourDate = [dateFormatter dateFromString:dictForCell[@"startdate"]];
    dateFormatter.dateFormat = @"h:mm a";
    
    cell.lblStartTime.text = [dateFormatter stringFromDate:yourDate];

    dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm";
    yourDate = [dateFormatter dateFromString:dictForCell[@"enddate"]];
    dateFormatter.dateFormat = @"h:mm a";
    cell.lblEndTime.text = [dateFormatter stringFromDate:yourDate];
    
    cell.lblLoc.text = dictForCell[@"province"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    EventDetailViewController *eventDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
    eventDetailVC.dictForPage =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
}
- (void)setupIndexData {
    
    self.arrayOfCharacters = [[NSMutableArray alloc] init];
    self.objectsForCharacters = [[NSMutableDictionary alloc] init];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSMutableArray *arrayOfNames = [[NSMutableArray alloc] init];
    
    //  NSString *numbericSection = @"#";
    NSString *firstLetter;
    
    //  NSString *frID, *frName, *frMobile;
    int valcount = 0;
    
    for (NSMutableArray *inObject in arrayEventsByCat)
    {
        NSMutableDictionary *data = [arrayEventsByCat objectAtIndex:valcount];
        
        NSLog(@"%@",[data objectForKey:@"sortdate"]);
                     
        firstLetter = [[[[data objectForKey:@"sortdate"] stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]] substringToIndex:10] uppercaseString] ;
        
        // firstLetter = [cellData.Name substringToIndex:1];
        
        // Check if it's NOT a number
        if ([formatter numberFromString:firstLetter] == nil) {
            
            if (![objectsForCharacters objectForKey:firstLetter])
            {
                
                [arrayOfNames removeAllObjects];
                [arrayOfCharacters addObject:firstLetter];
            }
            
            [arrayOfNames addObject:[arrayEventsByCat objectAtIndex:valcount]];
            valcount = valcount+1;
            
            /**
             * Need to autorelease the copy to preven potential leak. Even though the
             * arrayOfNames is released below it still has a retain count of +1
             */
            [objectsForCharacters setObject:[arrayOfNames copy] forKey:firstLetter];
            
        }
    }
    [table reloadData];
}



#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
