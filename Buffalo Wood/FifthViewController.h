//
//  FifthViewController.h
//  Buffalo Wood
//
//  Created by Gaurav on 24/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface FifthViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MFMailComposeViewControllerDelegate>
{
    IBOutlet UITableView *table;
    NSArray *arrayTableData;
}
@end
