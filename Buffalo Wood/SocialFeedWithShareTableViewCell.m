//
//  SocialFeedWithShareTableViewCell.m
//  Buffalo Wood
//
//  Created by Gaurav on 24/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "SocialFeedWithShareTableViewCell.h"

@implementation SocialFeedWithShareTableViewCell

@synthesize imgViewType, imgView,imgstrip;

@synthesize lblTitle, lblDate;

@synthesize txtViewDiscription;

@synthesize lblShare;

@synthesize btnEmail, btnFB, btnTwitter, btnGPlus;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
//    [lblShare setBackgroundColor:UIColorFromRedGreenBlue(11, 40, 118)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
