//
//  EventsViewController.h
//  Buffalo Wood
//
//  Created by Gaurav on 03/09/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsViewController : UIViewController<UITableViewDelegate,MBProgressHUDDelegate>
{
    IBOutlet UITableView *table;
    MBProgressHUD *HUD;
    
}
@property (nonatomic,strong)  NSArray *arrayEventsTableData;
@property (nonatomic, retain) NSMutableArray *arrayOfCharacters;
@property (nonatomic, retain) NSMutableDictionary *objectsForCharacters;


@end
