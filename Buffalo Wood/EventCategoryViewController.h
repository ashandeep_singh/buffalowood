//
//  EventCategoryViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 09/09/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCategoryViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
     IBOutlet UITableView *table;
}
@end
