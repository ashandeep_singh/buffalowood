//
//  MoreViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 06/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import "MoreViewController.h"
#import "FifthViewController.h"
#import "TicketViewController.h"
#import "SocialFeedViewController.h"
#import "VideoViewController.h"
#import "PhotosViewController.h"

@interface MoreViewController ()

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"More";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    
    
    arrayTableData = @[@[@{@"title": @"Photos",@"image":@"menu-photos-icon-active.png",@"action":@"SocialFeedViewController"},
                         @{@"title": @"Videos",@"image":@"menu-videos-icon.png",@"action":@"VideosViewController"},@{@"title": @"Social Feed",@"image":@"menu-feed-icon-active.png",@"action":@"SocialFeedViewController"}],@[@{@"title": @"About",@"image":@"menu-help-icon.png",@"action":@"SocialFeedViewController"},
                                                                                                                                  @{@"title": @"Buying Tickets",@"image":@"menu-tickets-icon.png",@"action":@"SocialFeedViewController"}],];
    
    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [CommonFunctions colorWithHexString:@"174195"], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [self.navigationController.navigationBar setTintColor:[CommonFunctions colorWithHexString:@"174195"]];
    


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table Datasource and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [arrayTableData count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section==1)
    {
        return @"";
    }
    else if(section==2)
    {
        return @"";
    }
    else
    {
        return @"";
    }
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    if(section==1 || section==2)
    {
        label.frame = CGRectMake(16, 0, 320, 20);
    }
    else
    {
        label.frame = CGRectMake(16, 5, 320, 20);
    }
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor grayColor];
    label.font = [UIFont fontWithName: @"Avenir Next" size: 17.0 ];
    label.text = sectionTitle;
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableData[section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"FifthTableViewCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    NSDictionary *dictForCell = [arrayTableData[indexPath.section] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = dictForCell[@"title"];
    UIImage *imageName = [UIImage imageNamed:dictForCell[@"image"]];
    cell.imageView.image = imageName;
    
    UIFont *myFont = [ UIFont fontWithName: @"Avenir Next" size: 16.0 ];
    cell.textLabel.font  = myFont;
    
//    if(indexPath.section==1)
//    {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        
//    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            PhotosViewController *eventCatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotosVC"];
            
            [self.navigationController pushViewController:eventCatVC animated:YES];
        }
        else if(indexPath.row==1)
        {
            VideoViewController *eventCatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoVC"];
            
            [self.navigationController pushViewController:eventCatVC animated:YES];
        }
        else if(indexPath.row==2)
        {
            SocialFeedViewController *eventCatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SocialFeedViewController"];
            
            [self.navigationController pushViewController:eventCatVC animated:YES];
        }
    
    }
    else if(indexPath.section==1)
    {
        if(indexPath.row==0)
        {
            FifthViewController *eventCatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FifthViewController"];
            
            [self.navigationController pushViewController:eventCatVC animated:YES];
        }
        else
        {
            TicketViewController *spVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TicketsVC"];
            
            [self.navigationController pushViewController:spVC animated:YES];
        }
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
