//
//  FaqTableViewCell.m
//  SkyNanny
//
//  Created by Madhvi on 30/01/15.
//  Copyright (c) 2015 Baltech. All rights reserved.
//

#import "FaqTableViewCell.h"

@implementation FaqTableViewCell
@synthesize lblContent,lblTitle,imgViewArrow,imgViewSeparator;
- (void)awakeFromNib {
    // Initialization code
    
    [lblTitle setFont:[UIFont fontWithName:@"AvenirNext-Regular" size:14.0f]];
    lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
    lblTitle.numberOfLines = 4;
    [lblTitle setTextColor:[UIColor blackColor]];
    
    [lblContent setFont:[UIFont fontWithName:@"AvenirNext-Regular" size:14.0f]];
    lblContent.lineBreakMode = NSLineBreakByWordWrapping;
    lblContent.numberOfLines = 10;
    [lblContent setTextColor:[UIColor blackColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
