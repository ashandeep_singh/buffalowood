//
//  TicketViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 06/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import "TicketViewController.h"
#import "FeactureTableViewCell.h"
#import "AsyncImageView.h"
#import "FaqViewController.h"

@interface TicketViewController ()
{
    NSArray *arrayTKTableData;
}
@end

@implementation TicketViewController
@synthesize arrayTKTableData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Buy Tickets";
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    
    UIBarButtonItem *rightBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"FAQ" style:UIBarButtonItemStyleDone target:self action:@selector(getFaq)];
    
    [rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIFont fontWithName:@"Avenir Next" size:18], NSFontAttributeName,
                                               [CommonFunctions colorWithHexString:@"174195"], NSForegroundColorAttributeName,
                                               nil]
                                     forState:UIControlStateNormal];
    
    
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    //set back button arrow color
//    UIBarButtonItem *leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"< Back" style:UIBarButtonItemStyleDone target:self action:@selector(getFaq)];
//    
//    [leftBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                               [UIFont fontWithName:@"Avenir Next" size:18], NSFontAttributeName,
//                                               [CommonFunctions colorWithHexString:@"174195"], NSForegroundColorAttributeName,
//                                               nil]
//                                     forState:UIControlStateNormal];
//    
//    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
//    self.navigationItem.leftItemsSupplementBackButton = NO;
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *resultArray = [defaults objectForKey:@"TktsArray"];
    
    if(resultArray.count==0)
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.delegate = self;
        HUD.graceTime = 0.05f;
        
        [table setSeparatorInset:UIEdgeInsetsZero];
        
        arrayTKTableData = [NSArray new];
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(fetchDataForTickets:)
                       onTarget:self
                     withObject:nil
                       animated:YES];
    }
    else
    {
        if ([resultArray[@"Tickets"] isKindOfClass:[NSArray class]]) {
            arrayTKTableData = resultArray[@"Tickets"];
           [self seflayout];
            [table reloadData];
        }
    }
    
    
    
    UIRefreshControl *refContr = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [refContr setBackgroundColor: [CommonFunctions colorWithHexString:@"174195"]];
    refContr.tintColor = [UIColor whiteColor];
    [refContr addTarget:self
                 action:@selector(fetchDataForTickets:)
       forControlEvents:UIControlEventValueChanged];
    [scrollview addSubview:refContr];
    [refContr setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
    
    [[refContr.subviews objectAtIndex:0] setFrame:CGRectMake(30, 0, 20, 30)];
    
   // [table setContentSize:CGSizeMake(320,800)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchDataForTickets:(UIRefreshControl *)refCntrl {
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"getTickets.php?secureKey=%@",SiteSecureKey];
    NSLog(@"%@",strUrl);
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (!error && jsonStr) {
        SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
        NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
        NSLog(@"%@",dic);
#endif
        if (dic) {
            
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            [defaults setObject:dic forKey:@"TktsArray"];
            [defaults synchronize];
            
            // play with dic : server response
            if ([dic[@"Tickets"] isKindOfClass:[NSArray class]]) {
                arrayTKTableData = dic[@"Tickets"];
                if (arrayTKTableData.count == 0){
                    [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"There is no item." andDelegate:self];
                } else {
                    if (refCntrl) {
                        
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"MMM d, h:mm a"];
                        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                                    forKey:NSForegroundColorAttributeName];
                        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                        refCntrl.attributedTitle = attributedTitle;
                        
                        [refCntrl endRefreshing];
                        
                    }
                    
                    [self seflayout];
                    [table reloadData];
                }
            } else {
                [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
            }
        } else {
            [CommonFunctions showServerNotFoundError];
        }
    } else {
        [CommonFunctions showServerNotFoundError];
    }
}
#pragma mark - UITableView delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
   return [arrayTKTableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FeactureTableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"TicketCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[FeactureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    NSDictionary *dictForCell =  [arrayTKTableData objectAtIndex:indexPath.row];
    
    cell.lblName1.text = dictForCell[@"title"];
    cell.lblName2.text =[NSString stringWithFormat:@"$%@",dictForCell[@"price"]];
    
    cell.imgView1.image = [UIImage imageNamed:@"AppIcon87x87.png"];
    
    if(![[dictForCell objectForKey:@"image"] isEqualToString:@""])
    {
        NSURL *imageviewurl1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@images-tickets/%@",SiteURL,[dictForCell objectForKey:@"image"]]];
    
        cell.imgView1.imageURL = imageviewurl1;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
      NSDictionary *dictForCell =  [arrayTKTableData objectAtIndex:indexPath.row];
    
   
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dictForCell objectForKey:@"url"]]];
}

-(void)getFaq
{
    FaqViewController *faqViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FaqVC"];
    
    [self.navigationController pushViewController:faqViewController animated:YES];
    
}
-(void)seflayout
{
    CGRect tableFrame = [table frame];
    
    tableFrame.size.height = 65*arrayTKTableData.count;
    
    [table setFrame:tableFrame];

    if(tableFrame.size.height>200)
    {
        [scrollview setContentSize:CGSizeMake(320,350+tableFrame.size.height)];
    }
    else
    {
        [scrollview setContentSize:CGSizeMake(320,350+200)];
    }
}

@end
