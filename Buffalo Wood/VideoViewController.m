//
//  VideoViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 23/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import "VideoViewController.h"
#import "FeactureTableViewCell.h"
#import "AsyncImageView.h"
#import "VideoDetailViewController.h"

@interface VideoViewController ()
{
    NSArray *arrayYTableData;
}
@end


@implementation VideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Videos";
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
       
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *resultArray = [defaults objectForKey:@"videosArray"];
    
    if(resultArray.count==0)
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.delegate = self;
        HUD.graceTime = 0.05f;
        
        [table setSeparatorInset:UIEdgeInsetsZero];
        
        arrayYTableData = [NSArray new];
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(fetchDataForTickets:)
                       onTarget:self
                     withObject:nil
                       animated:YES];
    }
    else
    {
        if ([resultArray[@"images"] isKindOfClass:[NSArray class]]) {
            arrayYTableData = resultArray[@"images"];
            [table reloadData];
        }
    }
    
    
    
    UIRefreshControl *refContr = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [refContr setBackgroundColor: [CommonFunctions colorWithHexString:@"174195"]];
    refContr.tintColor = [UIColor whiteColor];
    [refContr addTarget:self
                 action:@selector(fetchDataForTickets:)
       forControlEvents:UIControlEventValueChanged];
    [scrollview addSubview:refContr];
    [refContr setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
    
    [[refContr.subviews objectAtIndex:0] setFrame:CGRectMake(30, 0, 20, 30)];
    
    // [table setContentSize:CGSizeMake(320,800)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchDataForTickets:(UIRefreshControl *)refCntrl {
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"getVideos.php?secureKey=%@",SiteSecureKey];
    NSLog(@"%@",strUrl);
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (!error && jsonStr) {
        SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
        NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
        NSLog(@"%@",dic);
#endif
        if (dic) {
            
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            [defaults setObject:dic forKey:@"VideosArray"];
            [defaults synchronize];
            
            // play with dic : server response
            if ([dic[@"Videos"] isKindOfClass:[NSArray class]]) {
                arrayYTableData = dic[@"Videos"];
                if (arrayYTableData.count == 0){
                    [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"There is no item." andDelegate:self];
                } else {
                    if (refCntrl) {
                        
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"MMM d, h:mm a"];
                        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                                    forKey:NSForegroundColorAttributeName];
                        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                        refCntrl.attributedTitle = attributedTitle;
                        
                        [refCntrl endRefreshing];
                        
                    }
                    [table reloadData];
                }
            } else {
                [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
            }
        } else {
            [CommonFunctions showServerNotFoundError];
        }
    } else {
        [CommonFunctions showServerNotFoundError];
    }
}
#pragma mark - UITableView delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [arrayYTableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FeactureTableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"VideoCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[FeactureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    NSDictionary *dictForCell =  [arrayYTableData objectAtIndex:indexPath.row];
    
    cell.lblName1.text = dictForCell[@"title"];
    cell.lblName2.text =dictForCell[@"added_by"];
    
    cell.imgView1.image = [UIImage imageNamed:@"AppIcon87x87.png"];
    
    if(![[dictForCell objectForKey:@"video_thumbnail"] isEqualToString:@""])
    {
        NSString *trimmedString = [[dictForCell objectForKey:@"video_thumbnail"]  stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        
        NSString *newCountryString =[trimmedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *imageviewurl1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@",newCountryString]];
        
        cell.imgView1.imageURL = imageviewurl1;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VideoDetailViewController *eventDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoDetailVC"];
    eventDetailVC.dictForPage =  [arrayYTableData objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:eventDetailVC animated:YES];
   
}


@end
