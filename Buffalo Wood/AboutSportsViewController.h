//
//  AboutSportsViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 22/11/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutSportsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
