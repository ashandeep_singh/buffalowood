//
//  VolunteerViewController.m
//  Buffalo Wood
//
//  Created by Gaurav on 29/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "VolunteerViewController.h"

#import "VolunteerTableViewCell.h"


const static CGFloat kJVFieldFontSize = 16.0f;
const static CGFloat kJVFieldFloatingLabelFontSize = 11.0f;

@interface VolunteerViewController ()

@end

@implementation VolunteerViewController

@synthesize txtFieldName, txtFieldEmail, txtFieldPhone;

NSArray *arrayForTableContentOfVolunteerVC;

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    self.title = @"Volunteer";
    
    _lblGetInTheGames.textColor =  [CommonFunctions colorWithHexString:@"B0B0B7"];
     _lblContactDetails.textColor =  [CommonFunctions colorWithHexString:@"B0B0B7"];
     _lblAreasOfInterest.textColor =  [CommonFunctions colorWithHexString:@"B0B0B7"];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    
    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    
    // Do any additional setup after loading the view.
    //self.navigationItem.leftBarButtonItem = [CommonFunctions getBackBtn];
   // self.navigationController.navigationBar.barTintColor = UIColorFromRedGreenBlue(22, 65, 149);
    
    arrayForTableContentOfVolunteerVC = @[[@{@"title": @"Events",@"status":@"1"}mutableCopy],
                                          [@{@"title": @"Culture and Ceremonies",@"status":@"1"}mutableCopy],
                                          [@{@"title": @"Media and Promotions",@"status":@"1"}mutableCopy],
                                          [@{@"title": @"Mascot Program",@"status":@"1"}mutableCopy],
                                          [@{@"title": @"Sports",@"status":@"1"}mutableCopy],
                                          [@{@"title": @"Venues",@"status":@"1"}mutableCopy],
                                          [@{@"title": @"Other",@"status":@"1"}mutableCopy]];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.graceTime = 0.05f;
    HUD.delegate = self;
    
    [_imgViewTop setBackgroundColor:UIColorFromRedGreenBlue(22, 65, 149)];
    
    [_scrollView setContentInset:UIEdgeInsetsMake(0, 0, 1350, 0)];
    
    // JVTextField
    UIColor *floatingLabelColor = [UIColor lightGrayColor];
    
    txtFieldName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Name", @"")
                                                                       attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    txtFieldName.font = [UIFont systemFontOfSize:kJVFieldFontSize];
    txtFieldName.floatingLabel.font = [UIFont boldSystemFontOfSize:kJVFieldFloatingLabelFontSize];
    txtFieldName.floatingLabelTextColor = floatingLabelColor;
    txtFieldName.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtFieldName.tintColor = [UIColor blueColor];
    
    txtFieldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", @"")
                                                                         attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    txtFieldEmail.font = [UIFont systemFontOfSize:kJVFieldFontSize];
    txtFieldEmail.floatingLabel.font = [UIFont boldSystemFontOfSize:kJVFieldFloatingLabelFontSize];
    txtFieldEmail.floatingLabelTextColor = floatingLabelColor;
    txtFieldEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    txtFieldPhone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Phone Number", @"")
                                                                         attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    txtFieldPhone.font = [UIFont systemFontOfSize:kJVFieldFontSize];
    txtFieldPhone.floatingLabel.font = [UIFont boldSystemFontOfSize:kJVFieldFloatingLabelFontSize];
    txtFieldPhone.floatingLabelTextColor = floatingLabelColor;
    txtFieldPhone.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
}
#pragma mark - btn pressed
- (IBAction)btnVolunteerPressed:(id)sender {
    if ([self validateForm]) {
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(sendDataToServer)
                       onTarget:self
                     withObject:nil
                       animated:YES];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if (isUserLogin) {
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"VolunteerNotificationsViewController"]
                                             animated:YES];
    }

}
- (BOOL)validateForm {
    if ([Validate isNull:txtFieldName.text]) {
        [CommonFunctions AlertTitle:@"Error" withMsg:@"Please enter valid name"];
    } else if (![Validate isValidEmailId:txtFieldEmail.text]) {
        [CommonFunctions AlertTitle:@"Error" withMsg:@"Please enter valid email ID"];
    } else {
        return YES;
    }
    return NO;
}
- (void)sendDataToServer{
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"volunteersignup.php?secureKey=%@&name=%@&email=%@&phone=%@&event=%@&cultureandceremonies=%@&mediaandpromotions=%@&mascotprogram=%@&sports=%@&venues=%@&other=%@&device_type=%@&device_id=%@&login_type=0&password=",SiteSecureKey,txtFieldName.text,txtFieldEmail.text,txtFieldPhone.text,
                      [arrayForTableContentOfVolunteerVC[0] objectForKey:@"status"],
                      [arrayForTableContentOfVolunteerVC[1] objectForKey:@"status"],
                      [arrayForTableContentOfVolunteerVC[2] objectForKey:@"status"],
                      [arrayForTableContentOfVolunteerVC[3] objectForKey:@"status"],
                      [arrayForTableContentOfVolunteerVC[4] objectForKey:@"status"],
                      [arrayForTableContentOfVolunteerVC[5] objectForKey:@"status"],
                      [arrayForTableContentOfVolunteerVC[6] objectForKey:@"status"],
                      @"iPhone",appDeviceToken];
    NSLog(@"%@",strUrl);
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"Message"] isEqualToString:@"1"]) {
                        [CommonFunctions userLoginDetail:@{@"name":txtFieldName.text,@"id":txtFieldEmail.text}];
                        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"VolunteerNotificationsViewController"]
                                                             animated:YES];
                    } else if ([dic[@"Message"] isEqualToString:@"-1"]) {
                        [CommonFunctions userLoginDetail:@{@"name":txtFieldName.text,@"id":txtFieldEmail.text}];
                        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"VolunteerNotificationsViewController"]
                                                             animated:YES];
                    } else {
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
}

- (IBAction)btnSwitchInsideTablePressed:(id)sender {
    UISwitch *btnSwitch = (UISwitch*)sender;
    NSMutableDictionary *dict = arrayForTableContentOfVolunteerVC[btnSwitch.tag];
    [dict setObject:btnSwitch.on?@"1":@"0" forKey:@"status"];
}

#pragma mark - UITableView delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex{
    return arrayForTableContentOfVolunteerVC.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    VolunteerTableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"VolunteerTableViewCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[VolunteerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    
    NSDictionary *dictForCell = arrayForTableContentOfVolunteerVC[indexPath.row];
    
    cell.lblTitle.text = dictForCell[@"title"];
    
    cell.btnSwitchInsideTable.tag = indexPath.row;
    if ([dictForCell[@"status"] isEqualToString:@"1"]) {
        cell.btnSwitchInsideTable.selected = YES;
    } else {
        cell.btnSwitchInsideTable.selected = NO;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    VolunteerTableViewCell *cell = (VolunteerTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.btnSwitchInsideTable.on = !cell.btnSwitchInsideTable.on;
    [self btnSwitchInsideTablePressed:cell.btnSwitchInsideTable];
}


#pragma mark - UITextField Delegates
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [CommonFunctions scrollViewToCenterOfScreen:textField onScrollView:_scrollView];
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    //[CommonFunctions becomeNextFirstResponder:_scrollView :textField];
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
