//
//  VolunteerViewController.h
//  Buffalo Wood
//
//  Created by Gaurav on 29/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface VolunteerViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewTop;
@property (weak, nonatomic) IBOutlet UILabel *lblVolunteer;
@property (weak, nonatomic) IBOutlet UILabel *lblGetInTheGames;
@property (weak, nonatomic) IBOutlet UILabel *lblVolunteerDiscription;

@property (weak, nonatomic) IBOutlet UILabel *lblContactDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtFieldName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtFieldEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtFieldPhone;

@property (weak, nonatomic) IBOutlet UILabel *lblAreasOfInterest;
@property (weak, nonatomic) IBOutlet UITableView *tableAreaOfInterest;
- (IBAction)btnSwitchInsideTablePressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnVolunteer;
- (IBAction)btnVolunteerPressed:(id)sender;


@end
