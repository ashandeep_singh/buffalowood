//
//  FaqViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 08/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import "FaqViewController.h"
#import "FaqTableViewCell.h"

#define FONT_SIZE 14.0f
#define CELL_CONTENT_WIDTH 300.0f
#define CELL_CONTENT_MARGIN 10.0f

@interface FaqViewController ()
{
    NSIndexPath *selectedIndex;
    NSArray *arrayTableData;
    NSIndexPath *currentIndex;
    CGSize constraint;
}
@end

@implementation FaqViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"FAQ";
   self.view.backgroundColor = UIColorFromRGB(0xefeff4);
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
  
//    
//    tblFaq.rowHeight = 0;
//    selectedIndex = [NSIndexPath indexPathForRow:-1 inSection:-1];
//    arrayTableData = @[@{@"title":@"Frequently Asked Questions",
//                         @"data":@[@{@"Question":@"How can I buy tickets for the Western Canada Summer Games 2015 Wood Buffalo?",
//                                     @"Answer":@"<html><body font='AvenirNext-Regular'>a.<b>On-Line at <a href='www.macdonaldisland.ca'>www.macdonaldisland.ca<a> or <a href='www.2015woodbuffalo.com'>www.2015woodbuffalo.com</a></b>Follow the Buy Tickets button from each event to place your order. American Express, MasterCard and VISA accepted as payment for online purchases.\nb.Charge by Phone (toll-free 1-888-281-6477)</body></html>"},
//                                   
//                                   @{@"Question":@"So we can only buy a skynanny.net device from www.skynanny.net shopping cart?",
//                                     @"Answer":@"Yes."}
//                                   ,
//                                   @{@"Question":@"When I buy a skynanny, what comes in the box?",
//                                     @"Answer":@"A skynanny.net device, charging pad for recharging your device, clothing pockets so the device can be attached to clothing, key-ring and a lanyard."}
//                                   ,
//                                   @{@"Question":@"If we need support, can we contact skynanny.net?",
//                                     @"Answer":@"Yes, you can email us at info@skynanny.net or call our support line on 1300SKYNANNY (1300 7596 2669)."}
//                                   ,
//                                   @{@"Question":@"Are there any on-going costs?",
//                                     @"Answer":@"Yes. The device contains a sim card which once activated costs AUD $9.90 per month for network access."}
//                                   ,
//                                   @{@"Question":@"Is there a contract with the monthly fee?",
//                                     @"Answer":@"Yes, the contract is for 12 months and can be renewed. skynanny.net will send you a reminder email one month prior to contract end."}
//                                   ,
//                                   @{@"Question":@"Is skynanny.net really the most unique locating device in the world?",
//                                     @"Answer":@"Yes. There is no device with our features in the world, which is something we’re proud of."},
//                                   @{@"Question":@"So skynanny.net can be used on anything?",
//                                     @"Answer":@"Yes. Uses of this device are only limited by your imagination."}]
//                         }];
//    tblFaq.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
    
    NSString *myDescriptionVal =@"<p><b>Frequently Asked Questions</b></p><b>1. How can I buy tickets for the Western Canada Summer Games 2015 Wood Buffalo?</b><ol>a. <b>On-Line at <a href='http://www.macdonaldisland.ca'>www.macdonaldisland.ca</a> or <a href='http://www.2015woodbuffalo.com'>www.2015woodbuffalo.com</a></b><br/>Follow the Buy Tickets button from each event to place your order.  American Express, MasterCard and VISA accepted as payment for online purchases.</ol><ol>b. <b>Charge by Phone (toll-free 1-888-281-6477)</b><br/>Available Monday to Friday, 11:00 a.m. to 5:00 p.m. however hours do vary according to 'on-sale' and event days. Tickets may be purchased by American Express, MasterCard or VISA.</ol><ol>c.<b> Visit the MacDonald Island Park Box Office and other Regional Recreation Corporation locations</b><br/>Regular box office hours are Monday to Friday, 5am – 11pm, weekends and stat holidays 7am-10pm.</ol><b>2. What is included in my tickets?</b></ol><ol>a. <b>2 Phase Super Pass:</b> entrance to all sporting events, except gold medal rounds of basketball, beach volleyball, indoor volleyball, and baseball. General seating only.</ol><ol>b. <b>Phase 1 and Phase 2 Pass:</b> entrance to all sporting events except gold medal rounds of basketball, beach volleyball, indoor volleyball, and baseball. General seating only. Phase 1 is from August 7-11. Phase 2 is August 12-16.</ol><ol>c.<b> Opening Ceremonies:</b> entrance to and seating in the seat assigned to the ticket you hold at the Opening Ceremonies held August 7, 2015.</ol><ol>d.<b> Closing Ceremonies:</b> entrance to and seating in the seat assigned to the ticket you hold at the Closing Ceremonies held August 16, 2015.</ol><ol>e.<b> Single Day Event Passes:</b> entrance into a specific sport for that specific day with general seating.</ol><b>3. Are there any discounts for children, toddlers, seniors, military?</b></ol><ol>a. Children 4-12, seniors over 65 and military personnel receive a 20% discount on all tickets. For seniors and military personnel, please be prepared to show valid identification. Anyone under the age of four will need to be on the lap of a parent or guardian.</ol><b>4. What is included in my 2 Phase Super Pass or Phase 1 and Phase 2 passes?</b></ol><ol>a.<b> 2 Phase Superpass:</b> entrance to all sporting events except gold medal rounds of basketball, beach volleyball, indoor volleyball, and baseball. General seating only.</ol><ol>b.<b> Phase 1 and Phase 2 Pass:</b> entrance to all sporting events except gold medal rounds of basketball, beach volleyball, indoor volleyball, and baseball. General seating only.</ol><b>5. Where can I pick my tickets up during the Games?</b></ol><ol>a. <b>Venue based Ticket Kiosk Locations</b></ol><ol>b. All Ticket Kiosks will sell passes and tickets for the current events for purchase, if available except the airport kiosk will only exchange voucher for pass bracelets.</ol><ol>Ticket Kiosk locations will be as follows:</ol><ol>1. YMM International Airport (Voucher exchange only. No sales.)</ol><ol>2. Suncor Community Leisure Centre</ol><ol>3. Syne Park</ol><ol>4. Syncrude Sport and Wellness Centre</ol><ol>5. Timberlea Community Park (Phase 1 only)</ol><ol>6. Syncrude Athletic Park (Phase 2 only)</ol><ol>7. And all Regional Recreation Box Offices</ol><b>6. Can I purchase single event passes?</b><ol>a. Yes! Single event passes are available and will grant you access to a single sport for the entire day you have purchased.</ol><b>7. Can I purchase daily passes?</b><ol>a. No we do not have day passes</ol><b>8. If I purchase a 2 week pass or phase 1/2 pass, where do I get my wristbands from?</b><ol>Ticket Kiosk locations will be as follows::</ol><ol>1. YMM International Airport (Voucher exchange only. No sales.)</ol><ol>2. Suncor Community Leisure Centre</ol><ol>3. Syne Park</ol><ol>4. Syncrude Sport and Wellness Centre</ol><ol>5. Timberlea Community Park (Phase 1 only)</ol><ol>6. Syncrude Athletic Park (Phase 2 only)</ol><ol>7. And all RRC Box Offices</ol><b>9. Is there stroller parking?</b><ol>a. Yes, there will be stroller parking available at all venues, except Opening and Closing ceremonies.</ol><b>10. When do gold medal games tickets go on sale?</b><ol>a. Medal round tickets will be released in three phases: April 29, 2015, July 8, 2015, and the final release will be four hours prior to each gold medal match.</ol><b>11. What events have special gold medal games tickets?</b><ol>a. Basketball</ol><ol>b. Beach Volleyball</ol><ol>c. Indoor Volleyball</ol><ol>d. Baseball</ol><b>12. What hours of operations will the ticketing kiosks be open?</b><ol>a. 8 a.m. until 8 p.m. during August 7-16, unless otherwise posted. </ol><b>13. Why are you releasing tickets in phases over time?</b><ol>a. This phased-in approach allows for the increase or decrease of passes depending on sales.</ol><ol>b. This phased in approach also is required to ensure athletes and their families who do not secure a spot on their team until later in the season, still have the ability to purchase tickets. </ol><b>14. Is there parking available at every venue?</b> <ol>a. More parking details will become available closer to the Games. Please check <a href='http://www.2015woodbuffalo.com'>www.2015woodbuffalo.com</a> in the coming months for full parking information.</ol>Please follow us on social media and check our website for more information about release dates for tickets, parking information and to get involved with the Games!<p><a href='http://www.2015woodbuffalo.com'>www.2015woodbuffalo.com</a></p><p>Twitter: @2015woodbuffalo</p><p>Facebook: Western Canada Summer Games 2015 Wood Buffalo</p>";
    
    
    NSString *myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                                   "<head> \n"
                                   "<style type=\"text/css\"> \n"
                                   "span {font-family:AvenirNext-Regular;font-size:14px;}ol {padding-left:10px;font-family:AvenirNext-Regular;font-size:14px;text-align:left;}p {font-family:AvenirNext-Regular;font-size:14px;text-align:center;}a {text-decoration:none;}\n"
                                   "</style> \n"
                                   "</head> \n"
                                   "<body style=\"font-family:AvenirNext-Regular;font-size:14px;color:black;\">%@</body> \n"
                                   "</html>",myDescriptionVal];
    
    
    [wbFaq loadHTMLString:myDescriptionHTML baseURL:nil];
    
    
}

#pragma mark - UITable view delegate and data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [arrayTableData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 35.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
- (UIView*) tableView:(UITableView*)tableView
viewForHeaderInSection:(NSInteger)section {
    
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
    
    UILabel *lblSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(15,8, 300, 25)];
    [lblSectionTitle setFont:[UIFont fontWithName:@"AvenirNext-Regular" size:FONT_SIZE]];
    [lblSectionTitle setTextColor:[UIColor redColor]];
    [lblSectionTitle setText:[arrayTableData[section] objectForKey:@"title"]];
    
    [sectionView addSubview:lblSectionTitle];
    
    return sectionView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Display recipe in the table cell
    NSArray *arraySelected = nil;
    arraySelected = [arrayTableData[indexPath.section] objectForKey:@"data"];
    
    currentIndex = indexPath;
    NSDictionary *dictForCell = arraySelected[indexPath.row];
    
 //   NSAttributedString *html = dictForCell[@"Answer"];
    
    constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:@"AvenirNext-Regular" size:FONT_SIZE],NSFontAttributeName,nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:dictForCell[@"Answer"] attributes:attributesDictionary];
  
    CGRect rect = [string boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil];
    
//    CGSize size = [html boundingRectWithSize:constraint
//                                     options:NSStringDrawingUsesLineFragmentOrigin
//                                  attributes:@{NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Regular" size:FONT_SIZE]}
//                                     context:nil].size;
    
    CGSize size =rect.size;
    
    if (indexPath.section == selectedIndex.section && indexPath.row == selectedIndex.row) {
        return MAX(size.height,21.0f) + 45.0f;
    }
    return 65.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arraySelected = nil;
    arraySelected = [arrayTableData[section] objectForKey:@"data"];
    return arraySelected.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"FaqTableViewCell";
    FaqTableViewCell *cell = (FaqTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[FaqTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    //cell.contentView.backgroundColor = [UIColor greenColor];
    
    // Display recipe in the table cell
    NSArray *arraySelected = [arrayTableData[indexPath.section] objectForKey:@"data"];
    
    
    NSDictionary *dictForCell = arraySelected[indexPath.row];
    
    // Question
    
    cell.lblTitle.text = dictForCell[@"Question"];
    
    
    [cell.lblTitle setNumberOfLines:0];
    
    [cell.lblTitle setFrame:CGRectMake(CELL_CONTENT_MARGIN, 5, CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), MAX(60.0f,21.0f))];
    [cell.lblTitle sizeToFit];
    
    // Answer
    
 //   NSString *html = dictForCell[@"Answer"];
    
    
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:@"AvenirNext-Regular" size:FONT_SIZE],NSFontAttributeName,nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:dictForCell[@"Answer"] attributes:attributesDictionary];
    
    CGRect rect = [string boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil];
    
    //    CGSize size = [html boundingRectWithSize:constraint
    //                                     options:NSStringDrawingUsesLineFragmentOrigin
    //                                  attributes:@{NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Regular" size:FONT_SIZE]}
    //                                     context:nil].size;
    
    CGSize size =rect.size;

    
    NSString * htmlString = dictForCell[@"Answer"];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    cell.lblContent.attributedText = attrStr;
    
    [cell.lblContent setNumberOfLines:0];
    
    if (tableView.editing) {
        cell.imgViewArrow.image = nil;
    } else if (indexPath.section == selectedIndex.section && indexPath.row == selectedIndex.row) {
        cell.imgViewArrow.image = [UIImage imageNamed:@"icon-arrow-up"];
        [cell.imgViewSeparator setFrame:CGRectMake(0,  MAX(size.height - 21.0f ,21.0f), 320, 1)];
        
        [cell.lblContent setFrame:CGRectMake(CELL_CONTENT_MARGIN,cell.lblTitle.frame.origin.y + cell.lblTitle.frame.size.height, CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), MAX(size.height , 21.0f))];
    } else {
        cell.imgViewArrow.image = [UIImage imageNamed:@"icon-arrow-down"];
        [cell.imgViewSeparator setFrame:CGRectMake(0,MAX(55.0f ,21.0f), 320, 1)];
        [cell.lblContent setFrame:CGRectMake(CELL_CONTENT_MARGIN,cell.lblTitle.frame.origin.y + cell.lblTitle.frame.size.height + 35, CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), MAX(size.height , 21.0f))];
    }
    
    
    [cell.lblContent sizeToFit];
    
    
    //pr(@"title -%f , content-%f ", cell.lblTitle.bounds.size.height , cell.lblContent.bounds.size.height);
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *indexArray;
    if (indexPath.section == selectedIndex.section && indexPath.row == selectedIndex.row) {
        selectedIndex = [NSIndexPath indexPathForRow:-1 inSection:-1];
        indexArray = @[indexPath];
    } else {
        if (selectedIndex.row != -1) {
            indexArray = @[indexPath, selectedIndex];
        } else {
            indexArray = @[indexPath];
        }
        selectedIndex = indexPath;
    }
    
    //for animation on cell
    [tableView reloadRowsAtIndexPaths:indexArray
                     withRowAnimation:UITableViewRowAnimationFade];
    
}



#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

