//
//  DeleteImageViewController.m
//  testblur
//
//  Created by Ashandeep Singh on 24/02/15.
//  Copyright (c) 2015 Baltech. All rights reserved.
//

#import "DeleteImageViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "FaceShareCollectionViewCell.h"
#import "AsyncImageView.h"

@interface DeleteImageViewController ()
{
    NSArray *arrData;
    NSMutableArray *arrayForShare;
    
}
@end

@implementation DeleteImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title= @"Edit";
    // Do any additional setup after loading the view.
      [self getData];
      arrayForShare = [[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getData{
    [self getDataForEmail];
    
    
}
- (void)getDataForEmail
{
//    arrData = [NSArray new];
//
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    
//    NSArray *resultArray = [defaults objectForKey:@"photosArray"];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.created_by = %@",[CommonFunctions getUseremail]];
//    
//    arrData = [resultArray filteredArrayUsingPredicate:predicate];
    
//
//   
//    arrData = [NSArray new];
//    arrData =  resultArray ;

    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    HUD.graceTime = 0.05f;
    
    arrData = [NSArray new];
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(fetchDeleteDataForPhotos)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    

}

- (void)fetchDeleteDataForPhotos{
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"getImages.php?secureKey=%@&email_id=%@",SiteSecureKey,[CommonFunctions getUseremail]];
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (!error && jsonStr) {
        SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
        NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
        NSLog(@"%@",dic);
#endif
        if (dic) {
            
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            [defaults setObject: [dic[@"Message"] objectForKey:@"Images"] forKey:@"photosArray"];
            [defaults synchronize];
            
            // play with dic : server response
            if ([[dic[@"Message"] objectForKey:@"Images"] isKindOfClass:[NSArray class]]) {
                arrData = [dic[@"Message"] objectForKey:@"Images"];
                if (arrData.count == 0){
                    [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"There is no item." andDelegate:self];
                } else {
                    [collectionViewFriends reloadData];
                }
            } else {
                [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
            }
        } else {
            [CommonFunctions showServerNotFoundError];
        }
    } else {
        [CommonFunctions showServerNotFoundError];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrData.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"DelateFaceCell";
    
    FaceShareCollectionViewCell *cell = (FaceShareCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgFaceShare];
    
    NSDictionary *dictForCell = arrData[indexPath.row];
    
    if ([dictForCell[@"isSelected"] isEqualToString:@"YES"]) {
        [cell.imgBorder setImage:[UIImage imageNamed:@"images-tile-active"]];
        [cell.imgBorder setHidden:NO];
    }
    else{
        [cell.imgBorder setImage:[UIImage imageNamed:@"images-tile"]];
        [cell.imgBorder setHidden:YES];
    }
    cell.imgFaceShare.image = [UIImage imageNamed:@"Placeholder"];
     NSURL *imageviewurl1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@images-game/%@",SiteURL,[self stringByURLEncodeMain:[dictForCell objectForKey:@"image"]]]];

    [cell.imgFaceShare setImageURL:imageviewurl1];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
      NSDictionary *dictForCell = arrData[indexPath.row];
    if ([dictForCell[@"isSelected"] isEqualToString:@"YES"]) {
        [dictForCell setValue:@"NO" forKey:@"isSelected"];
      
    } else {
        [dictForCell setValue:@"YES" forKey:@"isSelected"];
      
    }

     [collectionView reloadItemsAtIndexPaths:@[indexPath]];
    
    
}
- (NSString *)stringByURLEncode:imgurl {
    NSMutableString *tempStr = [NSMutableString stringWithString:imgurl];
    
    return [[NSString stringWithFormat:@"%@",tempStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

-(IBAction)DeleteImage:(id)sender
{
    NSString *deletedId=@"";
  
    for (NSMutableDictionary *dict in arrData) {
        if ([dict[@"isSelected"] isEqualToString:@"YES"])
        {
            if([deletedId isEqualToString:@""])
            {
                deletedId =[NSString stringWithFormat:@"%@",dict[@"id"]];
            }
            else
            {
                 deletedId =[NSString stringWithFormat:@"%@,%@",deletedId,dict[@"id"]];
            }
        }
    }
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"deleteImage.php?secureKey=%@&image_id=%@",SiteSecureKey,deletedId];
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (!error && jsonStr) {
        SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
        NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
        NSLog(@"%@",dic);
#endif
        if (dic) {
            
         [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteNotification" object:@""];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }


}
- (NSString *)stringByURLEncodeMain:strurl {
    NSMutableString *tempStr = [NSMutableString stringWithString:strurl];
    [tempStr replaceOccurrencesOfString:@" " withString:@"+" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [tempStr length])];
    
    return [[NSString stringWithFormat:@"%@",tempStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
}
-(IBAction)BackClick:(id)sender
{
      [self dismissViewControllerAnimated:YES completion:nil];
}

@end
