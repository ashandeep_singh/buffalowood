//
//  ZoomViewController.h
//  testblur
//
//  Created by Ashandeep Singh on 25/02/15.
//  Copyright (c) 2015 Baltech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPImageView.h"

@interface ZoomViewController : UIViewController<UIScrollViewDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    IBOutlet GPImageView  *imgView;
    
}
@property (strong, nonatomic) NSDictionary *arrImage;
@property (nonatomic,strong)  IBOutlet UIButton *bkbtnzoom;
@property (nonatomic,strong)  IBOutlet UIButton *btnlike;
@property (nonatomic,strong)  IBOutlet UILabel *lblName, *lblDate,*lblLikes;
@end
