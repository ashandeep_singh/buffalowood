//
//  DeleteImageViewController.h
//  testblur
//
//  Created by Ashandeep Singh on 24/02/15.
//  Copyright (c) 2015 Baltech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeleteImageViewController : UIViewController<MBProgressHUDDelegate>
{
     MBProgressHUD *HUD;
    IBOutlet UICollectionView *collectionViewFriends;
}
@end
