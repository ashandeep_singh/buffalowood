//
//  FeactureTableViewCell.h
//  TestImagecell
//
//  Created by Ashandeep Singh on 15/11/14.
//  Copyright (c) 2014 Baltech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeactureTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView1;
@property (strong, nonatomic) IBOutlet UIImageView *imgView2;
@property (strong, nonatomic) IBOutlet UILabel *lblName1;
@property (strong, nonatomic) IBOutlet UILabel *lblName2;
@end
