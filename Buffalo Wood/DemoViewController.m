//
//  DemoViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 30/10/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "DemoViewController.h"
#import "FirstViewController.h"

@interface DemoViewController ()
{
    MPMoviePlayerController *moviePlayer;
}
@end

@implementation DemoViewController
@synthesize innerView,btnView,MinnerView,btnNext;

AppDelegate *appDelegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
      isRepeat=@"1";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WelcomeGames:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [innerView setHidden:YES];
    [btnNext setHidden:YES];
    
   // [btnView setHidden:YES];
    
    // Do any additional setup after loading the view.
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *videoPlay = [defaults objectForKey:@"VideoPlay"];
    
    if(videoPlay==nil)
    {
        
       [self playMovie];
    }
    else
    {
//        NSString *pathForFile = [[NSBundle mainBundle] pathForResource: @"GamesVideoExtract" ofType: @"gif"];
//        NSData *dataOfGif = [NSData dataWithContentsOfFile: pathForFile];
//        
//        [wbview loadData:dataOfGif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
//
        [self playAnimateMovie];
    }
    
   
   
}

-(void)viewWillAppear:(BOOL)animated
{
   
}
-(void)playMovie
{
    
    NSString *filepath    =   [[NSBundle mainBundle] pathForResource:@"GamesVideo" ofType:@"mp4"];
    
    NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
    
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(moviePlaybackComplete:)
    //                                                 name:MPMoviePlayerPlaybackDidFinishNotification
    //                                               object:moviePlayer];
    //
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doneButtonClick:)
                                                 name:MPMoviePlayerWillExitFullscreenNotification
                                               object:nil];
    
    
    moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
    
  
    
    moviePlayer.shouldAutoplay = YES;
    [moviePlayer setFullscreen:YES animated:YES];
    
    UIView * playerView = [moviePlayer view];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        
        if(result.height > 480)
        {
            [playerView setFrame: CGRectMake(-44, 44, 568, 320)];
            
            CGAffineTransform landscapeTransform;
            landscapeTransform = CGAffineTransformMakeRotation(90*M_PI/180.0f);
            landscapeTransform = CGAffineTransformTranslate(landscapeTransform, 80, 80);
            [playerView setTransform: landscapeTransform];
            
            [self.view addSubview:moviePlayer.view];
            
            self.navigationController.navigationBarHidden =YES;
            
            [self.tabBarController.tabBar setHidden:YES];
            
            
        }
        else
        {
            
            [playerView setFrame: CGRectMake(0, 0, 468, 320)];
            
            CGAffineTransform landscapeTransform;
            landscapeTransform = CGAffineTransformMakeRotation(90*M_PI/180.0f);
            landscapeTransform = CGAffineTransformTranslate(landscapeTransform, 80, 80);
            [playerView setTransform: landscapeTransform];
            
            [self.view addSubview:moviePlayer.view];
            
            self.navigationController.navigationBarHidden =YES;
            
            [self.tabBarController.tabBar setHidden:YES];
            
            
//            UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//            [button addTarget:self
//                       action:@selector(aMethod)
//             forControlEvents:UIControlEventTouchUpInside];
//            [button setTitle:@"" forState:UIControlStateNormal];
//            button.frame = CGRectMake(422, 0, 50, 26);
//            [playerView addSubview:button];
        }
    }
    
     [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    [moviePlayer play];
    
    NSUserDefaults *defaults1=[NSUserDefaults standardUserDefaults];
    [defaults1 setObject:@"1" forKey:@"VideoPlay"];
    [defaults1 synchronize];
    
   
   
}

-(void)playAnimateMovie
{
    NSString *filepath    =   [[NSBundle mainBundle] pathForResource:@"fullDemo" ofType:@"mov"];
    
    NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
    
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback2:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doneButtonClick:)
                                                 name:MPMoviePlayerWillExitFullscreenNotification
                                               object:nil];
    
    
    
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:YES];
    
//    moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
    moviePlayer.shouldAutoplay = NO;
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.scalingMode = MPMovieScalingModeFill;
    moviePlayer.currentPlaybackRate = 1.5f;
    
    [moviePlayer setFullscreen:YES animated:YES];
    
    UIView * playerView = [moviePlayer view];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height > 480)
        {
            [playerView setFrame: CGRectMake(0, 44, 320, 568)];
            
             [self performSelector:@selector(addView) withObject:nil afterDelay:0.5 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
            
            self.navigationController.navigationBarHidden =YES;
            
            [self.tabBarController.tabBar setHidden:YES];
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        }
        else
        {
            [playerView setFrame: CGRectMake(0, 44, 320, 568)];
            
            [self performSelector:@selector(addView) withObject:nil afterDelay:0.5 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
            
            self.navigationController.navigationBarHidden =YES;
            
            [self.tabBarController.tabBar setHidden:YES];
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        }
    }
    
    
    [moviePlayer play];
    
    NSUserDefaults *defaults1=[NSUserDefaults standardUserDefaults];
    [defaults1 setObject:@"1" forKey:@"VideoPlay"];
    [defaults1 synchronize];
    
   

}

- (void)movieFinishedCallback:(NSNotification *)notification
{
    moviePlayer = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification
												  object:moviePlayer];
	
    [moviePlayer stop];
    [moviePlayer setFullscreen:NO animated:YES];
    [moviePlayer.view removeFromSuperview];
    
    self.navigationController.navigationBarHidden =NO;
    
    [self.tabBarController.tabBar setHidden:NO];
    
    [self WelcomeGames:nil];
    
}

- (void)movieFinishedCallback2:(NSNotification *)notification
{
    moviePlayer = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification
												  object:moviePlayer];
	
  //  [moviePlayer stop];
    [moviePlayer setFullscreen:NO animated:YES];
   // [moviePlayer.view removeFromSuperview];
    
 //   self.navigationController.navigationBarHidden =NO;
    
 //   [self.tabBarController.tabBar setHidden:NO];
    
   // [self.navigationController popViewControllerAnimated:YES];
    
    [self showscreen];
    
}

-(void)doneButtonClick:(NSNotification*)aNotification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification
												  object:moviePlayer];
	
    [moviePlayer stop];
    [moviePlayer setFullscreen:NO animated:YES];
    [moviePlayer.view removeFromSuperview];
    
    self.navigationController.navigationBarHidden =NO;
    
    [self.tabBarController.tabBar setHidden:NO];
//    
//        FirstViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FirstViewController"];
//    
//        [self.navigationController pushViewController:mapVC animated:YES];
 
     [self WelcomeGames:nil];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)WelcomeGames:(id)sender
{
   
    
  //  [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [innerView setHidden:YES];
    [MinnerView setHidden:YES];
    [btnNext setHidden:YES];
    
      [moviePlayer stop];
    [moviePlayer setFullscreen:NO animated:YES];
     [moviePlayer.view removeFromSuperview];
    
       self.navigationController.navigationBarHidden =NO;
    
       [self.tabBarController.tabBar setHidden:NO];

    
    [innerView removeFromSuperview];
    [MinnerView removeFromSuperview];
    [btnNext removeFromSuperview];
    
    [self.navigationController popViewControllerAnimated:YES];
  
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
-(void)showscreen
{    
    [moviePlayer.view addSubview:innerView];
    [moviePlayer.view addSubview:btnNext];
    [innerView setHidden:NO];
    [btnNext setHidden:NO];
    [innerView setAlpha:0];
    [btnNext setAlpha:0];
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:1.5];
    innerView.alpha = 1.0;
    btnNext.alpha =1.0;
    [UIView commitAnimations];
    innerView.frame = CGRectMake(38, 320, 249,42);
    btnNext.frame = CGRectMake(265, 331, 11,20);
    

}
-(void)addView
{
    [self.view addSubview:moviePlayer.view];
}
@end
