//
//  FifthViewController.m
//  Buffalo Wood
//
//  Created by Gaurav on 24/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "FifthViewController.h"
#import <MessageUI/MessageUI.h> 
#import "SponsorsViewController.h"
#import "AboutGamesViewController.h"
#import "AboutSportsViewController.h"
#import "WebAboutViewController.h"

@interface FifthViewController ()

@end

@implementation FifthViewController



- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"About";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    

    
    arrayTableData = @[@[@{@"title": @"About the Games",@"image":@"",@"action":@"SocialFeedViewController"},
                         @{@"title": @"Sponsors",@"image":@"",@"action":@"SocialFeedViewController"},
                         @{@"title": @"About Wood Buffalo",@"image":@"",@"action":@"SocialFeedViewController"},
                         @{@"title": @"Official Sports",@"image":@"",@"action":@"SocialFeedViewController"}],
                       @[@{@"title": @"Email",@"image":@"",@"action":@"SocialFeedViewController"},
                         @{@"title": @"2015woodbuffalo.com",@"image":@"",@"action":@"SocialFeedViewController"}
                         //,@{@"title": @"Help",@"image":@"",@"action":@"SocialFeedViewController"}
                         ],
                       /*@[@{@"title": @"Acknowledgements",@"image":@"",@"action":@"SocialFeedViewController"},
                         @{@"title": @"Privacy",@"image":@"",@"action":@"SocialFeedViewController"},  @{@"title": @"Terms",@"image":@"",@"action":@"SocialFeedViewController"}]
                        */];
    
    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
}

#pragma mark - Table Datasource and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [arrayTableData count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
   if(section==1)
   {
       return @"Contact Us";
   }
   else if(section==2)
   {
        return @"Legal";
   }
   else
   {
     return @"The Games";
   }
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    if(section==1 || section==2)
    {
        label.frame = CGRectMake(16, 0, 320, 20);
    }
    else
    {
    label.frame = CGRectMake(16, 5, 320, 20);
    }
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor grayColor];
    label.font = [UIFont fontWithName: @"Avenir Next" size: 17.0 ];
    label.text = sectionTitle;
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableData[section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"FifthTableViewCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    NSDictionary *dictForCell = [arrayTableData[indexPath.section] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = dictForCell[@"title"];
    UIImage *imageName = [UIImage imageNamed:dictForCell[@"image"]];
    cell.imageView.image = imageName;
    
    UIFont *myFont = [ UIFont fontWithName: @"Avenir Next" size: 16.0 ];
    cell.textLabel.font  = myFont;
    
    if(indexPath.section==1)
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            AboutGamesViewController *eventCatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutGamesVC"];
            
            [self.navigationController pushViewController:eventCatVC animated:YES];
        }
        else if(indexPath.row==1)
        {
            SponsorsViewController *spVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SponsorsVC"];
            
            [self.navigationController pushViewController:spVC animated:YES];
        }
        else if(indexPath.row==2)
        {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.woodbuffalo.ab.ca/"]];
//            exit(0);
            
            WebAboutViewController *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WebAboutVC"];
            
            webVC.webViewUrl = @"http://www.woodbuffalo.ab.ca/";
            
            [self.navigationController pushViewController:webVC animated:YES];
            
        }
        else if(indexPath.row==3)
        {
            AboutSportsViewController *eventCatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SportsVC"];
            
            [self.navigationController pushViewController:eventCatVC animated:YES];        }
        
    }
    else if(indexPath.section==1)
    {
        if(indexPath.row==0)
        {
            [self EmailPressed];
        }
        if(indexPath.row==1)
        {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.2015woodbuffalo.com"]];
//            exit(0);

            WebAboutViewController *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WebAboutVC"];
            
            webVC.webViewUrl = @"http://www.2015woodbuffalo.com";
            
            [self.navigationController pushViewController:webVC animated:YES];

        }

    }
    
    NSLog(@"row selected in my list %d,%d",indexPath.section,indexPath.row);
//    
//    NSString *viewController = [[arrayTableData[indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"action"];
//    
//    if (indexPath.section == 1 && indexPath.row == 0) {
//        if (isUserLogin) {
//            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"VolunteerNotificationsViewController"]
//                                                 animated:YES];
//        } else {
//            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewController]
//                                                 animated:YES];
//        }
//    } else {
//        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewController]
//                                             animated:YES];
//    }
}
- (void)EmailPressed
{
    if([MFMailComposeViewController canSendMail])
    {
        NSString *emailTitle = @"contact from iphone app";
        // Email Content
        NSString *messageBody = @"";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"info@2015woodbuffalo.com"];
        
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSLog (@"mail finished");
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark -
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
