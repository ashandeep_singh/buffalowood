//
//  SocialFeedTableViewCell.h
//  Buffalo Wood
//
//  Created by Gaurav on 24/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialFeedTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *imgViewType,*imgstrip;

@property (nonatomic, strong) IBOutlet UILabel *lblTitle, *lblDate;

@property (nonatomic, strong) IBOutlet UITextView *txtViewDiscription;

@property (nonatomic, strong) IBOutlet UILabel *lblShare;

@property (nonatomic, strong) IBOutlet UIButton *btnEmail, *btnFB, *btnTwitter, *btnGPlus;

@end