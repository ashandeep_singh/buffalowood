//
//  NewsViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 22/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    IBOutlet UITableView *table;
}
@property (nonatomic,strong)  NSArray *arrayNewsTableData;
@property (nonatomic, retain) NSMutableArray *arrayOfCharacters;
@property (nonatomic, retain) NSMutableDictionary *objectsForCharacters;

@end
