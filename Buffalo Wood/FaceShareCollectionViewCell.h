//
//  FaceShareCollectionViewCell.h
//  FaceShare
//
//  Created by Madhvi on 09/12/14.
//  Copyright (c) 2014 ___baltech___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaceShareCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)IBOutlet UIImageView *imgFaceShare;
@property(nonatomic,strong)IBOutlet UIImageView *imgBorder;
@end
