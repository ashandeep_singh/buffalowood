//
//  NewsDetailViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 22/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPImageView.h"

@interface NewsDetailViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) NSDictionary *dictForPage;
@property (strong, nonatomic) IBOutlet GPImageView *imgViewBg;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UIWebView *lblDiscription;
@property (strong, nonatomic) IBOutlet UIButton *btnLeftImageSlider;
@property (strong, nonatomic) IBOutlet UIButton *btnRightImageSlider;
@property (nonatomic) NSInteger indexForArray;
@property (nonatomic,strong)  IBOutlet UIButton *bkbtn;
@property (strong, nonatomic) IBOutlet UIPageControl *btnPager;

@end
