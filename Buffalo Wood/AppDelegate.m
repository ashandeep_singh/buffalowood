//
//  AppDelegate.m
//  Buffalo Wood
//
//  Created by Gaurav on 23/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "AppDelegate.h"

#import <GooglePlus/GooglePlus.h>
#import "FirstViewController.h"

@implementation AppDelegate
@synthesize isRepeat;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSLog(@"Registering for push notifications...");
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeAlert |
                                                                            UIRemoteNotificationTypeBadge |
                                                                            UIRemoteNotificationTypeSound)];
    
    // Override point for customization after application launch.
    mainWindow = self.window;
    
    isRepeat=@"0";
    
    //for GPS current location
    //for getting Lattitude & Longitude
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    mainWindow = _window;
    
    isUserLogin = [CommonFunctions isUserLogin];
    NSLog(@"user login = %d",isUserLogin);
    
    
    tabController = (UITabBarController*)[mainWindow rootViewController];
    
    [[UITabBar appearance] setTintColor:UIColorFromRedGreenBlue(11, 40, 118)];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"AvenirNext-Regular" size:11.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [CommonFunctions showTabBar:tabController];   
   
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
//     if([isRepeat isEqualToString:@"0"])
//     {
//         [tabController setSelectedIndex:0];
//      }
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - GPS current location
#pragma mark CLLocation methods
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //if the time interval returned from core location is more than two minutes we ignore it because it might be from an old session
    if ( abs([newLocation.timestamp timeIntervalSinceDate: [NSDate date]]) < 120) {
        //CLLocation *test = [[CLLocation alloc] initWithLatitude:-33.857034 longitude:151.035929];
        currentLocation = newLocation;
        coordinates = newLocation.coordinate;
        
        core_latitude = currentLocation.coordinate.latitude;
        core_longitude = currentLocation.coordinate.longitude;
        
        [locationManager stopUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    //latitude = 43.795365;
    // longitude = -79.183776;
    NSLog(@"location error: app delegate %@", [error description]);
    
}





//-----------------------------------------------fb  g+ joint methods starts
#pragma mark - fb methods

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    // Handle the user leaving the app while the Facebook login dialog is being shown
    // For example: when the user presses the iOS "home" button while the login dialog is active
  
}

#pragma mark - g+ delegates
- (BOOL)application: (UIApplication *)application
            openURL: (NSURL *)url
  sourceApplication: (NSString *)sourceApplication
         annotation: (id)annotation {
    
    NSString *urlChange = [NSString stringWithFormat:@"%@",url];
    if ([urlChange hasPrefix:@"fb"]) {
     
    }
    
    return [GPPURLHandler handleURL:url
                  sourceApplication:sourceApplication
                         annotation:annotation];
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    appDeviceToken = [NSString stringWithFormat:@"%@",deviceToken];
    
    appDeviceToken = [appDeviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    appDeviceToken = [appDeviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    appDeviceToken = [appDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
}
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)notification
//{
//   
//        UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle: @"You had receieved notification!!"
//                              message: msg
//                              delegate: self
//                              cancelButtonTitle: @"Cancel"
//                              otherButtonTitles: @"OK", nil];
//        [alert show];
//        
//}
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    switch (buttonIndex)
//    {
//        case 0: // cancel
//        {
//            NSLog(@"Cancelled by the user");
//        }
//            break;
//        case 1: // delete
//        {
//            fromNotification = @"1";
//            
//            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
//            
//            UINavigationController *navVc=(UINavigationController *) self.window.rootViewController;
//            
//            SplashViewController *splashViewController =[mainStoryboard instantiateViewControllerWithIdentifier:@"splashVC"];
//            
//            splashViewController.jobSId = jobId;
//            
//            [navVc pushViewController:splashViewController animated:NO];
//            
//            
//        }
//            break;
//            
//    }
//}
//-----------------------------------------------fb  g+ joint methods end

@end
