//
//  PhotosViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 25/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"

@interface PhotosViewController : UIViewController<MBProgressHUDDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    MBProgressHUD *HUD;
    IBOutlet UICollectionView *collectionViewFriends;
    IBOutlet UIImageView *imageView;
    IBOutlet UIView *hideView;
    IBOutlet UIActivityIndicatorView *actView;
}
@end
