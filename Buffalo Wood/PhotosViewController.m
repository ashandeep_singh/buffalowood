//
//  PhotosViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 25/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import "PhotosViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "AsyncImageView.h"
#import "FaceShareCollectionViewCell.h"
#import "ZoomViewController.h"
#import "DeleteImageViewController.h"

@interface PhotosViewController ()
{
     NSArray *arrData;
     NSArray *arrImages;
     UIView *popup;
     UITextField *popUpEmail;
     NSString *name;
     HMSegmentedControl *segmentedControlMain;
}
@property (nonatomic, strong) UIScrollView *scrollView;
@end

@implementation PhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"Photos";    
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(ReDeleteData:) name:@"deleteNotification" object: nil];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    // Do any additional setup after loading the view.
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    
    // Minimum code required to use the segmented control with the default styling.
    segmentedControlMain = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"All", @"Games Wear Friday", @"Your Photos"]];
    segmentedControlMain.frame = CGRectMake(20, 70, viewWidth, 35);
    segmentedControlMain.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    segmentedControlMain.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
    segmentedControlMain.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControlMain.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
  
    [segmentedControlMain addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControlMain];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSArray *resultArray = [defaults objectForKey:@"photosArray"];
    
    if(resultArray.count==0 || resultArray.count<12)
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.delegate = self;
        HUD.graceTime = 0.05f;
        
        arrData = [NSArray new];
        
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(fetchDataForPhotos:)
                       onTarget:self
                     withObject:nil
                       animated:YES];
        
    }
    else
    {
            arrData =  resultArray;
            arrImages = resultArray;
            [collectionViewFriends reloadData];
     }
    
    [hideView setHidden:YES];
    [actView setHidden:YES];
    
    UIRefreshControl *refContr = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [refContr setBackgroundColor: [CommonFunctions colorWithHexString:@"174195"]];
    refContr.tintColor = [UIColor whiteColor];
    [refContr addTarget:self
                 action:@selector(fetchDataForPhotos:)
       forControlEvents:UIControlEventValueChanged];
    [collectionViewFriends addSubview:refContr];
    [refContr setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
    
    [[refContr.subviews objectAtIndex:0] setFrame:CGRectMake(30, 0, 20, 30)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)ReDeleteData:(NSNotification *)notification
{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    HUD.graceTime = 0.05f;
    
    arrData = [NSArray new];
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(fetchDataForPhotos:)
                   onTarget:self
                 withObject:nil
                   animated:YES];
    
    segmentedControlMain.selectedSegmentIndex=0;
    
     self.navigationItem.rightBarButtonItems = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
     self.navigationController.navigationBarHidden = NO;
}

- (void)fetchDataForPhotos:(UIRefreshControl *)refCntrl {
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"getImages.php?secureKey=%@",SiteSecureKey];
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (!error && jsonStr) {
        SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
        NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
        NSLog(@"%@",dic);
#endif
        if (dic) {
            
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            [defaults setObject: [dic[@"Message"] objectForKey:@"Images"] forKey:@"photosArray"];
            [defaults synchronize];
            
            // play with dic : server response
            if ([[dic[@"Message"] objectForKey:@"Images"] isKindOfClass:[NSArray class]]) {
                arrData = [dic[@"Message"] objectForKey:@"Images"];
                arrImages = [dic[@"Message"] objectForKey:@"Images"];
                if (arrData.count == 0){
                    [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"There is no item." andDelegate:self];
                } else {
                    if (refCntrl) {
                        
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"MMM d, h:mm a"];
                        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                                    forKey:NSForegroundColorAttributeName];
                        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                        refCntrl.attributedTitle = attributedTitle;
                        
                        [refCntrl endRefreshing];
                        
                    }
                    
                  [collectionViewFriends reloadData];
                }
            } else {
                [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
            }
        } else {
            [CommonFunctions showServerNotFoundError];
        }
    } else {
        [CommonFunctions showServerNotFoundError];
    }
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrData.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"FacesCell";
    
    FaceShareCollectionViewCell *cell = (FaceShareCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgFaceShare];
    
    NSDictionary *dictForCell = arrData[indexPath.row];
    
    cell.imgFaceShare.image = [UIImage imageNamed:@"Placeholder"];

    
    NSURL *imageviewurl1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@images-game/%@",SiteURL,[self stringByURLEncodeMain:[dictForCell objectForKey:@"image"]]]];
    
    [cell.imgFaceShare setImageURL:imageviewurl1];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ZoomViewController *zoomVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ZoomVC"];
    
    zoomVC.arrImage = [arrData objectAtIndex:indexPath.row];
    
    zoomVC.hidesBottomBarWhenPushed = YES;

    [UIView transitionWithView:self.navigationController.view
                      duration:.50
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController pushViewController:zoomVC animated:NO];
                    }
                    completion:nil];
    
    
    
}
- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    if(segmentedControl.selectedSegmentIndex==1)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.category_id = %@", @"8"];
        
        arrData = [arrImages filteredArrayUsingPredicate:predicate];
        
        [collectionViewFriends reloadData];
        
        self.navigationItem.rightBarButtonItems = nil;
    }
    else if(segmentedControl.selectedSegmentIndex==2)
    {
        NSString *userEmail = [CommonFunctions getUseremail];
        
        if(![userEmail isKindOfClass:[NSString class]])
        {
            [self showPopUp];
        }
        else
        {
           NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.created_by = %@", [userEmail lowercaseString] ];
            
            arrData = [arrImages filteredArrayUsingPredicate:predicate];
            
            [collectionViewFriends reloadData];
            
            [self addrightbarbutton];
        }
    }
    else
    {
        arrData = arrImages;
        
        [collectionViewFriends reloadData];
        
         self.navigationItem.rightBarButtonItems = nil;
    }
    
}

- (void)uisegmentedControlChangedValue:(UISegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld", (long)segmentedControl.selectedSegmentIndex);
}
-(IBAction)EditClick:(id)sender
{
    DeleteImageViewController *deleteVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DeleteImageVC"];
    
   // [self presentModalViewController:deleteVC animated:YES completion:nil];
    
    [self presentViewController:deleteVC animated:YES completion:nil];
}
- (void)showPopUp {
    
    //--- create view for popup
    popup = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [popup setBackgroundColor:[UIColor clearColor]];
    
    // for light black background
    UIView *popupTempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    popupTempView.alpha = 0.5f;
    [popupTempView setBackgroundColor:[UIColor blackColor]];
    [popup addSubview:popupTempView];
    
    //--- add on popup view
    UILabel *popUpLbl = [[UILabel alloc] initWithFrame:CGRectMake(45, 165, 230, 120)];
    [popUpLbl setBackgroundColor:UIColorFromRGB(0xEEEEEE)];
    popUpLbl.layer.cornerRadius = 4.0f;
    popUpLbl.clipsToBounds = YES;
    [popup addSubview:popUpLbl];
    
    
    UILabel *popUpTitle = [[UILabel alloc] initWithFrame:CGRectMake(125, 180, 100, 20)];
    [popUpTitle setTextColor:UIColorFromRGB(0x405568)];
    //[popUpTitle setFont:UIFontSourceSansProRegularSize(17.0f)];
    [popUpTitle setText:@"Photos"];
    [popup addSubview:popUpTitle];

     popUpEmail = [[UITextField alloc] initWithFrame:CGRectMake(58, 210, 200, 25)];
    [popUpEmail setTextColor:UIColorFromRGB(0x405568)];
    //[popUpDiscription setFont:[]];
    [popUpEmail setPlaceholder:@"Enter email address"];
    [popUpEmail setTextAlignment:NSTextAlignmentCenter];
    [popUpEmail setBorderStyle:UITextBorderStyleNone];
    [popUpEmail becomeFirstResponder];
    [popup addSubview:popUpEmail];

    
    //--- ok buttons
    UIButton *btnOk = [[UIButton alloc] initWithFrame:CGRectMake(112, 245, 95, 30)];
    [btnOk setBackgroundColor:UIColorFromRGB(0x405568)];
    [btnOk setTitle:@"Ok" forState:UIControlStateNormal];
    //[btnOk.titleLabel setFont:UIFontSourceSansProRegularSize(17.0f)];
    [btnOk setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnOk addTarget:self
              action:@selector(btnInsidePopUpPressed)
    forControlEvents:UIControlEventTouchUpInside];
    btnOk.userInteractionEnabled = YES;
    btnOk.layer.cornerRadius = 4;
    btnOk.clipsToBounds = YES;
    [popup addSubview:btnOk];
    
    [mainWindow addSubview:popup];
}

- (void)btnInsidePopUpPressed{
   if([popUpEmail.text isEqualToString:@""])
    {
         [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Please enter your email address"];
    }
    else
    {
        NSString *emailadr = [[popUpEmail.text stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]]lowercaseString];
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:emailadr forKey:@"useremail"];
        [defaults synchronize];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.created_by = %@", [emailadr lowercaseString]];
        
        arrData = [arrImages filteredArrayUsingPredicate:predicate];
        
        [collectionViewFriends reloadData];
        
        [popup removeFromSuperview];
         popup = nil;
    }
    
}
-(void)addrightbarbutton
{
    
    UIImage* image1 = [UIImage imageNamed:@"navbar-edit-icon.png"];
    CGRect frameimg = CGRectMake(0, 0, image1.size.width, image1.size.height);
    UIButton *editbtn = [[UIButton alloc] initWithFrame:frameimg];
    [editbtn setBackgroundImage:image1 forState:UIControlStateNormal];
    [editbtn addTarget:self action:@selector(EditClick:)
      forControlEvents:UIControlEventTouchUpInside];
    [editbtn setShowsTouchWhenHighlighted:YES];
    
    UIImage* image2 = [UIImage imageNamed:@"navbar-add-icon.png"];
    CGRect frameimg2 = CGRectMake(0, 0, image2.size.width, image2.size.height);
    UIButton *addbtn = [[UIButton alloc] initWithFrame:frameimg2];
    [addbtn setBackgroundImage:image2 forState:UIControlStateNormal];
    [addbtn addTarget:self action:@selector(imageBtn:)
     forControlEvents:UIControlEventTouchUpInside];
    [addbtn setShowsTouchWhenHighlighted:YES];
    
    
    UIBarButtonItem *bareditbutton =[[UIBarButtonItem alloc] initWithCustomView:editbtn];
    
    UIBarButtonItem *baraddbutton =[[UIBarButtonItem alloc] initWithCustomView:addbtn];
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:baraddbutton,bareditbutton, nil]];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if  ([buttonTitle isEqualToString:@"Destructive Button"]) {
        NSLog(@"Destructive pressed --> Delete Something");
        imageView.image=nil;
        
    }
    if ([buttonTitle isEqualToString:@"Take Photo"]) {
        NSLog(@"Camera Click");
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"message:@"Device has no camera"delegate:nil cancelButtonTitle:@"OK"otherButtonTitles: nil];
            [myAlertView show];
        }
        
        else{
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        
    }
    if ([buttonTitle isEqualToString:@"Choose Existing Photo"]) {
        NSLog(@"Choose Existing");
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    if ([buttonTitle isEqualToString:@"Cancel Button"]) {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:NO completion:nil];
    imageView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    NSURL *imagePath = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
    name=[imagePath lastPathComponent];
     [self showaddview];
    //NSLog(@"%@",name);
    
}

- (IBAction)imageBtn:(id)sender {
    NSString *actionSheetTitle = @"Choose Picture"; //Action Sheet Title
    NSString *fromGallary = @"Choose Existing Photo";
    NSString *fromCamera = @"Take Photo";
    NSString *cancelTitle = @"Cancel Button";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:actionSheetTitle delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:fromGallary,fromCamera, nil];
    
    [actionSheet showInView:self.view];
    
}

- (IBAction)uploadImageAtServer:(id)sender{
    
    //    v3 = [[YLActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 180, 15)];
    //    v3.center = CGPointMake(160, 90);
    //    v3.dotCount = 10;
    //    v3.duration = 2.0f;
    //    [self.view addSubview:v3];
    //    [v3 startAnimating];
    //     [v3 setHidden:NO];
    //     [self.view bringSubviewToFront:v3];
    
    [actView startAnimating];
    [actView setHidden:NO];
    
    [self performSelector:@selector(sendserver) withObject:nil afterDelay:0.1];
    
    
    
    
}
-(void)sendserver
{
    [self sendata];
     [self hideaddview];
}
-(void)sendata
{
    NSString *strURL = [SiteAPIURL stringByAppendingFormat:@"uploadImage.php?secureKey=%@&email_id=%@&category_id=0&image_title=%@",SiteSecureKey,[[CommonFunctions getUseremail] lowercaseString],name];
    
    NSURL *siteURL = [[NSURL alloc] initWithString:[self stringByURLEncodeMain:strURL]];
    
    
    // create the connection
    NSMutableURLRequest *siteRequest = [NSMutableURLRequest requestWithURL:siteURL
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:30.0];
    
    // change type to POST (default is GET)
    [siteRequest setHTTPMethod:@"POST"];
    
    // just some random text that will never occur in the body
    NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    
    // header value
    NSString *headerBoundary = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",
                                stringBoundary];
    
    // set header
    [siteRequest addValue:headerBoundary forHTTPHeaderField:@"Content-Type"];
    
    //add body
    NSMutableData *postBody = [NSMutableData data];
    //    pro(@"body made");
    
    //image
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@\"\r\n",name] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: image/jpg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSData *imageData =UIImageJPEGRepresentation(imageView.image, 0);
    
    [postBody appendData:imageData];
    
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //pr(@"message data post data %@",postBody);
    
    // add body to post
    [siteRequest setHTTPBody:postBody];
    
    
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:siteRequest
                                               returningResponse:&response
                                                           error:&error];
    
    if (!error && returnData) {
        NSError *error1;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:returnData
                                                            options:NSJSONReadingMutableContainers
                                                              error:&error1];
        //                    pr(@"%@, error %@",dic,error1);
        if (dic && !error1) {
            if ([dic[@"Message"] isEqualToString:@"Image uploded successfully"]) {
                [actView stopAnimating];
                [actView setHidden:YES];
                [hideView setHidden:YES];
               
                HUD = [[MBProgressHUD alloc] initWithView:self.view];
                HUD.delegate = self;
                HUD.graceTime = 0.05f;
                
                arrData = [NSArray new];
                
                [mainWindow addSubview:HUD];
                [HUD showWhileExecuting:@selector(fetchDataForPhotos:)
                               onTarget:self
                             withObject:nil
                               animated:YES];
                
                 segmentedControlMain.selectedSegmentIndex=0;
            }
            else
            {
               
                [actView stopAnimating];
                [actView setHidden:YES];
                [hideView setHidden:YES];
            }
        }
    }
    
}
- (NSString *)stringByURLEncodeMain:strurl {
    NSMutableString *tempStr = [NSMutableString stringWithString:strurl];
    [tempStr replaceOccurrencesOfString:@" " withString:@"+" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [tempStr length])];
    
    return [[NSString stringWithFormat:@"%@",tempStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
}
-(void)showaddview
{
    [UIView transitionWithView:hideView
                      duration:0.50
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    
    [UIView transitionWithView:collectionViewFriends
                      duration:0.50
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    
    [collectionViewFriends setFrame:(CGRectMake(0, 170, 320, 298))];
    [hideView setFrame:(CGRectMake(0, 110, 320, 63))];
    [hideView setHidden:NO];
}
-(void)hideaddview
{
    [UIView transitionWithView:hideView
                      duration:0.50
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    
    [UIView transitionWithView:collectionViewFriends
                      duration:0.50
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    
    [collectionViewFriends setFrame:(CGRectMake(0, 116, 320, 403))];
    [hideView setFrame:(CGRectMake(0, 110, 320, 0))];
    [hideView setHidden:YES];
}
- (IBAction)CancelClick:(id)sender{
    [self hideaddview];
}
@end
