//
//  SponsorsViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 16/11/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "SponsorsViewController.h"
#import "FeactureTableViewCell.h"
#import "AsyncImageView.h"

@interface SponsorsViewController ()
{
    NSDictionary *msgEventDic,*mainJson;
    NSMutableArray *Eventdata;
  
}
@end

@implementation SponsorsViewController
AppDelegate *appDelegate;
@synthesize arrayOfCharacters,objectsForCharacters;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Sponsors List";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];

//    UIBarButtonItem *leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"About" style:UIBarButtonItemStyleDone target:self action:@selector(BackButtonClick)];
//    
//    [leftBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                               [UIFont fontWithName:@"Avenir Next" size:18], NSFontAttributeName,
//                                               [CommonFunctions colorWithHexString:@"174195"], NSForegroundColorAttributeName,
//                                               nil]
//                                     forState:UIControlStateNormal];
//    
//    
//    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
//    self.navigationItem.leftItemsSupplementBackButton = NO;
    
    UIRefreshControl *refContr = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [refContr setBackgroundColor: [CommonFunctions colorWithHexString:@"174195"]];
    refContr.tintColor = [UIColor whiteColor];
    [refContr addTarget:self
                 action:@selector(fetchData:)
       forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refContr];
    [refContr setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
    
    [[refContr.subviews objectAtIndex:0] setFrame:CGRectMake(30, 0, 20, 30)];
   
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableArray *resultArray = [defaults objectForKey:@"SponsorsArray"];
    
    if(resultArray.count==0)
    {
        hud = [[MBProgressHUD alloc] initWithWindow:mainWindow];
        hud.delegate = (id)self;
        hud.graceTime = 0.05f;
        [mainWindow addSubview:hud];
        
        [hud showWhileExecuting:@selector(fetchData:)
                       onTarget:self
                     withObject:nil
                       animated:YES];
    }
    else
    {
            Eventdata = resultArray;
             [self setupIndexData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fetchData:(UIRefreshControl *)refCntrl
{
    NSURL *siteURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@getSponsor.php?secureKey=%@",SiteAPIURL,SiteSecureKey]];
    
    NSData *urlData = [NSData dataWithContentsOfURL:siteURL];
    
    if(urlData == nil)
    {
        [msgEventDic setValue:@"Sorry" forKey:@"title"];
        [msgEventDic setValue:@"Data not found." forKey:@"message"];
        [self performSelectorOnMainThread:@selector(showAlert)
                               withObject:msgEventDic
                            waitUntilDone:NO];
        return;
    }
    NSError *error;
    mainJson=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    
    if(mainJson != nil)
    {
        Eventdata = [mainJson objectForKey:@"sponsors"];
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:Eventdata forKey:@"SponsorsArray"];
        [defaults synchronize];
    }
    
    if (refCntrl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refCntrl.attributedTitle = attributedTitle;
        
        [refCntrl endRefreshing];
        
    }

    [hud removeFromSuperview];
     [self setupIndexData];
}

-(void)ReBindData:(NSNotification *)notification
{
    
    hud = [[MBProgressHUD alloc] initWithWindow:mainWindow];
    hud.delegate = (id)self;
    hud.graceTime = 0.05f;
    [mainWindow addSubview:hud];
    
    [hud showWhileExecuting:@selector(fetchData:)
                   onTarget:self
                 withObject:nil
                   animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [arrayOfCharacters count];
    
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"yyyy-MM-dd";
//    NSDate *yourDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",arrayOfCharacters[section]]];
//    dateFormatter.dateFormat = @"EEEE, dd MMMM";
//    return [NSString stringWithFormat:@"    %@",[dateFormatter stringFromDate:yourDate]];
//
//}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.text = [NSString stringWithFormat:@"%@",arrayOfCharacters[section]];
    header.textLabel.font = [UIFont fontWithName: @"Avenir Next" size: 15.0 ];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int numRows = [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:section]] count]/2;
    
    if(numRows==0 || numRows==1)
    {
        numRows=1;
    }
    else if([[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:section]] count]%2>0)
    {
        numRows=numRows+1;
    }
    
    return numRows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     static NSString *CellIdentifier = @"FCell";
    
    FeactureTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[FeactureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
          [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgView1];
        
          [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgView2];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    int cntRow = indexPath.row*2;
    
    NSDictionary *dictForCell =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:cntRow];
    
        NSURL *imageviewurl = [NSURL URLWithString:[dictForCell objectForKey:@"image"]];
        
        //set placeholder image or cell won't update when image is loaded
        cell.imgView1.image = [UIImage imageNamed:@"Placeholder.png"];
        
        cell.imgView1.imageURL = imageviewurl;
    
       if([[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] count] > cntRow+1)
        {
            NSDictionary *dictForCell1 =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:cntRow+1];
    
             cell.imgView2.image = [UIImage imageNamed:@"Placeholder.png"];
            
            NSURL *imageviewurl1 = [NSURL URLWithString:[dictForCell1 objectForKey:@"image"]];

            cell.imgView2.imageURL = imageviewurl1;
        }
        else
        {
            cell.imgView2.imageURL = nil;

        }
    
        cell.lblName1.text =[[Eventdata objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"row selected in my list %d",indexPath.row);
}

-(void)showAlert
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buffalo Wood" message:@"Unable to Connect to Server" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)setupIndexData {
    
    self.arrayOfCharacters = [[NSMutableArray alloc] init];
    self.objectsForCharacters = [[NSMutableDictionary alloc] init];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSMutableArray *arrayOfNames = [[NSMutableArray alloc] init];
    
    //  NSString *numbericSection = @"#";
    NSString *firstLetter;
    
    //  NSString *frID, *frName, *frMobile;
    int valcount = 0;
    
    for (NSMutableArray *inObject in Eventdata)
    {
        NSMutableDictionary *data = [Eventdata objectAtIndex:valcount];
        
        NSLog(@"%@",[data objectForKey:@"sortdate"]);
        
        firstLetter = [[data objectForKey:@"category"] uppercaseString] ;
        
        // firstLetter = [cellData.Name substringToIndex:1];
        
        // Check if it's NOT a number
        if ([formatter numberFromString:firstLetter] == nil) {
            
            if (![objectsForCharacters objectForKey:firstLetter])
            {
                
                [arrayOfNames removeAllObjects];
                [arrayOfCharacters addObject:firstLetter];
            }
            
            [arrayOfNames addObject:[Eventdata objectAtIndex:valcount]];
            valcount = valcount+1;
            
            /**
             * Need to autorelease the copy to preven potential leak. Even though the
             * arrayOfNames is released below it still has a retain count of +1
             */
            [objectsForCharacters setObject:[arrayOfNames copy] forKey:firstLetter];
            
        }
    }
    [self.tableView reloadData];
    
}
-(void)BackButtonClick
{
   
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
