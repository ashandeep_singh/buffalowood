//
//  NewsViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 22/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import "NewsViewController.h"
#import "VolunteerNotificationsTableViewCell.h"
#import "NewsDetailViewController.h"

@interface NewsViewController ()
{
      NSArray *arrayNewsTableData;
}
@end

@implementation NewsViewController
@synthesize arrayNewsTableData,arrayOfCharacters,objectsForCharacters;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"News";

    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = YES;

    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    
    
    // self.navigationItem.leftBarButtonItem = [self getBackBtn];
    
    
    // self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *resultArray = [defaults objectForKey:@"NewsArray"];
    
    if(resultArray.count==0)
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.delegate = self;
        HUD.graceTime = 0.05f;
        
        [table setSeparatorInset:UIEdgeInsetsZero];
        
        arrayNewsTableData = [NSArray new];
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(fetchNewsFromServer:)
                       onTarget:self
                     withObject:nil
                       animated:YES];
    }
    else
    {
        if ([resultArray[@"News"] isKindOfClass:[NSArray class]]) {
            arrayNewsTableData = resultArray[@"News"];
            [self setupIndexData];
        }
    }
    
    
    
    UIRefreshControl *refContr = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [refContr setBackgroundColor: [CommonFunctions colorWithHexString:@"174195"]];
    refContr.tintColor = [UIColor whiteColor];
    [refContr addTarget:self
                 action:@selector(fetchNewsFromServer:)
       forControlEvents:UIControlEventValueChanged];
    [table addSubview:refContr];
    [refContr setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
    
    [[refContr.subviews objectAtIndex:0] setFrame:CGRectMake(30, 0, 20, 30)];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:YES];
}
- (void)viewDidAppear:(BOOL)animated {
}
-(UIBarButtonItem*) getBackBtn{
    UIButton * backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_iPad) {
        [backBtn setFrame:CGRectMake(0.0f,0.0f,20.0f,84.0f)];
    } else {
        [backBtn setFrame:CGRectMake(0.0f,0.0f,23.0f,23.0f)];
    }
    
    [backBtn setImage:[UIImage imageNamed:@"navbar-back-icon"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(BackBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    return backBarButton;
}
-(void)BackBtnPressed{
    UITabBarController *tabBarController = (UITabBarController*)[DELEGATE.window rootViewController];
    UINavigationController *navigationController = (UINavigationController*)[tabBarController selectedViewController];
    [navigationController  popToRootViewControllerAnimated:YES];
}

- (void)fetchNewsFromServer:(UIRefreshControl *)refCntrl {
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"getNews.php?secureKey=%@",SiteSecureKey];
    NSLog(@"%@",strUrl);
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (![NSThread isMainThread])
    {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr)
            {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
                
            if (dic)
            {
            
                    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                    [defaults setObject:dic forKey:@"NewsArray"];
                    [defaults synchronize];
                    
                    // play with dic : server response
                    if ([dic[@"News"] isKindOfClass:[NSArray class]]) {
                        arrayNewsTableData = dic[@"News"];
                        if (arrayNewsTableData.count == 0)
                        {
                            [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"There is no item." andDelegate:self];
                        } else
                        {
                            if (refCntrl) {
                                
                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                [formatter setDateFormat:@"MMM d, h:mm a"];
                                NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                                NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                                            forKey:NSForegroundColorAttributeName];
                                NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                                refCntrl.attributedTitle = attributedTitle;
                                
                                [refCntrl endRefreshing];
                                
                            }
                            
                            [self setupIndexData];
                        }
                    } else {
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            }
                else
            {
                [CommonFunctions showServerNotFoundError];
            }
        
        });
     }
    
    else
    {
        if (refCntrl) {
            SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
            NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
            
            if (dic)
            {
                
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                [defaults setObject:dic forKey:@"NewsArray"];
                [defaults synchronize];
                
                // play with dic : server response
                if ([dic[@"News"] isKindOfClass:[NSArray class]]) {
                    arrayNewsTableData = dic[@"News"];
                    if (arrayNewsTableData.count == 0)
                    {
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"There is no item." andDelegate:self];
                    } else
                    {
                        if (refCntrl) {
                            
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:@"MMM d, h:mm a"];
                            NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                            NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                                        forKey:NSForegroundColorAttributeName];
                            NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                            refCntrl.attributedTitle = attributedTitle;
                            
                            [refCntrl endRefreshing];
                            
                        }
                        
                        [self setupIndexData];
                    }
                } else {
                    [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        }
        else
        {
            [CommonFunctions showServerNotFoundError];
        }
    }
}

    
#pragma mark - UITableView delegates
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [arrayOfCharacters count];
    
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",arrayOfCharacters[section]]];
    dateFormatter.dateFormat = @"EEEE, dd MMMM";
    
    header.textLabel.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:yourDate]];
    header.textLabel.font = [UIFont fontWithName: @"Avenir Next" size: 15.0 ];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:section]] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    VolunteerNotificationsTableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"NewsTableViewCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[VolunteerNotificationsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    NSDictionary *dictForCell =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    cell.lblHeading.text = dictForCell[@"title"];
    
 //   NSString *strVal = dictForCell[@"news_description"];
    
   // cell.lblTime.attributedText = dictForCell[@"news_description"];
    
    NSString *myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                                   "<head> \n"
                                   "<style type=\"text/css\"> \n"
                                   "body {font-family: \"%@\"; font-size: %@;}\n"
                                   "</style> \n"
                                   "</head> \n"
                                   "<body>%@</body> \n"
                                   "</html>", @"Avenir Next", [NSNumber numberWithInt:14], dictForCell[@"news_description"]];
    
    [cell.lblDetail loadHTMLString:myDescriptionHTML baseURL:nil];
    
    //[cell.lblDetail loadHTMLString:dictForCell[@"news_description"] baseURL:nil];
    
    cell.lblDetail.scrollView.scrollEnabled = NO;
    cell.lblDetail.scrollView.bounces = NO;
    
    return cell;
}
#pragma mark -  HUD
- (void)hudWasHidden:(MBProgressHUD *)hud{
    [hud removeFromSuperview];
}
#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)setupIndexData {
    
    self.arrayOfCharacters = [[NSMutableArray alloc] init];
    self.objectsForCharacters = [[NSMutableDictionary alloc] init];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSMutableArray *arrayOfNames = [[NSMutableArray alloc] init];
    
    //  NSString *numbericSection = @"#";
    NSString *firstLetter;
    
    //  NSString *frID, *frName, *frMobile;
    int valcount = 0;
    
    for (NSMutableArray *inObject in arrayNewsTableData)
    {
        NSMutableDictionary *data = [arrayNewsTableData objectAtIndex:valcount];
        
        NSLog(@"%@",[data objectForKey:@"news_date"]);
        
        firstLetter = [[[[data objectForKey:@"news_date"] stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]] substringToIndex:10] uppercaseString] ;
        
        // firstLetter = [cellData.Name substringToIndex:1];
        
        // Check if it's NOT a number
        if ([formatter numberFromString:firstLetter] == nil) {
            
            if (![objectsForCharacters objectForKey:firstLetter])
            {
                
                [arrayOfNames removeAllObjects];
                [arrayOfCharacters addObject:firstLetter];
            }
            
            [arrayOfNames addObject:[arrayNewsTableData objectAtIndex:valcount]];
            valcount = valcount+1;
            
            /**
             * Need to autorelease the copy to preven potential leak. Even though the
             * arrayOfNames is released below it still has a retain count of +1
             */
            [objectsForCharacters setObject:[arrayOfNames copy] forKey:firstLetter];
            
        }
    }
    [table reloadData];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewsDetailViewController *eventDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailVC"];
    eventDetailVC.dictForPage =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
}


@end
