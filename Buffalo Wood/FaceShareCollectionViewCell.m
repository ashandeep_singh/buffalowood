//
//  FaceShareCollectionViewCell.m
//  FaceShare
//
//  Created by Madhvi on 09/12/14.
//  Copyright (c) 2014 ___baltech___. All rights reserved.
//

#import "FaceShareCollectionViewCell.h"

@implementation FaceShareCollectionViewCell

@synthesize imgFaceShare;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
