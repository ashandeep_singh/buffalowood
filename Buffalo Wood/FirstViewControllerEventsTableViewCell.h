//
//  FirstViewControllerTableViewCell.h
//  Buffalo Wood
//
//  Created by Gaurav on 23/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewControllerEventsTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblTitle, *lblSubTitle, *lblDistance;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewNotation;
@end
