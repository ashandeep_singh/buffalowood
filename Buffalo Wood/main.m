//
//  main.m
//  Buffalo Wood
//
//  Created by Gaurav on 23/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
