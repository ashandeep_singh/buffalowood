//
//  SocialFeedViewController.m
//  Buffalo Wood
//
//  Created by Gaurav on 24/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "SocialFeedViewController.h"
#import "SocialFeedWithShareTableViewCell.h"
#import "SocialFeedTableViewCell.h"
#import <MessageUI/MessageUI.h> 


@interface SocialFeedViewController ()
{
    NSIndexPath *currentSelection;
}
@end

@implementation SocialFeedViewController
@synthesize arrayOfCharacters,objectsForCharacters;
int tagval;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Social";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    

    
   // self.navigationItem.leftBarButtonItem = [CommonFunctions getBackBtn];
    
    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
   
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    HUD.graceTime = 0.05f;
    
    
    selectedCellIndex = -1;
    ////--- g+
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    //signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = kGPlusClientID;
  
//    [GPPShare sharedInstance].delegate = self;
//    [GPPSignIn sharedInstance].clientID = kGPlusClientID;
//    [GPPDeepLink setDelegate:self];
//    [GPPDeepLink readDeepLinkAfterInstall];
    
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    //signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    
    
    
    ////----twitter
    _accountStore = [[ACAccountStore alloc] init];
    _apiManager = [[TWTAPIManager alloc] init];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(_refreshTwitterAccounts)
//                                                 name:ACAccountStoreDidChangeNotification
//                                               object:nil];
    
    [table setSeparatorInset:UIEdgeInsetsZero];
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary* resultArray = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"arrayTableData"]];
    
    arrayTableData = [[NSMutableArray alloc] init];
    
    if(resultArray.count==0)
    {
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(fetchDataFromServer:)
                       onTarget:self
                     withObject:nil
                       animated:YES];
        
        [table reloadData];

    }
    else
    {
        if ([resultArray[@"SocialData"] isKindOfClass:[NSArray class]])
        {
            arrayTableData = resultArray[@"SocialData"];
            [self setupIndexData];
        }
    }
    
    UIRefreshControl *refContr = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [refContr setBackgroundColor: [CommonFunctions colorWithHexString:@"174195"]];
    refContr.tintColor = [UIColor whiteColor];
    [refContr addTarget:self
                 action:@selector(fetchDataFromServer:)
       forControlEvents:UIControlEventValueChanged];
    [table addSubview:refContr];
    [refContr setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
    
    [[refContr.subviews objectAtIndex:0] setFrame:CGRectMake(30, 0, 20, 30)];

    
}

- (void)viewDidAppear:(BOOL)animated {
  
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)fetchDataFromServer:(UIRefreshControl *)refCntrl {
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"getrssItem.php?secureKey=%@",SiteSecureKey];
   // NSLog(@"%@",strUrl);
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
//    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];

                if (dic) {
                    NSData *valData = [NSKeyedArchiver archivedDataWithRootObject:dic];
                    
                    NSUserDefaults *defaults1=[NSUserDefaults standardUserDefaults];
                    [defaults1 setObject:valData forKey:@"arrayTableData"];
                    [defaults1 synchronize];
                    
                    // play with dic : server response
                    if ([dic[@"SocialData"] isKindOfClass:[NSArray class]]) {
                      //  arrayTableData = dic[@"SocialData"];
                        
                        /*
                         
                         For top filtering
                         
                         */
                         NSMutableArray *arrayMTableData = [[NSMutableArray alloc]init];
                        
                        arrayMTableData = dic[@"SocialData"];
                        
                        [arrayTableData removeAllObjects];
                        
                        for(int i=0;i<arrayMTableData.count;i++)
                        {
                            if(tagval==11)
                            {
                                if([[[arrayMTableData objectAtIndex:i] objectForKey:@"posttype"] isEqualToString:@"fb"])
                                {
                                    [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
                                }
                            }
                            else if(tagval==12)
                            {
                                if([[[arrayMTableData objectAtIndex:i] objectForKey:@"posttype"] isEqualToString:@"tw"])
                                {
                                    [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
                                }
                            }
                            else
                            {
                                [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
                            }
                        }
                        
                        if(tagval==11)
                        {
                            [btnFb setBackgroundColor:[UIColor whiteColor]];
                            [btnTw setBackgroundColor:[UIColor clearColor]];
                            [btnAll setBackgroundColor:[UIColor clearColor]];
                        }
                        else if(tagval==12)
                        {
                            [btnFb setBackgroundColor:[UIColor clearColor]];
                            [btnTw setBackgroundColor:[UIColor whiteColor]];
                            [btnAll setBackgroundColor:[UIColor clearColor]];
                            
                        }
                        else
                        {
                            
                            [btnFb setBackgroundColor:[UIColor clearColor]];
                            [btnTw setBackgroundColor:[UIColor clearColor]];
                            [btnAll setBackgroundColor:[UIColor whiteColor]];
                        }

                        
                        if (arrayTableData.count == 0){
                            [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"There is no item." andDelegate:self];
                        } else {
                            //[table reloadData];
                             [self setupIndexData];
                            
                        }
                    } else {
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
                    }
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
    else
    {
        if (refCntrl) {
            SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
            NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
            
            if (dic) {
                NSData *valData = [NSKeyedArchiver archivedDataWithRootObject:dic];
                
                NSUserDefaults *defaults1=[NSUserDefaults standardUserDefaults];
                [defaults1 setObject:valData forKey:@"arrayTableData"];
                [defaults1 synchronize];

                // play with dic : server response
                if ([dic[@"SocialData"] isKindOfClass:[NSArray class]]) {
                   // arrayTableData = dic[@"SocialData"];
                    /* for top filtering */
                    
                    NSMutableArray *arrayMTableData = [[NSMutableArray alloc]init];
                    
                    arrayMTableData = dic[@"SocialData"];
                    
                    [arrayTableData removeAllObjects];
                    
                    for(int i=0;i<arrayMTableData.count;i++)
                    {
                        if(tagval==11)
                        {
                            
                            
                            if([[[arrayMTableData objectAtIndex:i] objectForKey:@"posttype"] isEqualToString:@"fb"])
                            {
                                [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
                            }
                        }
                        else if(tagval==12)
                        {
                            
                            if([[[arrayMTableData objectAtIndex:i] objectForKey:@"posttype"] isEqualToString:@"tw"])
                            {
                                [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
                            }
                        }
                        else
                        {
                            
                            [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
                        }
                    }
                    if(tagval==11)
                    {
                        [btnFb setBackgroundColor:[UIColor whiteColor]];
                        [btnTw setBackgroundColor:[UIColor clearColor]];
                        [btnAll setBackgroundColor:[UIColor clearColor]];
                    }
                    else if(tagval==12)
                    {
                        [btnFb setBackgroundColor:[UIColor clearColor]];
                        [btnTw setBackgroundColor:[UIColor whiteColor]];
                        [btnAll setBackgroundColor:[UIColor clearColor]];

                    }
                    else
                    {
                        
                        [btnFb setBackgroundColor:[UIColor clearColor]];
                        [btnTw setBackgroundColor:[UIColor clearColor]];
                        [btnAll setBackgroundColor:[UIColor whiteColor]];
                    }
                    
                    if (arrayTableData.count == 0){
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"There is no item." andDelegate:self];
                    } else {
                        //[table reloadData];
                        if (refCntrl) {
                            
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:@"MMM d, h:mm a"];
                            NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                            NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                                        forKey:NSForegroundColorAttributeName];
                            NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                            refCntrl.attributedTitle = attributedTitle;
                            
                            [refCntrl endRefreshing];
                            
                        }
                        
                        [self setupIndexData];
                        
                    }
                } else {
                    [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Server Internal Error!"];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        } else {
            [CommonFunctions showServerNotFoundError];
        }
    }
}
- (void)setupIndexData {
    
    self.arrayOfCharacters = [[NSMutableArray alloc] init];
    self.objectsForCharacters = [[NSMutableDictionary alloc] init];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSMutableArray *arrayOfNames = [[NSMutableArray alloc] init];
    
    //  NSString *numbericSection = @"#";
    NSString *firstLetter;
    
    //  NSString *frID, *frName, *frMobile;
    int valcount = 0;
    
    for (NSMutableArray *inObject in arrayTableData)
    {
        NSMutableDictionary *data = [arrayTableData objectAtIndex:valcount];
        
    //    NSLog(@"%@",[data objectForKey:@"date"]);
        
        firstLetter = [[[[data objectForKey:@"date"] stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]] substringToIndex:10] uppercaseString] ;
        
        // firstLetter = [cellData.Name substringToIndex:1];
        
        // Check if it's NOT a number
        if ([formatter numberFromString:firstLetter] == nil) {
            
            if (![objectsForCharacters objectForKey:firstLetter])
            {
                
                [arrayOfNames removeAllObjects];
                [arrayOfCharacters addObject:firstLetter];
            }
            
            [arrayOfNames addObject:[arrayTableData objectAtIndex:valcount]];
            valcount = valcount+1;
            
            /**
             * Need to autorelease the copy to preven potential leak. Even though the
             * arrayOfNames is released below it still has a retain count of +1
             */
            [objectsForCharacters setObject:[arrayOfNames copy] forKey:firstLetter];
            
        }
    }
    [table reloadData];
    
}


#pragma mark - btn pressed
- (IBAction)btnEmailPressed:(id)sender{
    if([MFMailComposeViewController canSendMail])
    {
        
        selectedCellIndex = [(UIButton*)sender tag];
        
        NSDictionary *dataToPost = arrayTableData[selectedCellIndex];
        
        
    //    NSString  *emailto=@"info@games.com";
    //    
    //    NSString *recipients = [NSString stringWithFormat:@"mailto:%@?subject=feed share from iphone app",emailto];
    //    
    //    NSString *body =[NSString stringWithFormat:@"&body%@",dataToPost[@"description"]];
    //    
    //    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    //    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
        
        NSString *emailTitle = @"Feed share from iphone app";
        // Email Content
        NSString *messageBody = dataToPost[@"description"];
        // To address
        //NSArray *toRecipents = [NSArray arrayWithObject:@"support@2015woodbuffalo.com"];
        
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
   // NSLog (@"mail finished");
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)btnFBPressed:(id)sender{
    selectedCellIndex = [(UIButton*)sender tag];
    
     NSDictionary *dataToPost =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:((UIButton*)sender).titleLabel.tag]] objectAtIndex:selectedCellIndex];
    
//    [self postWithText:@""
//             ImageName:@"Icon-76@2x.png"
//                   URL:dataToPost[@"image"]
//               Caption:@"join me on Buffalo wood!"
//                  Name:dataToPost[@"name"]
//        andDescription:dataToPost[@"description"]];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
          if (![dataToPost[@"description"] isEqual:[NSNull null]]  && ![dataToPost[@"description"] isEqualToString:@""]) {
                [controller setInitialText:dataToPost[@"description"]];
         }
        
        if (![dataToPost[@"url"] isEqual:[NSNull null]]  && ![dataToPost[@"url"] isEqualToString:@""]) {
           [controller addURL:[NSURL URLWithString:dataToPost[@"url"]]];
        }
       
        if (![dataToPost[@"image"] isEqual:[NSNull null]]  && ![dataToPost[@"image"] isEqualToString:@""]) {
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:dataToPost[@"image"]]];
            [controller addImage:[UIImage imageWithData:data]];
        }
        
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else
    {
         [CommonFunctions AlertTitle:@"Facebook Account" withMsg:@"Facebook account must be configured via settings."];
    }
}


#pragma mark - alertview delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != 0) { // cancel btn is not pressed
//        UITextField * alertTextField = [alertView textFieldAtIndex:0];
        
    }
}
#pragma mark - Twitter

- (IBAction)btnTwitterPressed:(id)sender{
    
    selectedCellIndex = [(UIButton*)sender tag];
   
    
     NSDictionary *dataToPost =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:0]] objectAtIndex:selectedCellIndex];
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        if (![dataToPost[@"description"] isEqual:[NSNull null]]  && ![dataToPost[@"description"] isEqualToString:@""]) {
            [tweetSheet setInitialText:dataToPost[@"description"]];
        }
        
        if (![dataToPost[@"url"] isEqual:[NSNull null]]  && ![dataToPost[@"url"] isEqualToString:@""]) {
            [tweetSheet addURL:[NSURL URLWithString:dataToPost[@"url"]]];
        }
        
        if (![dataToPost[@"image"] isEqual:[NSNull null]]  && ![dataToPost[@"image"] isEqualToString:@""]) {
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:dataToPost[@"image"]]];
            [tweetSheet addImage:[UIImage imageWithData:data]];
        }

        
//        [tweetSheet setInitialText:dataToPost[@"description"]];
//        [tweetSheet addURL:[NSURL URLWithString:dataToPost[@"url"]]];
//        [tweetSheet addImage:[UIImage imageNamed:dataToPost[@"image"]]];

        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
         [CommonFunctions AlertTitle:@"Twitter Account" withMsg:@"Twitter account must be configured via settings."];
    }

}

#pragma mark -

- (void)_displayAlertWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buffalo Wood"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}



/**
 *  Handles the button press that initiates the token exchange.
 *
 *  We check the current configuration inside -[UIViewController viewDidAppear].
 */


#pragma mark g+ delegates and btn pressed
- (IBAction)btnGPlusPressed:(id)sender{
    selectedCellIndex = [(UIButton*)sender tag];
    
    if ([[GPPSignIn sharedInstance] authentication]) {
        // user login
        [self postOnGPlus];
    } else {
        [signIn authenticate];
    }
}

- (void)postOnGPlus {

    id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
    
    NSDictionary *dataToPost = arrayTableData[selectedCellIndex];
    
    [shareBuilder setPrefillText:[NSString stringWithFormat:@"%@\n%@",dataToPost[@"name"],dataToPost[@"description"]]];
    
    // Set any prefilled text that you might want to suggest
//    [shareBuilder setTitle:dataToPost[@"name"]
//               description:dataToPost[@"description"]
//              thumbnailURL:[NSURL URLWithString:dataToPost[@"image"]]];
    
    [shareBuilder setURLToShare:[NSURL URLWithString:dataToPost[@"image"]]];

    [shareBuilder setContentDeepLinkID:[NSString stringWithFormat:@"%@%d",SiteSecureKey,selectedCellIndex]];
    [shareBuilder open];
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    if (error) {
        // Do some error handling here.
        [CommonFunctions AlertTitle:@"Google plus" withMsg:@"Login error. Please try again later."];
    } else {
        [self postOnGPlus];
    }

}

- (void)presentSignInViewController:(UIViewController *)viewController {
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
}
// if require sign out
- (void)signOut {
    [[GPPSignIn sharedInstance] signOut];
}



#pragma mark - UITableView delegates

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [arrayOfCharacters count];
    
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"dd-MM-yyyy";
//    NSDate *yourDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",arrayOfCharacters[section]]];
//    dateFormatter.dateFormat = @"EEEE, dd MMMM";
//    return [NSString stringWithFormat:@"     %@",[dateFormatter stringFromDate:yourDate]];
//    
//}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy";
    NSDate *yourDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",arrayOfCharacters[section]]];
    dateFormatter.dateFormat = @"EEEE, dd MMMM";
    
    header.textLabel.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:yourDate]];
    header.textLabel.font = [UIFont fontWithName: @"Avenir Next" size: 15.0 ];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:section]] count];
}

//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    NSDictionary *dictForCell = [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
//    
//    if (![dictForCell[@"image"] isEqual:[NSNull null]] && ![dictForCell[@"image"] isEqualToString:@""]) {
//        if ([indexPath isEqual:currentSelection]) {
//            return 262.0f;
//        } else {
//            return 262.0f - 50;
//        }
//    } else {
//        if ([indexPath isEqual:currentSelection]) {
//            return 221.0f;
//        } else {
//            return 221.0f - 50;
//        }
//    }
//    
//    return 0;
//}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dictForCell = [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    if (![dictForCell[@"image"] isEqual:[NSNull null]] && ![dictForCell[@"image"] isEqualToString:@""])
    {
        if ([indexPath isEqual:currentSelection])
        {
            int rowHeight =0.0f;
            
            NSString *temp;
            
            if (![dictForCell[@"description"] isEqual:[NSNull null]])
            {
                temp = dictForCell[@"description"];
            }
            
            CGSize size = [temp sizeWithFont:[UIFont fontWithName:@"Avenir Next" size:14.0] constrainedToSize:CGSizeMake(299.0f, 999999.0f)];
            
            rowHeight = size.height + 130+157; // i use 10.0f pixel extra because depend on font
            
            return rowHeight;
            
        }
        else
        {
            int rowHeight =0.0f;
            
            NSString *temp;
            
            if (![dictForCell[@"description"] isEqual:[NSNull null]])
            {
                temp = dictForCell[@"description"];
            }
            
            CGSize size = [temp sizeWithFont:[UIFont fontWithName:@"Avenir Next" size:14.0] constrainedToSize:CGSizeMake(299.0f, 999999.0f)];
            
            rowHeight = size.height + 80+157; // i use 10.0f pixel extra because depend on font
            
            return rowHeight;
            
        }
    }
    else
    {
        if ([indexPath isEqual:currentSelection])
        {
            int rowHeight =0.0f;
            
            NSString *temp;
            
            if (![dictForCell[@"description"] isEqual:[NSNull null]])
            {
                temp = dictForCell[@"description"];
            }
            
            CGSize size = [temp sizeWithFont:[UIFont fontWithName:@"Avenir Next" size:14.0] constrainedToSize:CGSizeMake(299.0f, 999999.0f)];
            
            rowHeight = size.height + 130; // i use 10.0f pixel extra because depend on font
            
            return rowHeight;

        }
        else
        {
            int rowHeight =0.0f;
            
            NSString *temp;
            
            if (![dictForCell[@"description"] isEqual:[NSNull null]])
            {
                temp = dictForCell[@"description"];
            }
            
            CGSize size = [temp sizeWithFont:[UIFont fontWithName:@"Avenir Next" size:14.0] constrainedToSize:CGSizeMake(299.0f, 999999.0f)];
            
            rowHeight = size.height + 80; // i use 10.0f pixel extra because depend on font
            
            return rowHeight;
            
        }
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // NSMutableDictionary *dictForCell = [arrayTableData objectAtIndex:indexPath.row];
    
    NSDictionary *dictForCell =  [[objectsForCharacters objectForKey:[arrayOfCharacters objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    if (![dictForCell[@"image"] isEqual:[NSNull null]]  && ![dictForCell[@"image"] isEqualToString:@""]) {
    
        SocialFeedWithShareTableViewCell *cell=nil;
        static NSString *AutoCompleteRowIdentifier = @"SocialFeedWithShareTableViewCell";
        cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
        
        if (cell == nil)
        {
            cell = [[SocialFeedWithShareTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgView];
        }
        //    cell.selectionStyle = UITableViewCellAccessoryNone;
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = UIColorFromRedGreenBlue(22, 57, 138);
        [cell setSelectedBackgroundView:bgColorView];
        
        if ([indexPath isEqual:currentSelection]) {
            
            [UIView beginAnimations:@"fade" context:NULL];
            [UIView setAnimationDuration:0.7];
            [cell setBackgroundColor:UIColorFromRedGreenBlue(22, 57, 138)];
            cell.lblTitle.textColor = [UIColor whiteColor];
            cell.lblDate.textColor = [UIColor whiteColor];
            cell.txtViewDiscription.textColor = [UIColor whiteColor];
            cell.lblShare.textColor =  [CommonFunctions colorWithHexString:@"9ea9c8"];
            
            if ([dictForCell[@"posttype"] isEqualToString:@"tw"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-share-twitter-icon"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"fb"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-share-facebook-icon"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"gp"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"modal-share-google-icon"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"rss"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-rss-icon-active"];
            }
            
            
            [cell.imgstrip setHidden:NO];
            [cell.btnGPlus setHidden:NO];
            [cell.btnTwitter setHidden:NO];
            [cell.btnFB setHidden:NO];
            [cell.btnEmail setHidden:NO];
            [cell.lblShare setHidden:NO];
            
             [cell.imgViewType setFrame:CGRectMake(3, 10, 29, 29)];
            UIImage * toImage = [UIImage imageNamed:@"feed-share-background.png"];
            [UIView transitionWithView:cell.imgstrip
                              duration:0.7f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                cell.imgstrip.image = toImage;
                            } completion:nil];
            
            [UIView commitAnimations];
            
            
        } else {
            
            [cell.imgstrip setHidden:YES];
            [cell.btnGPlus setHidden:YES];
            [cell.btnTwitter setHidden:YES];
            [cell.btnFB setHidden:YES];
            [cell.btnEmail setHidden:YES];
            [cell.lblShare setHidden:YES];
            
            [UIView beginAnimations:@"fade" context:NULL];
            [UIView setAnimationDuration:0.7];
            [cell setBackgroundColor:[UIColor whiteColor]];
            cell.lblTitle.textColor = [UIColor blackColor];
            cell.lblDate.textColor = [UIColor blackColor];
            cell.txtViewDiscription.textColor = [UIColor blackColor];
            
            if ([dictForCell[@"posttype"] isEqualToString:@"tw"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-twitter-icon-active"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"fb"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-facebook-icon-active"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"gp"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"modal-share-google-icon"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"rss"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-rss-icon-active"];
            }
             [cell.imgViewType setFrame:CGRectMake(10, 15, 15, 16)];
            [UIView transitionWithView:cell.imgstrip
                              duration:0.7f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                cell.imgstrip.image =nil;
                            } completion:nil];
            
            [UIView commitAnimations];
                     }
        
        NSString *imgURL = dictForCell[@"image"];
        

            cell.imgView.image = [UIImage imageNamed:@"Placeholder.png"];
            cell.imgView.imageURL = [NSURL URLWithString:imgURL];


        if (![dictForCell[@"author"] isEqual:[NSNull null]])
        {
            if( [dictForCell[@"author"] isEqualToString:@"Western Canada Summer Games 2015 Wood Buffalo"])
            {
                cell.lblTitle.text = @"2015 Wood Buffalo";
            }
            else
            {
                cell.lblTitle.text = dictForCell[@"author"];
            }
        }
        else
        {
            cell.lblTitle.text = @"2015 Wood Buffalo";
        }

        
        @try{
            
           if (![dictForCell[@"description"] isEqual:[NSNull null]])
            {
                NSString *str = dictForCell[@"description"];
                
                cell.txtViewDiscription.text = [str stringByReplacingOccurrencesOfString:@"&quot;"
                                                      withString:@""];
                
                
            }
            else
            {
                 cell.txtViewDiscription.text=@"";
            }
       
        
        NSString *myString = [dictForCell objectForKey:@"date"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm";
        NSDate *yourDate = [dateFormatter dateFromString:myString];
        dateFormatter.dateFormat = @"HH:mm";
        cell.lblDate.text = [dateFormatter stringFromDate:yourDate];
            
         
        }
        @catch(NSException *ex)
        {
        
        }
        ////---- social share
        cell.btnEmail.tag = indexPath.row;
        cell.btnEmail.titleLabel.tag =indexPath.section;
        cell.btnFB.tag = indexPath.row;
        cell.btnFB.titleLabel.tag =indexPath.section;
        cell.btnTwitter.tag = indexPath.row;
        cell.btnTwitter.titleLabel.tag =indexPath.section;
        cell.btnGPlus.tag = indexPath.row;
         cell.btnGPlus.titleLabel.tag =indexPath.section;
        
        cell.clipsToBounds = YES;
        
        NSString *temp;
        
        if (![dictForCell[@"description"] isEqual:[NSNull null]])
        {
            temp = dictForCell[@"description"];
        }
        
        CGSize size = [temp sizeWithFont:[UIFont fontWithName:@"Avenir Next" size:14.0] constrainedToSize:CGSizeMake(299.0f, 999999.0f)];
        
        
        int valHeight = size.height + 90 + 157;
        
        
        [cell.imgstrip setFrame:CGRectMake(cell.imgstrip.frame.origin.x, valHeight-10, cell.imgstrip.frame.size.width, cell.imgstrip.frame.size.height)];
        
        [cell.btnEmail setFrame:CGRectMake(cell.btnEmail.frame.origin.x, valHeight, cell.btnEmail.frame.size.width, cell.btnEmail.frame.size.height)];
        
        [cell.btnFB setFrame:CGRectMake(cell.btnFB.frame.origin.x, valHeight, cell.btnFB.frame.size.width, cell.btnEmail.frame.size.height)];
        
        [cell.btnTwitter setFrame:CGRectMake(cell.btnTwitter.frame.origin.x, valHeight, cell.btnTwitter.frame.size.width, cell.btnEmail.frame.size.height)];
        
        [cell.btnGPlus setFrame:CGRectMake(cell.btnGPlus.frame.origin.x, valHeight, cell.btnGPlus.frame.size.width, cell.btnGPlus.frame.size.height)];
        
        [cell.lblShare setFrame:CGRectMake(cell.lblShare.frame.origin.x, valHeight+5, cell.lblShare.frame.size.width, cell.lblShare.frame.size.height)];
        
        [cell.txtViewDiscription setFrame:CGRectMake(cell.txtViewDiscription.frame.origin.x, cell.txtViewDiscription.frame.origin.y, cell.txtViewDiscription.frame.size.width, cell.btnGPlus.frame.size.height+valHeight)];
        
        return cell;
    }
    else {
        SocialFeedTableViewCell *cell=nil;
        static NSString *AutoCompleteRowIdentifier = @"SocialFeedTableViewCell";
        cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
        
        if (cell == nil)
        {
            cell = [[SocialFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
        }
        //    cell.selectionStyle = UITableViewCellAccessoryNone;
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = UIColorFromRedGreenBlue(22, 57, 138);
        [cell setSelectedBackgroundView:bgColorView];
        

        if ([indexPath isEqual:currentSelection])
        {
            [UIView beginAnimations:@"fade" context:NULL];
            [UIView setAnimationDuration:0.7];
//            cell.layer.transform = CATransform3DIdentity;
//            cell.alpha = 1;
//            cell.layer.shadowOffset = CGSizeMake(0, 0);
           
            [cell setBackgroundColor:UIColorFromRedGreenBlue(22, 57, 138)];
            cell.lblTitle.textColor = [UIColor whiteColor];
            cell.lblDate.textColor = [UIColor whiteColor];
            cell.txtViewDiscription.textColor = [UIColor whiteColor];
            cell.lblShare.textColor =  [CommonFunctions colorWithHexString:@"9ea9c8"];
            
            
            if ([dictForCell[@"posttype"] isEqualToString:@"tw"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-share-twitter-icon"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"fb"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-share-facebook-icon"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"gp"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"modal-share-google-icon"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"rss"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-rss-icon-active"];
            }
            [cell.imgViewType setFrame:CGRectMake(3, 10, 29, 29)];
            
            
            [cell.imgstrip setHidden:NO];
            [cell.btnGPlus setHidden:NO];
            [cell.btnTwitter setHidden:NO];
            [cell.btnFB setHidden:NO];
            [cell.lblShare setHidden:NO];
            [cell.btnEmail setHidden:NO];
            
            UIImage * toImage = [UIImage imageNamed:@"feed-share-background.png"];
            [UIView transitionWithView:cell.imgstrip
                              duration:0.7f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                cell.imgstrip.image = toImage;
                            } completion:nil];
            
             [UIView commitAnimations];
            
        } else {
            [UIView beginAnimations:@"fade" context:NULL];
            [UIView setAnimationDuration:0.7];
            [cell setBackgroundColor:[UIColor whiteColor]];
            cell.lblTitle.textColor = [UIColor blackColor];
            cell.lblDate.textColor = [UIColor blackColor];
            cell.txtViewDiscription.textColor = [UIColor blackColor];
            if ([dictForCell[@"posttype"] isEqualToString:@"tw"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-twitter-icon-active"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"fb"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-facebook-icon-active"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"gp"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"modal-share-google-icon"];
            } else if ([dictForCell[@"posttype"] isEqualToString:@"rss"]) {
                cell.imgViewType.image = [UIImage imageNamed:@"feed-rss-icon-active"];
            }
            
            [cell.imgViewType setFrame:CGRectMake(10, 15, 15, 16)];
            
            
            [UIView transitionWithView:cell.imgstrip
                              duration:0.7f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                cell.imgstrip.image =nil;
                            } completion:nil];
            
            [UIView commitAnimations];
            
            [cell.imgstrip setHidden:YES];
            [cell.btnGPlus setHidden:YES];
            [cell.btnTwitter setHidden:YES];
            [cell.btnFB setHidden:YES];
            [cell.lblShare setHidden:YES];
            [cell.btnEmail setHidden:YES];

        }
        
        if (![dictForCell[@"author"] isEqual:[NSNull null]])
        {
            if( [dictForCell[@"author"] isEqualToString:@"Western Canada Summer Games 2015 Wood Buffalo"])
            {
                 cell.lblTitle.text = @"2015 Wood Buffalo";
            }
            else
            {
                cell.lblTitle.text = dictForCell[@"author"];
            }
        }
        else
        {
            cell.lblTitle.text = @"2015 Wood Buffalo";
        }

       
         @try{
             if (![dictForCell[@"description"] isEqual:[NSNull null]])
             {
                 NSString *str = dictForCell[@"description"];
                 
                 cell.txtViewDiscription.text = [str stringByReplacingOccurrencesOfString:@"&quot;"
                                                                               withString:@""];
             }
             else
             {
                  cell.txtViewDiscription.text = @"";
             }
        
        NSString *myString = [dictForCell objectForKey:@"date"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm";
        NSDate *yourDate = [dateFormatter dateFromString:myString];
        dateFormatter.dateFormat = @"HH:mm";
        cell.lblDate.text = [dateFormatter stringFromDate:yourDate];
             
             
             
          }
        @catch(NSException *ex)
        {
        
        }
        ////---- social share
        cell.btnEmail.tag = indexPath.row;
        cell.btnEmail.titleLabel.tag =indexPath.section;
        cell.btnFB.tag = indexPath.row;
        cell.btnFB.titleLabel.tag =indexPath.section;
        cell.btnTwitter.tag = indexPath.row;
        cell.btnTwitter.titleLabel.tag =indexPath.section;
        cell.btnGPlus.tag = indexPath.row;
        cell.btnGPlus.titleLabel.tag =indexPath.section;
        
        cell.clipsToBounds = YES;
        
        NSString *temp;
        
        if (![dictForCell[@"description"] isEqual:[NSNull null]])
        {
            temp = dictForCell[@"description"];
        }
        
        CGSize size = [temp sizeWithFont:[UIFont fontWithName:@"Avenir Next" size:14.0] constrainedToSize:CGSizeMake(299.0f, 999999.0f)];
        
        
        int valHeight = size.height + 90;
        
        
        [cell.imgstrip setFrame:CGRectMake(cell.imgstrip.frame.origin.x, valHeight-10, cell.imgstrip.frame.size.width, cell.imgstrip.frame.size.height)];
        
        [cell.btnEmail setFrame:CGRectMake(cell.btnEmail.frame.origin.x, valHeight, cell.btnEmail.frame.size.width, cell.btnEmail.frame.size.height)];
        
         [cell.btnFB setFrame:CGRectMake(cell.btnFB.frame.origin.x, valHeight, cell.btnFB.frame.size.width, cell.btnEmail.frame.size.height)];
        
        [cell.btnTwitter setFrame:CGRectMake(cell.btnTwitter.frame.origin.x, valHeight, cell.btnTwitter.frame.size.width, cell.btnEmail.frame.size.height)];
        
        [cell.btnGPlus setFrame:CGRectMake(cell.btnGPlus.frame.origin.x, valHeight, cell.btnGPlus.frame.size.width, cell.btnGPlus.frame.size.height)];
        
        [cell.lblShare setFrame:CGRectMake(cell.lblShare.frame.origin.x, valHeight+5, cell.lblShare.frame.size.width, cell.lblShare.frame.size.height)];
        
        [cell.txtViewDiscription setFrame:CGRectMake(cell.txtViewDiscription.frame.origin.x, cell.txtViewDiscription.frame.origin.y, cell.txtViewDiscription.frame.size.width, cell.btnGPlus.frame.size.height+valHeight)];
        
        
        return cell;
    
    }

    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  //  NSLog(@"row selected in my list %d",indexPath.row);
    
    if([currentSelection isEqual:indexPath])
    {
        currentSelection=nil;
    }
    else
    {
        currentSelection = indexPath;
    }
    [table reloadData];
    
    
//    [tableView beginUpdates];
//    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//    [tableView endUpdates];
}



#pragma mark -  HUD
- (void)hudWasHidden:(MBProgressHUD *)hud{
    [hud removeFromSuperview];
}

#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)ButtonClick:(id)sender
{
    tagval = ((UIButton *)sender).tag;
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary* resultArray = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"arrayTableData"]];
    
    if ([resultArray[@"SocialData"] isKindOfClass:[NSArray class]])
    {
        NSMutableArray *arrayMTableData = [[NSMutableArray alloc]init];
        
        arrayMTableData =  resultArray[@"SocialData"];
        
        [arrayTableData removeAllObjects];
        
        for(int i=0;i<arrayMTableData.count;i++)
        {
            if(tagval==11)
            {
                if([[[arrayMTableData objectAtIndex:i] objectForKey:@"posttype"] isEqualToString:@"fb"])
                {
                    [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
                }
            }
            else if(tagval==12)
            {
                if([[[arrayMTableData objectAtIndex:i] objectForKey:@"posttype"] isEqualToString:@"tw"])
                {
                    [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
                }
            }
            else
            {
                [arrayTableData addObject:[arrayMTableData objectAtIndex:i]];
            }
        }
        if(tagval==11)
        {
            [btnFb setBackgroundColor:[UIColor whiteColor]];
            [btnTw setBackgroundColor:[UIColor clearColor]];
            [btnAll setBackgroundColor:[UIColor clearColor]];
        }
        else if(tagval==12)
        {
            [btnFb setBackgroundColor:[UIColor clearColor]];
            [btnTw setBackgroundColor:[UIColor whiteColor]];
            [btnAll setBackgroundColor:[UIColor clearColor]];
            
        }
        else
        {
            
            [btnFb setBackgroundColor:[UIColor clearColor]];
            [btnTw setBackgroundColor:[UIColor clearColor]];
            [btnAll setBackgroundColor:[UIColor whiteColor]];
        }
        [self setupIndexData];
    }

}

@end
