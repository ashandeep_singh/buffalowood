//
//  VideoDetailViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 23/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import "VideoDetailViewController.h"

@interface VideoDetailViewController ()
{
    MPMoviePlayerController *moviePlayer;
    double dislikeval;
     double likeval;
}
@end

@implementation VideoDetailViewController
@synthesize dictForPage;


- (void)viewDidLoad {
    [super viewDidLoad];
     self.title = @"Video Detail";
    
     [self.progressview setHidden:YES];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    
    self.navigationItem.leftBarButtonItem = [CommonFunctions getBackBtn];
    self.navigationController.navigationBarHidden = NO;
    _lblTitle.text = dictForPage[@"title"];
    
    NSString *myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                                   "<head> \n"
                                   "<style type=\"text/css\"> \n"
                                   "body {font-family: \"%@\"; font-size: %@; color:gray}\n"
                                   "</style> \n"
                                   "</head> \n"
                                   "<body>%@</body> \n"
                                   "</html>", @"Avenir Next", [NSNumber numberWithInt:14], dictForPage[@"description"]];
    
    [_lblDiscription loadHTMLString:myDescriptionHTML baseURL:nil];
    
     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:dictForPage[@"created_on"]];
    dateFormatter.dateFormat = @"MMMM dd";
    NSMutableString *startDate = [[dateFormatter stringFromDate:yourDate] mutableCopy];
    
    _lblDate.text = [NSString stringWithFormat:@"%@",startDate];

    _lblAddedBy.text =[NSString stringWithFormat:@"by %@", dictForPage[@"added_by"]];
    
    NSString *strUrl=[NSString stringWithFormat:@"https://gdata.youtube.com/feeds/api/videos/%@?v=2&alt=json",dictForPage[@"video_id"]];
    NSLog(@"%@",strUrl);
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (!error && jsonStr) {
        SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
        NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];

      _lblCount.text = [[[dic objectForKey:@"entry"] objectForKey:@"yt$statistics"] objectForKey:@"viewCount"];
        
        dislikeval =[[[[dic objectForKey:@"entry"] objectForKey:@"yt$rating"] objectForKey:@"numDislikes"] intValue];
        
        likeval = [[[[dic objectForKey:@"entry"] objectForKey:@"yt$rating"] objectForKey:@"numLikes"] intValue];
    
        if(dislikeval >0 && likeval>0)
        {
            NSLog(@"%f",likeval/(likeval+dislikeval)*100);
            
            double valfinal = likeval/(likeval+dislikeval)*100;
            self.progressview.progress = valfinal/100;
              [self.progressview setHidden:NO];
        }
        else if(likeval>0)
        {
            self.progressview.progress = 1;
            [self.progressview setHidden:NO];
        }
        else
        {
            [self.progressview setHidden:YES];
        }
    
    }
    
     [self playMovie];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
        UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(handleSwipeFromRight:)];
    
        gesture.direction = UISwipeGestureRecognizerDirectionRight;
    
        [self.view addGestureRecognizer:gesture];
    
}
- (void)handleSwipeFromRight:(UISwipeGestureRecognizer *)recognizer
{
    [self BackBtnClicked:recognizer];
}
-(IBAction)BackBtnClicked:(id)sender{
   
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)playMovie
{
//        NSString * myString = dictForPage[@"video_id"];
//        
//        NSString * newString = [NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",myString];
//        
//        
//        NSString *embedHTML = [NSString stringWithFormat:@"<iframe width=\"320\" height=\"205\" src=\"%@%@&autoplay=true&hl=en_US\" frameborder=\"0\" allowfullscreen></iframe>", newString,@"?rel=0&hd=1"];
//    
//        [_webview loadHTMLString:embedHTML baseURL:nil];
    
     NSString * myString = dictForPage[@"video_id"];
   
    NSMutableString *html = [NSMutableString string];
    [html appendString:@"<html>"];
    [html appendString:@"<head>"];
    [html appendString:@"<style type=\"text/css\">"];
    [html appendString:@"body {"];
    [html appendString:@"background-color: transparent;"];
    [html appendString:@"color: white;"];
    [html appendString:@"margin: 0;"];
    [html appendString:@"}"];
    [html appendString:@"</style>"];
    [html appendString:@"</head>"];
    [html appendString:@"<body>"];
    [html appendFormat:@"<iframe id=\"ytplayer\" type=\"text/html\" width=\"%0.0f\" height=\"%0.0f\" src=\"http://www.youtube.com/embed/%@\" frameborder=\"0\"/>", 320.0, 190.0, myString];
    [html appendString:@"</body>"];
    [html appendString:@"</html>"];
    
    
    [_webview loadHTMLString:html baseURL:nil];
}

- (void)moviePlaybackComplete:(NSNotification *)notification
{
    MPMoviePlayerController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerController];
    
    [moviePlayerController.view removeFromSuperview];
    
}

@end
