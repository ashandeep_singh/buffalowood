//
//  SponsorsViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 16/11/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SponsorsViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *hud;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *arrayOfCharacters;
@property (nonatomic, retain) NSMutableDictionary *objectsForCharacters;

@end
