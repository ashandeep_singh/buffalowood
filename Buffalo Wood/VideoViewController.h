//
//  VideoViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 23/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    IBOutlet UITableView *table;
    IBOutlet UIScrollView *scrollview;
    
}

@end
