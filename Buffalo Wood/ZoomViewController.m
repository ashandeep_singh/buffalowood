//
//  ZoomViewController.m
//  testblur
//
//  Created by Ashandeep Singh on 25/02/15.
//  Copyright (c) 2015 Baltech. All rights reserved.
//

#import "ZoomViewController.h"

@interface ZoomViewController ()
{
    UIView *popup;
    UITextField *popUpEmail;
}
@end

@implementation ZoomViewController
@synthesize arrImage;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   // self.navigationItem.leftBarButtonItem = [CommonFunctions getBackBtn];
    
    self.navigationController.navigationBarHidden = YES;
    
    NSString *imgURL =[NSString stringWithFormat:@"%@images-game/%@",SiteURL,[arrImage objectForKey:@"image"]];
   
   [imgView setImageFromURL:imgURL showActivityIndicator:YES setCacheImage:YES];
    
    self.lblName.text =[arrImage objectForKey:@"image_title"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[arrImage objectForKey:@"create_on"]]];
    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    
    self.lblDate.text = [dateFormatter stringFromDate:yourDate];
    
    self.lblLikes.text = [arrImage objectForKey:@"likes"];
    
    [self.bkbtnzoom setFrame:CGRectMake(12, 30, 23, 23)];
    
    NSString *userEmail = [CommonFunctions getUseremail];
    
    if(![userEmail isKindOfClass:[NSString class]])
    {
        [self showPopUp];
    }
    else
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.delegate = self;
        HUD.graceTime = 0.05f;
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(fetchLiksFromServer)
                       onTarget:self
                     withObject:nil
                       animated:YES];
    }
}
- (void)fetchLiksFromServer {
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"checkIsLike.php?secureKey=%@&email_id=%@&image_id=%@",SiteSecureKey,[CommonFunctions getUseremail],[arrImage objectForKey:@"id"]];
    NSLog(@"%@",strUrl);
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (![NSThread isMainThread])
    {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error && jsonStr)
            {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
                
                if (dic)
                {
                    // play with dic : server response
                        @try{
                            
                               self.lblLikes.text = [dic[@"Message"] objectForKey:@"LikeCount"];
                            
                                if([[dic[@"Message"] objectForKey:@"IsLike"] isEqualToString:@"1"])
                                {
                                    [self.btnlike setImage:[UIImage imageNamed:@"images-like-icon-active"] forState:UIControlStateNormal];
                                }
                                else
                                {
                                     [self.btnlike setImage:[UIImage imageNamed:@"images-like-icon"] forState:UIControlStateNormal];
                                }
                            
                            }
                        @catch(NSException *ex)
                            {
                                 [self.btnlike setImage:[UIImage imageNamed:@"images-like-icon"] forState:UIControlStateNormal];
                            }
                        
                        }
                    }
                 else {
                    [CommonFunctions showServerNotFoundError];
                }
            
        });
    }
}


- (NSString *)stringByURLEncode:imgurl {
    NSMutableString *tempStr = [NSMutableString stringWithString:imgurl];
    
    return [[NSString stringWithFormat:@"%@",tempStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Scroll View Delegate

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView1{
    return imgView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView1 {
    
      [self.bkbtnzoom setFrame:CGRectMake(12, 30, 23, 23)];
    
    UIView *subView = [scrollView1.subviews objectAtIndex:0];
    
    CGFloat offsetX = MAX((scrollView1.bounds.size.width - scrollView1.contentSize.width) * 0.5, 0.0);
    CGFloat offsetY = MAX((scrollView1.bounds.size.height - scrollView1.contentSize.height) * 0.5, 0.0);
    
    subView.center = CGPointMake(scrollView1.contentSize.width * 0.5 + offsetX,
                                 scrollView1.contentSize.height * 0.5 + offsetY);
}
-(IBAction)BackBtnClicked:(id)sender{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:.50
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                       [self.navigationController popViewControllerAnimated:NO];
                    }
                    completion:nil];
}
- (IBAction)btnShareApp:(id)sender {
   // UIImage *shareImage = imgView.image;
    NSString *text = [arrImage objectForKey:@"image_title"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@images-game/%@",SiteURL,[arrImage objectForKey:@"image"]]];
    UIImage *image =  imgView.image;
    
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[text, url, image]
     applicationActivities:nil];
    
    [self presentViewController:controller animated:YES completion:nil];
    
}
- (IBAction)btnLikeImage:(id)sender {
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"likeImage.php?secureKey=%@&email_id=%@&image_id=%@",SiteSecureKey,[CommonFunctions getUseremail],[arrImage objectForKey:@"id"]];
    NSLog(@"%@",strUrl);
    
    NSError *error;
    NSString *jsonStr=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (!error && jsonStr) {
        SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
        NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr error:nil];
#ifdef DEBUG
        NSLog(@"%@",dic);
#endif
        if (dic) {
            
            _lblLikes.text = [[dic objectForKey:@"Message"] objectForKey:@"LikeCount"];
            
            if([[dic[@"Message"] objectForKey:@"IsLike"] isEqualToString:@"1"])
            {
                [self.btnlike setImage:[UIImage imageNamed:@"images-like-icon-active"] forState:UIControlStateNormal];
            }
            else
            {
                [self.btnlike setImage:[UIImage imageNamed:@"images-like-icon"] forState:UIControlStateNormal];
            }
         }
        
     }
}

- (void)showPopUp {
    
    //--- create view for popup
    popup = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [popup setBackgroundColor:[UIColor clearColor]];
    
    // for light black background
    UIView *popupTempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    popupTempView.alpha = 0.5f;
    [popupTempView setBackgroundColor:[UIColor blackColor]];
    [popup addSubview:popupTempView];
    
    //--- add on popup view
    UILabel *popUpLbl = [[UILabel alloc] initWithFrame:CGRectMake(45, 165, 230, 120)];
    [popUpLbl setBackgroundColor:UIColorFromRGB(0xEEEEEE)];
    popUpLbl.layer.cornerRadius = 4.0f;
    popUpLbl.clipsToBounds = YES;
    [popup addSubview:popUpLbl];
    
    
    UILabel *popUpTitle = [[UILabel alloc] initWithFrame:CGRectMake(125, 180, 100, 20)];
    [popUpTitle setTextColor:UIColorFromRGB(0x405568)];
    //[popUpTitle setFont:UIFontSourceSansProRegularSize(17.0f)];
    [popUpTitle setText:@"Photos"];
    [popup addSubview:popUpTitle];
    
    popUpEmail = [[UITextField alloc] initWithFrame:CGRectMake(58, 210, 200, 25)];
    [popUpEmail setTextColor:UIColorFromRGB(0x405568)];
    //[popUpDiscription setFont:[]];
    [popUpEmail setPlaceholder:@"Enter email address"];
    [popUpEmail setTextAlignment:NSTextAlignmentCenter];
    [popUpEmail setBorderStyle:UITextBorderStyleNone];
    [popUpEmail becomeFirstResponder];
    [popup addSubview:popUpEmail];
    
    
    //--- ok buttons
    UIButton *btnOk = [[UIButton alloc] initWithFrame:CGRectMake(112, 245, 95, 30)];
    [btnOk setBackgroundColor:UIColorFromRGB(0x405568)];
    [btnOk setTitle:@"Ok" forState:UIControlStateNormal];
    //[btnOk.titleLabel setFont:UIFontSourceSansProRegularSize(17.0f)];
    [btnOk setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnOk addTarget:self
              action:@selector(btnInsidePopUpPressed)
    forControlEvents:UIControlEventTouchUpInside];
    btnOk.userInteractionEnabled = YES;
    btnOk.layer.cornerRadius = 4;
    btnOk.clipsToBounds = YES;
    [popup addSubview:btnOk];
    
    [mainWindow addSubview:popup];
}

- (void)btnInsidePopUpPressed{
    if([popUpEmail.text isEqualToString:@""])
    {
        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Please enter your email address"];
    }
    else
    {
        
        NSString *emailadr = [[popUpEmail.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]]lowercaseString];
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:emailadr forKey:@"useremail"];
        [defaults synchronize];
        [popup removeFromSuperview];
        popup = nil;
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.delegate = self;
        HUD.graceTime = 0.05f;
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(fetchLiksFromServer)
                       onTarget:self
                     withObject:nil
                       animated:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteNotification" object:@""];
      
    }
    
}
@end
