//
//  EventCategoryViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 09/09/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "EventCategoryViewController.h"
#import "EventsViewController.h"

@interface EventCategoryViewController ()
{
   
}
@end

@implementation EventCategoryViewController

NSIndexPath *currentIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    
    self.title = @"Categories";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    if (arrayEventsAllCats == nil || arrayEventsAllCats.count == 0)
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.delegate = self;
        HUD.graceTime = 0.05f;
        
        [mainWindow addSubview:HUD];
        [HUD showWhileExecuting:@selector(getCatsFromServer)
                       onTarget:self
                     withObject:nil
                       animated:YES];
  
    }
    else
    {
          [table reloadData];
    }
}
- (void)getCatsFromServer {
    
    NSString *strUrl=[SiteAPIURL stringByAppendingFormat:@"getCategory.php?secureKey=%@",SiteSecureKey];
    
    NSError *error2;
    NSString *jsonStr2=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error2];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error2 && jsonStr2) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr2 error:nil];
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    // play with dic : server response
                    if ([dic[@"categories"] isKindOfClass:[NSArray class]]) {
                        arrayEventsAllCats = dic[@"categories"];
                        [arrayEventsAllCats insertObject:@"All" atIndex:0];
                    } else  {
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Categories not found!"];
                    }
                    
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
        });
    }
    
    
    // call if all work is done
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ( (arrayEventsAllCats != nil && arrayEventsAllCats.count > 0) || (arrayEventsAllCats != nil && arrayEventsAllCats.count > 0) ) {
                [table reloadData];
            } });
    }
    
}

#pragma mark - UITableView delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex{
    return arrayEventsAllCats.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"catCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
        //        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgProduct];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    NSDictionary *dictForCell = arrayEventsAllCats[indexPath.row];
    
    if(indexPath.row==0)
    {
        cell.textLabel.text = @"All";
    }
    else
    {
        cell.textLabel.text = dictForCell[@"name"];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if(indexPath.row==0 && currentIndex==nil)
    {
      cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    
    if(currentIndex!=nil && indexPath.row == currentIndex.row)
    {
         cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    UIFont *myFont = [ UIFont fontWithName: @"Avenir Next" size: 16.0 ];
    cell.textLabel.font  = myFont;

    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(currentIndex != nil)
    {
       UITableViewCell *cell = [tableView cellForRowAtIndexPath:currentIndex];
        
        
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
       UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    
     cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
     currentIndex = indexPath;
    
    if(indexPath.row==0)
    {
        [self Cancelclick];
    }
    else
    {
        [self closeclick:nil];
    }
    //    EventsViewController *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
//    EventDetailViewController *eventDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
//    eventDetailVC.dictForPage = [arrayEvents objectAtIndex:indexPath.row];
//    
//    [self.navigationController pushViewController:eventDetailVC animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)closeclick:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSDictionary *dictForCell = arrayEventsAllCats[currentIndex.row];
    
    NSLog(@"%@",dictForCell[@"id"]);
    
      [[NSNotificationCenter defaultCenter] postNotificationName:@"dealNotification" object: [NSString stringWithFormat:@"%@",dictForCell[@"id"]]];
}

-(void)Cancelclick
{
    currentIndex = nil;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dealNotification" object:@""];
}
-(IBAction)Backclick:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
  /*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
