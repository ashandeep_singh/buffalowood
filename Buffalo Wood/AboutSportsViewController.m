//
//  AboutSportsViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 22/11/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "AboutSportsViewController.h"
#import "FeactureTableViewCell.h"

@interface AboutSportsViewController ()
{
    NSArray *Sportsdata;
}
@end
@implementation AboutSportsViewController
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    
    self.title = @"Official Sports";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];

    
    [scrollView setContentOffset:CGPointMake(0,0)];
    [scrollView setContentSize:CGSizeMake(320, 1250)];
    Sportsdata = @[@{@"title": @"Artistic Gymnastics",@"image":@"artistic-gymnastics-icon-large.png"},
                         @{@"title": @"Athletics",@"image":@"athletics-icon-large.png"},
                         @{@"title": @"Badminton",@"image":@"badminton-icon-large.png"},
                         @{@"title": @"Baseball",@"image":@"baseball-icon-large.png"},
                         @{@"title": @"Basketball",@"image":@"basketball-icon-large.png"},
                         @{@"title": @"Beach Volleyball",@"image":@"beach-volleyball-icon-large.png"},  @{@"title": @"Canoe/Kayak",@"image":@"canoe-kayak-icon-large.png"}, @{@"title": @"Cycling",@"image":@"cycling-icon-large.png"}, @{@"title": @"Golf",@"image":@"golf-icon-large.png"}, @{@"title": @"Judo",@"image":@"judo-icon-large.png"},  @{@"title": @"Rowing",@"image":@"rowing-icon-large.png"}, @{@"title": @"Soccer",@"image":@"soccer-icon-large.png"}, @{@"title": @"Softball",@"image":@"softball-icon-large.png"},  @{@"title": @"Swimming",@"image":@"swimming-icon-large.png"}, @{@"title": @"Tennis",@"image":@"results-tennis.png"}, @{@"title": @"Triathlon",@"image":@"triathlon-icon-large.png"},
                            @{@"title": @"Volleyball",@"image":@"volleyball-icon-large.png"}, @{@"title": @"Wrestling",@"image":@"wrestling-icon-large.png"}];
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View Delegates and Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [Sportsdata count]/2;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SportsCell";
    
    FeactureTableViewCell *cell =  [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[FeactureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    int cntRow = indexPath.row*2;
    
     NSDictionary *dictForCell = [Sportsdata objectAtIndex:cntRow];
    
    cell.imgView1.image = [UIImage imageNamed:dictForCell[@"image"]];
    
    cell.lblName1.text = dictForCell[@"title"];

    
    NSDictionary *dictForCell1 = [Sportsdata objectAtIndex:cntRow+1];
    
    cell.imgView2.image = [UIImage imageNamed:dictForCell1[@"image"]];
    
    cell.lblName2.text = dictForCell1[@"title"];
    
    
    return cell;

   
}
@end
