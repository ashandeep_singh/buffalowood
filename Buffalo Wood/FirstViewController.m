//
//  FirstViewController.m
//  Buffalo Wood
//
//  Created by Gaurav on 23/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "FirstViewController.h"
#import "FirstViewControllerEventsTableViewCell.h"
#import "EventsViewController.h"
#import "EventDetailViewController.h"
#import "DemoViewController.h"

@interface FirstViewController ()
{
    NSArray *arrayMascot;
    NSArray *arrayEvents;
    
    ///// ----- for parallax effect
    CGRect defaultMapViewFrame;
    
    //// --- for long press gesture
    BOOL isMapViewInFullScreen;

}
@end

@implementation FirstViewController


@synthesize wbview;

- (void)viewDidLoad
{
    if([isRepeat isEqualToString:@"0"])
    {
        DemoViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DemoVC"];
        
        [self.navigationController pushViewController:mapVC animated:NO];
    }
  
    [self performSelector:@selector(binddataformain)
                    withObject:nil
                    afterDelay:1.0];
    
    [super viewDidLoad];
    
 
}
-(void)binddataformain
{
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"Map";
    //[UIFont fontWithName: @"Avenir Next" size: 15.0 ];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    UIImage *image = [UIImage imageNamed:@"navbar-background.png"];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    self.navigationItem.leftBarButtonItem = nil;
    
    
    self.navigationItem.hidesBackButton = YES;
    
    
    
    // self.navigationController.navigationBar.translucent = YES;
    
    [table.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [table.layer setBorderWidth:1.0f];
    
    ///// ----- for parallax effect -- start
    void *context = (__bridge void *)self;
    [table addObserver:self
            forKeyPath:@"contentOffset"
               options:NSKeyValueObservingOptionNew
               context:context];
    defaultMapViewFrame = viewForMap.frame;
    
    //// ---- long presses gesture
    isMapViewInFullScreen = NO;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                       action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.4f; //seconds
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [viewForMap addGestureRecognizer:lpgr];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *resultArray = [defaults objectForKey:@"MapsArray"];
    
    if(resultArray.count==0)
    {
        [self loadmapdata];
    }
    else
    {
        if ([resultArray[@"Events"] isKindOfClass:[NSArray class]])
        {
            
            arrayEvents = resultArray[@"Events"];
        }
        if ([resultArray[@"Mascot"] isKindOfClass:[NSArray class]])
        {
            arrayMascot = resultArray[@"Mascot"];
        }
        
        [table reloadData];
        
        [self loadMap];
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    // [self loadmapdata];
//
//    [self performSelector:@selector(getDataFromServer)
//               withObject:nil
//               afterDelay:1.0];
    
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    
    dispatch_async(myQueue, ^{
        [self getDataFromServer];
    });

}
-(void)loadmapdata
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    HUD.graceTime = 0.05f;
    
    [mainWindow addSubview:HUD];
    [HUD showWhileExecuting:@selector(getDataFromServer)
                   onTarget:self
                 withObject:nil
                   animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    viewForMap.frame = defaultMapViewFrame;
    [table scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}
-(void)loadMap
{
    NSMutableArray *arrVal = [[NSMutableArray alloc] init];
    NSMutableArray *arrMascot = [[NSMutableArray alloc] init];
    
    for(int i=0;i<arrayEvents.count;i++)
    {
        NSDictionary *dictForCell = arrayEvents[i];
        
        NSString* newString = [NSString stringWithFormat:@"[%@,%@,'%@','%d']",dictForCell[@"lat"], dictForCell[@"long"], dictForCell[@"name"],i];
        
        [arrVal addObject:newString];
    }
  
    if(arrayMascot.count>0)
    {
        NSDictionary *dictForCell = arrayMascot[0];
        
        NSString* newString = [NSString stringWithFormat:@"[%@,%@,'%@']",dictForCell[@"lat"], dictForCell[@"long"], dictForCell[@"address"]];
        
        [arrMascot addObject:newString];

    }
	// Do any additional setup after loading the view, typically from a nib.
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"htmldata" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    
    
    NSString *neededstring = @"var addressPoints =";
    
    for(int i=0;i<[arrVal count];i++)
    {
        if(i==0)
        {
            neededstring = [NSString stringWithFormat:@"%@[%@,",neededstring,arrVal[i]];
        }
        else if(i>0 && i<[arrVal count]-1)
        {
            neededstring = [NSString stringWithFormat:@"%@%@,",neededstring,arrVal[i]];
        }
        else
        {
            neededstring = [NSString stringWithFormat:@"%@%@",neededstring,arrVal[i]];
        }
    }
    
    neededstring = [NSString stringWithFormat:@"%@];",neededstring];
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"var addressPoints = [];"
                                                       withString:neededstring];
    
    NSString *neededmuscotstring = @"var mascotPoints =";
    
    neededmuscotstring = [NSString stringWithFormat:@"%@%@;",neededmuscotstring,arrMascot[0]];
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"var mascotPoints = [];"
                                                       withString:neededmuscotstring];
    
    NSString *currLoc;
    
   // NSLog(@"%@",[arrayEvents[0] objectForKey:@"lat"]);
    
    
        currLoc = [NSString stringWithFormat:@".setView([%@,%@], 13);",[arrayEvents[0] objectForKey:@"lat"], [arrayEvents[0] objectForKey:@"long"]];
         //currLoc =@".setView([45.2501566,-75.8002568], 10);";
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@".setView([-37.82, 175.215], 14);"
                                                       withString:currLoc];

    
    [wbview loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"http://2015woodbuffalo.com"]];
}
#pragma mark - KVO Methods
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
	// Make sure we are observing this value.
	if (context != (__bridge void *)self) {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
		return;
	}
    
    if ((object == table) &&
        ([keyPath isEqualToString:@"contentOffset"] == YES)) {
        [self scrollViewDidScrollWithOffset:table.contentOffset.y];
    }
    
}

- (void)scrollViewDidScrollWithOffset:(CGFloat)scrollOffset
{
    if (scrollOffset >= defaultMapViewFrame.size.height) {
        CGRect newMapFrame = viewForMap.frame;
        newMapFrame.size.height = 0;
        viewForMap.frame = newMapFrame;
    } else {
        CGRect newMapFrame = viewForMap.frame;
        newMapFrame.size.height = defaultMapViewFrame.size.height - scrollOffset;
        viewForMap.frame = newMapFrame;
    }
}

///// ----- for parallax effect -- end

////// ------- long press gesture

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateRecognized) {
        
        if (isMapViewInFullScreen ) {
            [UIView animateWithDuration:1.0
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^ {
                                 viewForMap.frame = defaultMapViewFrame;
                                 [table scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                             }
                             completion:^(BOOL finished) {
                                 
                             }];
        }
        else {
            [UIView animateWithDuration:1.0
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^ {
                                 viewForMap.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                                 
                             }
                             completion:^(BOOL finished) {
                                 
                             }];
        }
        
        isMapViewInFullScreen = !isMapViewInFullScreen;
    }
}


- (void)getDataFromServer {
    
    
    NSString *strUrl2=[SiteAPIURL stringByAppendingFormat:@"getEvents.php?secureKey=%@&device_id=%@",SiteSecureKey,appDeviceToken];
    NSLog(@"%@",strUrl2);
    NSError *error2;
    NSString *jsonStr2=[[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl2]
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error2];
    //    NSLog(@"%@",jsonStr);
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error2 && jsonStr2) {
                SBJsonParser *jsonParser=[[SBJsonParser alloc] init];
                NSMutableDictionary *dic=[jsonParser objectWithString:jsonStr2 error:nil];
                
#ifdef DEBUG
                NSLog(@"%@",dic);
#endif
                if (dic) {
                    
                    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                    [defaults setObject:dic forKey:@"MapsArray"];
                    [defaults synchronize];
                    
                    // play with dic : server response
                    if ([dic[@"Events"] isKindOfClass:[NSArray class]])
                    {
                        if([dic[@"Events"] count] != arrayEvents.count)
                        {
                            arrayEvents = dic[@"Events"];
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Events not found!"];
                    }
                    if ([dic[@"Mascot"] isKindOfClass:[NSArray class]])
                    {
                        arrayMascot = dic[@"Mascot"];
                    }
                    else
                    {
                        [CommonFunctions AlertTitle:@"Buffalo Wood" withMsg:@"Mascot not found!"];
                    }
                    
                     [self loadMap];
                    
                } else {
                    [CommonFunctions showServerNotFoundError];
                }
            } else {
                [CommonFunctions showServerNotFoundError];
            }
    
    
    // call if all work is done
   
            if ( (arrayMascot != nil && arrayMascot.count > 0) || (arrayEvents != nil && arrayEvents.count > 0) ) {
              //  [self setMapView:viewForMap];
                [table reloadData];
            }
        
        });
    }
}


//for (NSDictionary *dic in arrayEvents) {
//    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([dic[@"lat"] floatValue],[dic[@"long"] floatValue]);
//    //        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(45.40f,75.60f);
//    RMAnnotation *annotation = [RMAnnotation annotationWithMapView:mapView
//                                                        coordinate:location
//                                                          andTitle:dic[@"name"]];
//}
//    // focus on map
//    NSDictionary *dic = arrayMascot[0];
//    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([dic[@"lat"] floatValue],[dic[@"long"] floatValue]);
//    [mapView setCenterCoordinate:location];
//    
//    RMAnnotation *annotation = [RMAnnotation annotationWithMapView:mapView
//                                                        coordinate:location
//
- (IBAction)btnSportsPressed:(id)sender{
    NSLog(@"btnSportsPressed");
}
- (IBAction)btnVenuesPressed:(id)sender{
    NSLog(@"btnVenuesPressed");
}
- (IBAction)btnEventsPressed:(id)sender{
    NSLog(@"btnEventsPressed");
}


#pragma mark - UITableView delegates
//- (void)table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex{
    return arrayEvents.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FirstViewControllerEventsTableViewCell *cell=nil;
    static NSString *AutoCompleteRowIdentifier = @"FirstViewControllerEventsTableViewCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[FirstViewControllerEventsTableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
        //        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgProduct];
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    NSDictionary *dictForCell = arrayEvents[indexPath.row];
    
    double distanceInKm = [CommonFunctions distanceFromMyLocationWithLat:[dictForCell[@"lat"] floatValue]
                                                                 andLong:[dictForCell[@"long"] floatValue]];
    
    
    
    cell.lblTitle.text = dictForCell[@"name"];
    cell.lblDistance.text = [NSString stringWithFormat:@"%0.0lf Km",distanceInKm];
    cell.lblSubTitle.text = dictForCell[@"province"];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"row selected in my list %ld",(long)indexPath.row);
    EventDetailViewController *eventDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
    eventDetailVC.dictForPage =  [arrayEvents objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
}


#pragma mark - HUD delegates
- (void)hudWasHidden:(MBProgressHUD *)hud{
    [hud removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    // only do something if a link has been clicked...
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        
        // check if the url requests starts with our custom protocol:
        if ([[[request URL] absoluteString] hasPrefix:@"http://"])
        {
          
            NSString *newString = [NSString stringWithFormat:@"%@",[request URL]];
            
            newString =  [newString substringToIndex:newString.length-1];

            NSString *finalString = [newString stringByReplacingOccurrencesOfString:@"http://"
                                                               withString:@""];

            EventDetailViewController *eventDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
            eventDetailVC.dictForPage =  [arrayEvents objectAtIndex:[finalString intValue]];
            [self.navigationController pushViewController:eventDetailVC animated:YES];

            
            return NO;
        }
        
         return NO;
    }
    
    return YES;
}


@end
