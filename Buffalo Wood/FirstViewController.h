//
//  FirstViewController.h
//  Buffalo Wood
//
//  Created by Gaurav on 23/08/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FirstViewController : UIViewController<MBProgressHUDDelegate, UIGestureRecognizerDelegate>
{
    IBOutlet UIView *viewForMap, *viewForBtns;
    IBOutlet UITableView *table;
    IBOutlet UIButton *btnSports, *btnVenues, *btnEvents;
    IBOutlet UILabel *lblBtnUnderLine;
    
    MBProgressHUD *HUD;
}

@property (nonatomic, retain) IBOutlet UIWebView *wbview;
- (IBAction)btnSportsPressed:(id)sender;
- (IBAction)btnVenuesPressed:(id)sender;
- (IBAction)btnEventsPressed:(id)sender;

@end
