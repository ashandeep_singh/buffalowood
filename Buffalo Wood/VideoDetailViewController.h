//
//  VideoDetailViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 23/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h> 

@interface VideoDetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary *dictForPage;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDate,*lblCount,*lblAddedBy;
@property (strong, nonatomic) IBOutlet UIWebView *lblDiscription;
@property (nonatomic,retain) IBOutlet  UIWebView *webview;
@property (nonatomic,retain) IBOutlet UIProgressView *progressview;
@end
