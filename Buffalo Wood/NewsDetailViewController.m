//
//  NewsDetailViewController.m
//  Buffalo Wood
//
//  Created by Ashandeep on 22/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import "NewsDetailViewController.h"

@interface NewsDetailViewController ()
{
    UIScrollView *aScrollView;
    NSMutableArray *temp;
    NSInteger hieght;
    NSInteger numberOfPapers;
    NSInteger currentPage;
}
@end

@implementation NewsDetailViewController
@synthesize dictForPage;
@synthesize btnLeftImageSlider, btnRightImageSlider,indexForArray;
NSInteger _currentImageIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view. self.title = dictForPage[@"name"];
    
     hieght=255;
    
    [self.bkbtn setFrame:CGRectMake(12, 30, 23, 23)];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Avenir Next" size:18],
      NSFontAttributeName, nil]];
    
    
    self.navigationItem.leftBarButtonItem = [CommonFunctions getBackBtn];  
    self.navigationController.navigationBarHidden = YES;
    
    _lblTitle.text = dictForPage[@"title"];
    
    
    NSString *myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                                   "<head> \n"
                                   "<style type=\"text/css\"> \n"
                                   "body {font-family: \"%@\"; font-size: %@;}\n"
                                   "</style> \n"
                                   "</head> \n"
                                   "<body>%@</body> \n"
                                   "</html>", @"Avenir Next", [NSNumber numberWithInt:14], dictForPage[@"news_description"]];
    
    [_lblDiscription loadHTMLString:myDescriptionHTML baseURL:nil];
    
   // [_lblDiscription loadHTMLString:dictForPage[@"news_description"] baseURL:nil];

    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:dictForPage[@"news_date"]];
    dateFormatter.dateFormat = @"MMMM dd";
    NSMutableString *startDate = [[dateFormatter stringFromDate:yourDate] mutableCopy];
    
    _lblDate.text = [NSString stringWithFormat:@"%@",startDate];
    
    
    
    temp=[[NSMutableArray alloc] init];
    
    if([dictForPage[@"images"] count]>0)
    {
        
        for(int i=0;i<[dictForPage[@"images"] count];i++)
        {
            NSString *imageviewurl1 = [NSString stringWithFormat:@"%@images-news/%@",SiteURL,[[dictForPage[@"images"] objectAtIndex:i] objectForKey:@"image"]];
            
    //        [_imgViewBg setImageFromURL:imageviewurl1
    //              showActivityIndicator:YES
    //                      setCacheImage:YES];

            [temp addObject:imageviewurl1];
        }
        
        _currentImageIndex = 0;
        
//        UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(changeImage:)];
//        swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
//        [_imgViewBg addGestureRecognizer:swipeLeftGesture];
//      
//        
//        UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(changeImage:)];
//        swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
//        [_imgViewBg addGestureRecognizer:swipeRightGesture];
        
        [self.bkbtn setImage:[UIImage imageNamed:@"navbar-back-icon-inverted"] forState:UIControlStateNormal];

    }
    else
    {
        [self.bkbtn setImage:[UIImage imageNamed:@"navbar-back-icon"] forState:UIControlStateNormal];
    }
    
   
    
    //  [self makeButtonSlider:temp];
    
    [btnLeftImageSlider setFrame:CGRectMake(0,125,25,40)];
    [btnRightImageSlider setFrame:CGRectMake(290,125,25,40)];
    
    [self setData];
    
}

- (void)changeImage:(UISwipeGestureRecognizer *)swipe{
    
    NSArray *images = temp;
    
    NSInteger nextImageInteger = _currentImageIndex;
    
    
    if(swipe.direction == UISwipeGestureRecognizerDirectionLeft)
        nextImageInteger++;
    else
        nextImageInteger--;
    
    
    if(nextImageInteger < 0)
        nextImageInteger = images.count -1;
    else if(nextImageInteger > images.count - 1)
        nextImageInteger = 0;
    
    _currentImageIndex = nextImageInteger;
    
    UIImage *target = [images objectAtIndex:_currentImageIndex];
    
    CABasicAnimation *crossFade = [CABasicAnimation animationWithKeyPath:@"contents"];
    crossFade.duration      = 0.5;
    crossFade.fromValue     = _imgViewBg.image;
    crossFade.toValue       = target;
    [_imgViewBg.layer addAnimation:crossFade forKey:@"animateContents"];
    _imgViewBg.image = target;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
  
//    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc]
//                                         initWithTarget:self action:@selector(handleSwipeFromRight:)];
//    
//    gesture.direction = UISwipeGestureRecognizerDirectionRight;
//    
//    [self.view addGestureRecognizer:gesture];
    
    UISwipeGestureRecognizer *gesture1 = [[UISwipeGestureRecognizer alloc]
                                         initWithTarget:self action:@selector(handleSwipeFromRightImg:)];
    
    gesture1.direction = UISwipeGestureRecognizerDirectionRight;
    
    UISwipeGestureRecognizer *gesture2 = [[UISwipeGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleSwipeFromLeftImg:)];
    
    gesture2.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [self.view addGestureRecognizer:gesture1];
     [self.view addGestureRecognizer:gesture2];
    
}
- (void)handleSwipeFromRight:(UISwipeGestureRecognizer *)recognizer
{
    [self BackBtnClicked:recognizer];
}
- (void)handleSwipeFromLeftImg:(UISwipeGestureRecognizer *)recognizer
{
    [self btnLeftImageSliderPressed:recognizer];
}
- (void)handleSwipeFromRightImg:(UISwipeGestureRecognizer *)recognizer
{
    [self btnRightImageSliderPressed:recognizer];
}

#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}
-(IBAction)BackBtnClicked:(id)sender{
    //    UITabBarController *tabBarController = (UITabBarController*)[DELEGATE.window rootViewController];
    //    UINavigationController *navigationController = (UINavigationController*)[tabBarController selectedViewController];
    //    [navigationController  popToRootViewControllerAnimated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)setData
{
    
    UIScrollView *scrollView = (UIScrollView*)[self.view viewWithTag:111];
    NSArray *btnArray = [scrollView subviews];
    
    for (id btnObject in btnArray) {
        if ([btnObject isKindOfClass:[UIButton class]]) {
            UIButton *btnNotSet = (UIButton*)btnObject;
            [btnNotSet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
    
    
    //    [aScrollView removeFromSuperview];
    [self imageSlider];
    
}

- (void)imageSlider{
    // ----- image slider code
    CGFloat pointX = 0, paperWidth = 320, paperHeight = hieght;
    //    if (IS_iPad) {
    //        pointX = 55;
    //        paperWidth = 650;
    //        paperHeight = 300;
    //    }
    
    aScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pointX, 0, paperWidth, paperHeight)];
    aScrollView.showsHorizontalScrollIndicator = NO;
    [aScrollView setDelegate:self];
    //[aScrollView setBackgroundColor:[CommonFunctions getThemeColor:@"backgroundColor"]];
    
    numberOfPapers = temp.count;
    // hide left right button remove
    if (numberOfPapers <= 1) {
        btnLeftImageSlider.hidden = YES;
        btnRightImageSlider.hidden = YES;
          self.btnPager.hidden = YES;
    } else {
        btnLeftImageSlider.hidden = NO;
        btnRightImageSlider.hidden = NO;
          self.btnPager.hidden = NO;
    }
    
    // set images
    for (NSUInteger i = 0; i < numberOfPapers; i++) {
        NSString *imgURL = temp[i];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(paperWidth * i, 0, paperWidth, aScrollView.bounds.size.height)];
        [imageView setContentMode:UIViewContentModeScaleToFill];
        
        imageView.image = [UIImage imageNamed:@"Placeholder"];
        
        NSURL *urlimg = [NSURL URLWithString:imgURL];
        
        imageView.imageURL=urlimg;
        
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [aScrollView addSubview:imageView];
    }
    
    CGSize contentSize = CGSizeMake(paperWidth * numberOfPapers, aScrollView.bounds.size.height);
    aScrollView.contentSize = contentSize;
    
    aScrollView.pagingEnabled = YES;
    
    [self.view addSubview:aScrollView];
    [self.view sendSubviewToBack:aScrollView];
    
    self.btnPager.numberOfPages = numberOfPapers;
    self.btnPager.currentPage = 0;
    // pageControl.currentPageIndicatorTintColor = [CommonFunctions getThemeColor:@"buttonColor"];
      //---------- image slider code end
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = aScrollView.frame.size.width;
    float xPos = aScrollView.contentOffset.x+10;
    int page = (int)xPos/pageWidth;
    currentPage = page;
      self.btnPager.currentPage = page;
}
- (IBAction)btnInsideSliderPressed:(id)sender {
    UIButton *btnPressed = (UIButton*)sender;
    NSLog(@"btn pressed inside the btn slider %ld",(long)btnPressed.tag);
    indexForArray = btnPressed.tag;
    [self setData];
    
}


// Image Buttons
- (IBAction)btnLeftImageSliderPressed:(id)sender{
    if (currentPage > 0 ) {
        CGFloat pageWidth = aScrollView.frame.size.width;
        [aScrollView setContentOffset:CGPointMake(aScrollView.contentOffset.x - pageWidth, aScrollView.contentOffset.y) animated:YES];
    } else {
        CGFloat pageWidth = aScrollView.frame.size.width;
        [aScrollView setContentOffset:CGPointMake(aScrollView.contentSize.width-pageWidth, aScrollView.contentOffset.y)
                             animated:YES];
    }
}

// check for overflow
- (IBAction)btnRightImageSliderPressed:(id)sender{
    if (currentPage < (numberOfPapers-1)) {
        
        CGFloat pageWidth = aScrollView.frame.size.width;
        [aScrollView setContentOffset:CGPointMake(aScrollView.contentOffset.x + pageWidth, aScrollView.contentOffset.y)
                             animated:YES];
    } else {
        [aScrollView setContentOffset:CGPointMake(0, aScrollView.contentOffset.y)
                             animated:YES];
    }
}
@end
