//
//  TicketViewController.h
//  Buffalo Wood
//
//  Created by Ashandeep on 06/02/15.
//  Copyright (c) 2015 ___iOS Technology___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    IBOutlet UITableView *table;
    IBOutlet UIScrollView *scrollview;

}
@property (nonatomic,strong)  NSArray *arrayTKTableData;

@end
