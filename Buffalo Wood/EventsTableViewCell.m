//
//  EventsTableViewCell.m
//  Buffalo Wood
//
//  Created by Gaurav on 03/09/14.
//  Copyright (c) 2014 ___iOS Technology___. All rights reserved.
//

#import "EventsTableViewCell.h"

@implementation EventsTableViewCell

@synthesize lblStartTime, lblEndTime, lblTitle,lblLoc;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
